$(document).ready(function() {
    
     $("#txtNombre").attr('maxlength','100');
     $("#txtCoordenadas").attr('maxlength','50');
     $("#txtUrl").attr('maxlength','100');    
    
    $("#txtUrl").prop('disabled', true);
    document.getElementById("radioNo").checked = true;

    //Se desactiva o activa el input de la URL
   $( "input" ).click(function() {
       if($( "input:checked" ).val() === "no" ){
           $("#txtUrl").val("");
           $("#txtUrl").prop('disabled', true);
       } 
       if($( "input:checked" ).val() === "si" ){
           $("#txtUrl").prop('disabled', false);
       }     
   });

   //Evento de la acción submit del formulario
   $( "#formRegistroDependencia" ).submit(function( event ) {
        $("#errorUrl").empty();  
        $("#errorNombre").empty();      

        var status = false;
        var txtNombre = $("#txtNombre").val();
        //Se hacen validaciones
        if(txtNombre.trim().length > 0 && txtNombre.trim().length <= 100){
          var txtCoordenadas = $("#txtCoordenadas").val();
          if(txtCoordenadas.trim().length > 0 && txtCoordenadas.trim().length <= 50){
              var conUrl = $( "input:checked" ).val();
              status=true;
              var txtUrl = $("#txtUrl").val();
              if(conUrl==="si"){
                  if(txtUrl.trim().length > 0 && txtUrl.trim().length <= 100){
                      status=true;
                  }else{
                      status=false;
                      $("#errorUrl").empty();
                      $("#errorUrl").append( "Longitud de URL de servidor de avisos incorrecta" );
                  }
              }
          }
        }else{
            if(txtNombre.trim().length === 0){
               $("#errorNombre").empty();
               $("#errorNombre").append( "Este campo es requerido" );             
            }else{
               $("#errorNombre").empty();
               $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" );             
            }

        }

        if(!status){
            event.preventDefault();
        }

   });  
});
$(document).ready(function(){
    var nombresEspacios = [];
    var nombresResponsables = [];
    
    //Se obtienen los nombres de los espacios de interés registrados de la dependencia
    $.ajax({
            url: baseurl+"AJAX/getEspaciosDeInteresRegistrados",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $.each(array, function(index, val) {
                   nombresEspacios[index] = val.nombre;
            });
    });    
    
    //Se cargan los nombres de los responsables en el campo con propiedad autocompletar
    $("#txtNombreEspacio").autocomplete({
        source: nombresEspacios
    });

    //Se obtienen los nombres de los responsables registrados de la dependencia
    $.ajax({
            url: baseurl+"AJAX/getResponsablesRegistrados",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
               $.each(array, function(index, val) {
                   nombresResponsables[index] = val.nombre;
                });
    }); 

    //Se cargan los nombres de los responsables en el campo con propiedad autocompletar
    $("#txtNombreResponsable").autocomplete({
        source: nombresResponsables
    });

    //Evento de la acción submit del formulario
    $("#formRegistroServicio").submit(function(event){
        $("#errorNombreEspacio").empty();  
        $("#errorNombre").empty();
        $("#errorDescripcion").empty();
        $("#errorNombreResponsable").empty();

        var status = false;
        //Se validan los campos
        if($("#txtNombreEspacio").val().trim().length > 0 && $("#txtNombreEspacio").val().trim().length <= 100){
            if($("#txtNombre").val().trim().length > 0 && $("#txtNombre").val().trim().length <= 100){
                if($("#txtDescripcion").val().trim().length > 0 && $("#txtDescripcion").val().trim().length <= 300){
                    if($("#txtNombreResponsable").val().trim().length > 0 && $("#txtNombreResponsable").val().trim().length <= 100){
                        status = true;
                    }else{
                        if($("#txtNombreResponsable").val().trim().length === 0){
                           $("#errorNombreResponsable").empty();
                           $("#errorNombreResponsable").append("Este campo es requerido" );
                        }else{
                           $("#errorNombreResponsable").empty();
                           $("#errorNombreResponsable").append( "El dato introducido en este campo excede el límite permitido" ); 
                        }                          
                    }
                }else{
                    if($("#txtDescripcion").val().trim().length === 0){
                       $("#errorDescripcion").empty();
                       $("#errorDescripcion").append("Este campo es requerido" );
                    }else{
                       $("#errorDescripcion").empty();
                       $("#errorDescripcion").append( "El dato introducido en este campo excede el límite permitido" ); 
                    }                      
                }
            }else{
                if($("#txtNombre").val().trim().length === 0){
                   $("#errorNombre").empty();
                   $("#errorNombre").append("Este campo es requerido" );
                }else{
                   $("#errorNombre").empty();
                   $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" ); 
                }                        
            }
        }else{
            if($("#txtNombreEspacio").val().trim().length === 0){
               $("#errorNombreEspacio").empty();
               $("#errorNombreEspacio").append("Este campo es requerido" );
            }else{
               $("#errorNombreEspacio").empty();
               $("#errorNombreEspacio").append( "El dato introducido en este campo excede el límite permitido" ); 
            }            
        }
        
      //Si ocurrió algún error de validación se detiene el evento submit
      if(!status){
          event.preventDefault();
      }        
        
    });

});
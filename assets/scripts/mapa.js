/*
 * Archivo JS que controla los elementos del Mapa
 */

google.maps.event.addDomListener(window, 'load', initialize);
var rectDependencia;

function initialize(){
    
    var mapOptions = {
      center: new google.maps.LatLng(19.541408, -96.927369),
      zoom: 16
    };

    //Se crea el mapa
    var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

    var bounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(19.541, -96.927),
        new google.maps.LatLng(19.543, -96.925)
    );

    //Se crea el rectángulo que marca las coordenadas de la dependencia que se va a registrar 
    rectDependencia = new google.maps.Rectangle({
      strokeColor: '#5AD435',
      fillColor: '#5AD435',
      bounds: bounds,
      editable: true,
      draggable: true
    });


    $("#txtCoordenadas").val(rectDependencia.getBounds().toUrlValue());

    //Se obtienen las coordenadas del lugar donde se posicione el rectángulo  
    google.maps.event.addListener(rectDependencia, 'dragend', function() {
          $("#txtCoordenadas").val(this.getBounds().toUrlValue());
    });  

    cargarDatos(map);

    rectDependencia.setMap(map);    
}

/*
 * Función que obtiene mediante AJAX las coordenadas de las dependencias registradas
 * y crea rectángulos para cargarlos en el mapa.
 */
function cargarDatos(mapa){
    $.ajax({
        url: baseurl+"RegistroDependencia/enviarCoordenadasRegistradas",
        type: 'GET'
    })
    .done(function(datos) {
        var array = $.parseJSON(datos);
        $.each(array, function(index, val) {
           var array_coordenadas = val.coordenadas.split(",");
           var lat_lo= parseFloat(array_coordenadas[0]);
           var lng_lo= parseFloat(array_coordenadas[1]);
           var lat_hi= parseFloat(array_coordenadas[2]);
           var lng_hi= parseFloat(array_coordenadas[3]);
           
           var bounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(lat_lo, lng_lo),
                new google.maps.LatLng(lat_hi, lng_hi)
            );
           
           var rectangle = new google.maps.Rectangle({
               strokeColor: '#05C1F9',
               strokeOpacity: 0.8,
               strokeWeight: 2,
               fillColor: '#05C1F9',
               fillOpacity: 0.35,
               bounds: bounds
           });
         
           rectangle.setMap(mapa);
        });
    });    
}
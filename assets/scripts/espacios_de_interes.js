var menuEstado = false;
$(document).ready(function(){
    //Si ya no hay marcadores desactivamos el botón para agregar espacios de interés y mostramos un mensaje
    if(hayMarcadores == false){
        $("#btnAgregar").attr("disabled",true);
        $("#mensajeMarcadores").css('margin-bottom','0px');
        $($(".alert")[0]).css('margin-bottom','0px');
    }    
    
    //Evento del botón que elimina espacios de interés
    $("#div-eliminar, #btn-eliminar").click(function(){
        var idEspacios = [];
        //Se obtienen los elemenos marcados
        var mediaArray = $(".media >span >input:checked");
        //Si hay elementos seleccionados, se abre un cuadro de dialogo para confirmar
        if(mediaArray.length > 0){
            var p = $("<p>");
            $(p).append("Uno o varios espacios de interés van a eliminarse. Los servicios, eventos asociados deberán de ser asignados a otro espacio de interés posteriormente, los señalamientos asociados serán eliminados. ¿Desea continuar?");
            $( "#dialog" ).empty();
            $( "#dialog" ).append(p);
            $( "#dialog" ).dialog({
              width: 520,
              modal: true,
              draggable: false,
              resizable: false,
              buttons: [
                {
                  text: "Aceptar",
                  //Evento para el botón aceptar de cuadro de dialogo
                  click: function() {
                    //Se obtiene los nombres de los espacios de los elementos marcados
                    for(var i=0; i < mediaArray.length; i++){
                        var aux = $(".media >span >input:checked")[i];
                        idEspacios[i] = $(aux).data().idEspacio;
                    }
                    //Mediante AJAX se envían los nombres de los espacios que serán eliminados
                    $.ajax({
                      method: "POST",
                      url: baseurl+"EspaciosDeInteres/eliminarEspacioDeInteres",
                      data: { espacios: idEspacios}
                    })
                    //Se borran los elementos de la lista que fueron eliminados
                      .done(function( msg ) {
                        var count = 0;
                        for(var i=0; i < mediaArray.length; i++){
                            var aux =  mediaArray[i];
                            var liespacio =  $( aux ).parents().get(1);
                            $(liespacio).remove();
                            count++;
                        }        
                        //Se muestra un mensaje de confirmación
                        var mensaje = document.getElementById("mensaje");
                        mensaje.innerHTML = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Exito!</strong> Se ha eliminado '+count+' espacio(s) de interés</div>';
                        if(hayMarcadores == false){
                            hayMarcadores = true;
                            $("#btnAgregar").attr("disabled",false);     
                            $("#mensajeMarcadores").css('display','none');
//                            $(mensaje).css('margin-top',$('nav').css('height'));
                        }                        
                        $( "#dialog" ).dialog( "close" );
                      });

                  }
                },      
                {
                  text: "Cancelar",
                  //Evento para el botón Cancelar del cuadro de dialogo
                  click: function() {
                    $( this ).dialog( "close" );
                  }
                }                
                
              ]
            });     
  
        }else{
            //Se muestra un mensaje para avisar que no fue seleccionado ningún elemento
            var mensaje = document.getElementById("mensaje");         
            mensaje.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Espere! </strong> Primero debe seleccionar un espacio de interés</div>';
//            if(hayMarcadores){
//                //$(mensaje).css('margin-top',$('nav').css('height'));
//            } 
        }
    });
    
    //Evento para el link (<a>) que manda a la página para modificar la información del espacio de interés
    $("#ar-Modificar, #aModificar").click(function(e){
        var mediaArray = $(".media >span >input:checked");
        //Si hay solo un elemento seleccionado
        if(mediaArray.length == 1){
            var aux = $(".media >span >input:checked")[0];
            //Se añade a la ruta del link el nombre del evento que será recibido como parametro por el controlador
            var idEspacio = $(aux).data().idEspacio;
            var href = $(this).attr("href") + idEspacio;
            $(this).attr("href", href);
        }else{
            //Se despliega un mensaje en caso de que no haya elementos seleccionados
            e.preventDefault();
            var mensaje = document.getElementById("mensaje");   
            mensaje.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Espere! </strong> Debe seleccionar solo un espacio de interés</div>';
//            if(hayMarcadores){
////               $(mensaje).css('margin-top',$('nav').css('height'));
//            } 
        }        
    });
    
    $("#btnMenu").click(function(e){
       if(menuEstado){
           $("#bs-example-navbar-collapse-1").addClass('collapse');
           $("#bs-example-navbar-collapse-1").addClass('navbar-collapse'); 
           $("#cerrarSesionResponsivo").css('display','none')
           menuEstado = false;
       }else{
           $("#bs-example-navbar-collapse-1").removeClass('collapse');
           $("#bs-example-navbar-collapse-1").removeClass('navbar-collapse');
           $("#cerrarSesionResponsivo").css('display','block')
           menuEstado = true;
       } 
    });
    
});


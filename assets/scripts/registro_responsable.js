$(document).ready(function(){
    
    //Evento del submit del formulario para el modo básico
    $("#formRegistroResponsableBasico").submit(function( event) {
        $("#errorCorreo").empty();
        $("#errorNombre").empty();        
        
        var status = false;
        //Se validan los campos
        if($("#txtNombre").val().trim().length > 0 && $("#txtNombre").val().trim().length <= 100){
            if($("#txtCorreo").val().trim().length > 0 && $("#txtCorreo").val().trim().length <= 50){
                if($("#txtCorreo").val().indexOf("@uv.mx") > 1){
                    status = true;
                }else{
                    $("#errorCorreo").empty();
                    $("#errorCorreo").append( "Ingrese un correo con el formato correo@uv.mx" );
                }
            }else{
                if($("#txtCorreo").val().trim().length === 0){
                    $("#errorCorreo").empty();
                    $("#errorCorreo").append( "Este campo es requerido" );
                }else{
                    $("#errorCorreo").empty();
                    $("#errorCorreo").append( "El dato introducido en este campo excede el límite permitido" );   
                }
            }
        }else{
            if($("#txtNombre").val().trim().length === 0){
               $("#errorNombre").empty();
               $("#errorNombre").append("Este campo es requerido" );
            }else{
               $("#errorNombre").empty();
               $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" ); 
            }
        }
        if(!status){
            event.preventDefault();
        }
    });
    
    //Evento del submit del formulario para el modo avanzado
    $("#formRegistroResponsableAvanzado").submit(function( event ) {
        $("#errorUrl").empty();         
        var status = false;
        //Se validan los campos
        if($("#txtUrl").val().trim().length > 0 && $("#txtUrl").val().trim().length <= 100){
            if($("#txtUrl").val().indexOf("uv.mx") > 0){
                status = true;
            }else{
               $("#errorUrl").empty();
               $("#errorUrl").append("Ingrese una URL de personal académico con dominio www.uv.mx" );                
            }
        }else{
            if($("#txtUrl").val().trim().length === 0){
               $("#errorUrl").empty();
               $("#errorUrl").append("Este campo es requerido" );
            }else{
               $("#errorUrl").empty();
               $("#errorUrl").append( "El dato introducido en este campo excede el límite permitido" ); 
            }            
        }
        
        if(!status){
            event.preventDefault();
        }        
        
    });    
    
    //Muestra u oculta la tabla de responsables registrados
    $("#btn-mostrar").click(function(){
        if($("#btn-mostrar").text() === "Mostrar"){
            $("#btn-mostrar").empty();
            $("#btn-mostrar").append("Ocultar");
        }else{
            $("#btn-mostrar").empty();
            $("#btn-mostrar").append("Mostrar");            
        }
    });
    
    //Evento del botón eliminar, elimina los responsables seleccionados de la tabla
    $("#btn-eliminar").click(function(){
        var correosResponsables = [];
        //Obtiene los responsables seleccionados
         var responsablesSeleccionados = $("table tr input:checked");
         //Muestra cuadro de dialogo para confirmar
         if(responsablesSeleccionados.length > 0){
             var p = $("<p>");
            $(p).append("Uno o varios responsables van a eliminarse. Los servicios asociados deberán de ser asignados a otro responsable posteriormente. ¿Desea continuar?");
            $( "#dialog" ).empty();
            $( "#dialog" ).append(p);
            $( "#dialog" ).dialog({
              width: 520,
              modal: true,
              draggable: false,
              resizable: false,
              buttons: [
                {
                  text: "Aceptar",
                  //Evento del botón aceptar del cuadro de dialogo
                  click: function() {       
                      //Obtiene los correos de los responsables seleccionados
                    for(var i=0; i < responsablesSeleccionados.length; i++){
                        var aux = $("table tr input:checked")[i];
                        correosResponsables[i] = $(aux).val();
                    }     
                    //Mediante AJAX envía los correos de los responsables que serán eliminados
                    $.ajax({
                      method: "POST",
                      url: baseurl+"RegistroPersonal/eliminarResponsable",
                      data: { responsables: correosResponsables}
                    })                
                     .done(function( msg ) {
                        var count = 0;
                        //Elimina los responsables de la tabla
                        for(var i=0; i < responsablesSeleccionados.length; i++){
                            var aux = $("table tr input:checked")[i];
                            var tr =  $( aux ).parents().get(1);
                            $(tr).remove();
                            count++;
                        }
                        var mensaje = document.getElementById("mensaje");
                        mensaje.innerHTML = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Exito!</strong> Se ha eliminado '+count+' responsable(s)</div>';
                        $( "#dialog" ).dialog( "close" );
                      });                  
                  }           
            },
                {
                  text: "Cancelar",
                  //Evento para el botón cancelar del dialogo
                  click: function() {
                    $( this ).dialog( "close" );
                  }
                }                
                
              ]
            });                   
         }else{
             //Se despliega un mensaje en caso de que no se haya seleccionado ningún responsable
            var mensaje = document.getElementById("mensaje");         
            mensaje.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Espere! </strong> Primero debe seleccionar un responsable</div>';
                         
         }
    });
    
    $("#txtBuscar").keyup(function(){
        var txtBuscar = $(this).val();
        if(txtBuscar.length === 0){
            var trs  = $("#tablaPersonal tbody tr");
            $.each(trs, function(index, value){
                $(value).css("display","table-row");
            });             
        }
    });    
    
    $("#btnBuscar").click(function (){
        var txtBuscar = $("#txtBuscar").val();
        if(txtBuscar.length > 0){
            var trs  = $("#tablaPersonal tbody tr");
            $.each(trs, function(index, value){
                var tdNombre = $(value).children()[2];
                var nombre = $(tdNombre).text();

                if(nombre.indexOf(txtBuscar) < 0){
                    $(this).css("display","none");
                }else{
                    $(this).css("display","table-row");
                }

            });            
        }
    });
    
    //Oculta la tabla de responsables al cargar la página
    $( "#btn-mostrar" ).trigger( "click" );    
    
});
$(document).ready(function() {
     $("#txtNombre").attr('maxlength','100');
     $("#txtCorreo").attr('maxlength','50');
     $("#txtContrasenia").attr('maxlength','64');
     $("#txtContraseniaVerif").attr('maxlength','64');

    //Se  consiguen los nombres de las depenencias registradas mediante AJAX y se cargan en un combobox
    $.ajax({
            url: baseurl+"RegistroAdministrador/enviarNombresDeDependencias",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);

            if(array.length>0){       
               $("#selectDependencia").append($("<option>").attr('value',"NULL").append("Dependencias"));
               $.each(array, function(index, val) {
                    var nombreDependencia = val.nombre;
                    $("#selectDependencia").append($("<option>").attr('value',nombreDependencia.trim()).append(nombreDependencia));
                });   
            }else{
                $("#selectDependencia").append($("<option>").attr('value',"NULL").append("No hay dependencias registradas"));
            }

    });

    //Evento al hacer submit al formulario
    $( "#formRegistroAdministrador" ).submit(function( event ) {    
      $("#errorNombreDependencia").empty();  
      $("#errorContrasenia").empty();
      $("#errorCorreo").empty();
      $("#errorNombre").empty();
      $("#errorVerifContrasenia").empty();

      var status = false;
      //Se validan los campos
      if($("#txtNombre").val().trim().length > 0 && $("#txtNombre").val().trim().length <= 100){
          if($("#txtCorreo").val().trim().length > 0 && $("#txtCorreo").val().trim().length <= 50){
              if($("#txtCorreo").val().indexOf("@uv.mx") > 1){
                  if($("#txtContrasenia").val().trim().length > 0 && $("#txtContrasenia").val().trim().length < 64){
                      if($("#txtContraseniaVerif").val().trim() === $("#txtContraseniaVerif").val().trim()){
                          var nombreDependencia = $( "#selectDependencia" ).val();
                          if(nombreDependencia != "NULL"){
                              $("#txtNombreDependencia").val(nombreDependencia);
                              status = true;
                          }else{
                              $("#errorNombreDependencia").empty();
                              $("#errorNombreDependencia").append( "No ha elegido una dependencia" );                          
                          }
                      }else{
                          $("#errorVerifContrasenia").empty();
                          $("#errorVerifContrasenia").append( "La contraseña y su verificación no coinciden" );                      
                      }
                  }else{
                      if($("#txtContrasenia").val().trim().length == 0){
                          $("#errorContrasenia").empty();
                          $("#errorContrasenia").append( "Este campo es requerido" );    
                      }else{
                          $("#errorContrasenia").empty();
                          $("#errorContrasenia").append( "El dato introducido en este campo excede el límite permitido" );  
                      }

                  }
              }else{
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "Ingrese un correo con el formato correo@uv.mx" );
              }
          }else{
              if($("#txtCorreo").val().trim().length == 0){
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "Este campo es requerido" );
              }else{
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "El dato introducido en este campo excede el límite permitido" );   
              }
          }
      }else{
          if($("#txtNombre").val().trim().length == 0){
             $("#errorNombre").empty();
             $("#errorNombre").append("Este campo es requerido" );
          }else{
             $("#errorNombre").empty();
             $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" ); 
          }

      }

      //Si ocurrió algún error de validación se detiene el evento submit
      if(!status){
          event.preventDefault();
      }

    });  

});
var menuEstado = false;
$(document).ready(function(){
    //Evento del botón que elimina servicios
    $("#div-eliminar, #btn-eliminar").click(function(){      
        var nombreEspacios = [];
        //Se obtienen los elemenos marcados
        var mediaArray = $(".list-group-item span input:checked");
        //Si hay elementos seleccionados, se abre un cuadro de dialogo para confirmar
        if(mediaArray.length > 0){
            var p = $("<p>");
            $(p).append("Uno o varios señalamientos van a eliminarse. ¿Desea continuar?");
            $( "#dialog" ).empty();
            $( "#dialog" ).append(p);
            $( "#dialog" ).dialog({
              width: 520,
              modal: true,
              draggable: false,
              resizable: false,
              buttons: [
                {
                  text: "Aceptar",
                  //Evento para el botón aceptar de cuadro de dialogo
                  click: function() {
                    //Se obtiene los nombres de los espacios de los elementos marcados
                    for(var i=0; i < mediaArray.length; i++){
                        var aux = $(".list-group-item span input:checked")[i];
                        nombreEspacios[i] = $(aux).data().senialamiento;
                    }
                    //Mediante AJAX se envían los nombres de los espacios que tienen señalamientos que serán eliminados
                    $.ajax({
                      method: "POST",
                      url: baseurl+"Senialamientos/eliminarSenialamiento",
                      data: { espacios: nombreEspacios}
                    })
                    //Se borran los elementos de la lista que fueron eliminados
                      .done(function( msg ) {
                        var count = 0;
                        for(var i=0; i < mediaArray.length; i++){
                            var aux = mediaArray[i];
                            var lisenialamiento =  $( aux ).parents().get(1);
                            $(lisenialamiento).remove();
                            count++;
                        }        
                        //Se muestra un mensaje de confirmación
                        var mensaje = document.getElementById("mensaje");
                        mensaje.innerHTML = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Exito!</strong> Se ha eliminado '+count+' señalamiento(s)</div>';                        
                        $( "#dialog" ).dialog( "close" );
                      });
                  }
                },      
                {
                  text: "Cancelar",
                  //Evento para el botón Cancelar del cuadro de dialogo
                  click: function() {
                    $( this ).dialog( "close" );
                  }
                }                
                
              ]
            });                
        }else{
            //Se muestra un mensaje para avisar que no fue seleccionado ningún elemento
            var mensaje = document.getElementById("mensaje");         
            mensaje.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Espere! </strong> Primero debe seleccionar un señalamiento</div>';
        }        
    });  
    
    //Evento para el link (<a>) que manda a la página para modificar la información del espacio de interés
    $("#ar-Modificar, #aModificar").click(function(e){
        var mediaArray = $(".list-group-item span input:checked");
        //Si hay solo un elemento seleccionado
        if(mediaArray.length == 1){
            var aux = $(".list-group-item span input:checked")[0];
            //Se añade a la ruta del link el nombre del evento que será recibido como parametro por el controlador
            var nombreEvento = $(aux).data().senialamiento;
            var href = $(this).attr("href") + nombreEvento;
            $(this).attr("href", href);
        }else{
            //Se despliega un mensaje en caso de que no haya elementos seleccionados
            e.preventDefault();
            var mensaje = document.getElementById("mensaje");   
            mensaje.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Espere! </strong> Debe seleccionar solo un señalamiento</div>';
        }        
    });    
    
    $("#btnMenu").click(function(e){
       if(menuEstado){
           $("#bs-example-navbar-collapse-1").addClass('collapse');
           $("#bs-example-navbar-collapse-1").addClass('navbar-collapse'); 
           $("#cerrarSesionResponsivo").css('display','none')
           menuEstado = false;
       }else{
           $("#bs-example-navbar-collapse-1").removeClass('collapse');
           $("#bs-example-navbar-collapse-1").removeClass('navbar-collapse');
           $("#cerrarSesionResponsivo").css('display','block')
           menuEstado = true;
       } 
    });        
    
});
$(document).ready(function(){
   
    var nombresEspacios = [];
    
    //Se obtienen los nombres de los espacios de interés registrados de la dependencia que no cuentan con señalamiento
    $.ajax({
            url: baseurl+"AJAX/getEspaciosDeInteresRegistradosSinSenialamiento",
            type: 'GET'
        })
        .done(function(datos) {
            if(datos !== "false"){
                var array = $.parseJSON(datos);
                $.each(array, function(index, val) {
                       nombresEspacios[index] = val.nombre;
                });                
            }else{
                $("#mensaje").append('<div class="alert alert-warning" role="alert"><strong>¡Aviso! </strong>  Todos los espacios de interés registrados ya cuentan con un señalamiento</div>');
            }
    });        
    
    //Se cargan los nombres de los espacios sin señalamientos en el campo con propiedad autocompletar
    $("#txtNombreEspacio").autocomplete({
        source: nombresEspacios
    });    
    
   
    //Evento de la acción submit del formulario
    $("form").submit(function(event){
        $("#errorNombreEspacio").empty();  
        $("#errorOrientaciones").empty();

        var status = false;
        //Se validan los campos
        if($("#txtNombreEspacio").val().trim().length > 0 && $("#txtNombreEspacio").val().trim().length <= 100){
            var txtArriba  = $("#txtArriba").val().trim();
            var txtArribaDerecha  = $("#txtArribaDerecha").val().trim();
            var txtDerecha  = $("#txtDerecha").val().trim();
            var txtAbajoDerecha  = $("#txtAbajoDerecha").val().trim();
            var txtAbajo  = $("#txtAbajo").val().trim();
            var txtAbajoIzquierda  = $("#txtAbajoIzquierda").val().trim();
            var txtIzquierda  = $("#txtIzquierda").val().trim();
            var txtIzquierdaArriba  = $("#txtIzquierdaArriba").val().trim();
            if( (txtArriba.length <= 300 && txtArriba.length > 0) || (txtArribaDerecha.length <= 300 && txtArribaDerecha.length > 0) ||
                    (txtDerecha.length <= 300 && txtDerecha.length > 0) || (txtAbajoDerecha.length <= 300 && txtAbajoDerecha.length > 0) ||
                    (txtAbajo.length <= 300 && txtAbajo.length > 0) ||(txtAbajoIzquierda.length <= 300 && txtAbajoIzquierda.length > 0) ||
                    (txtIzquierda.length <= 300 && txtIzquierda.length > 0) || (txtIzquierdaArriba.length <= 300 && txtIzquierdaArriba.length > 0)){               
                var status = true;
            }else{
                $("#errorOrientaciones").append("Debes ingresar por lo menos una indicación");
            }
        }else{
            if($("#txtNombreEspacio").val().trim().length === 0){
               $("#errorNombreEspacio").append("Este campo es requerido" );
            }else{
               $("#errorNombreEspacio").append( "El dato introducido en este campo excede el límite permitido" ); 
            }            
        }
        
      //Si ocurrió algún error de validación se detiene el evento submit
      if(!status){
          event.preventDefault();
      }        
        
    });    
    
    
});
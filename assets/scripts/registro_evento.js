$(document).ready(function(){
    var nombresEspacios = [];
    
    var dateToday = new Date();
    
    //Se crean los calendarios para elegir las fechas de inicio y fin del evento
    $('#txtFechaInicio').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });
    $('#txtFechaFin').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });    
    
    $("#txtFechaInicio").on("dp.change", function (e) {
        $('#txtFechaFin').data("DateTimePicker").minDate(e.date);
    });    
   
    $("#evtExterno").addClass("novisible");
    document.getElementById("radioSi").checked = true;

    //Se muestra u oculta los campos para agregar un evento externo
   $( "input" ).click(function() {
       if($( "input:checked" ).val() === "no" ){
           $("#evtExterno").removeClass("novisible");
           $("#txtNombreEspacio").addClass("novisible");
           $("#lbEvtExterno").addClass("novisible");
           $("#txtNombreEspacio").val("");
       } 
       if($( "input:checked" ).val() === "si" ){   
           $("#evtExterno").addClass("novisible");
           $("#txtNombreEspacio").removeClass("novisible");
           $("#lbEvtExterno").removeClass("novisible"); 
           $("#txtNombreLugar").val("");
       }     
   });        
    
    
    
    //Se obtienen los nombres de los espacios de interés registrados de la dependencia
    $.ajax({
            url: baseurl+"AJAX/getEspaciosDeInteresRegistrados",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $.each(array, function(index, val) {
                   nombresEspacios[index] = val.nombre;
            });
    });    
    
    //Se cargan los nombres de los responsables en el campo con propiedad autocompletar
    $("#txtNombreEspacio").autocomplete({
        source: nombresEspacios
    });


     //Se valida la imagen que seleccione el usuario
    $("#imagenDescriptiva").change(function(e){
        $("#errorImagenDescriptiva").empty();
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp1").prop("src", e.target.result);
                           $("#vp1").addClass("vistaPrevia");
                         };
                    })(value);
                     reader.readAsDataURL(value);   
               }else{
                   $("#errorImagenDescriptiva").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });
    
    //Se valida la imagen que seleccione el usuario
    $("#imagenMarcador").change(function(e){
        $("#errorImagenMarcador").empty(); 
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp2").prop("src", e.target.result);
                           $("#vp2").addClass("vistaPrevia");
                         };
                    })(value);
                     reader.readAsDataURL(value);                   
               }else{
                   $("#errorImagenMarcador").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });


    //Evento de la acción submit del formulario
    $("form").submit(function(event){
        $("#errorNombreEvento").empty();  
        $("#errorDescripcionEvento").empty();
        $("#errorFechaInicio").empty();
        $("#errorFechaFin").empty(); 
        $("#errorUrl").empty(); 
        $("#errorNombreEspacio").empty();  
        $("#errorNombreLugar").empty();
        $("#errorImagenDescriptiva").empty();
        $("#errorImagenMarcador").empty();        
        
        var status = false;
        var txtNombreEvento = $("#txtNombreEvento").val();        
        if(txtNombreEvento.trim().length > 0 && txtNombreEvento.trim().length <= 100){
            var txtDescripcionEvento = $("#txtDescripcionEvento").val();
            if(txtDescripcionEvento.trim().length > 0 && txtDescripcionEvento.trim().length <= 300){
                var txtFechaInicio = $("#txtFechaInicio").val();
                if(txtFechaInicio.trim().length === 16){
                    var txtFechaFin = $("#txtFechaFin").val();
                    if(txtFechaFin.trim().length === 16){
                        var txtUrlEvento = $("#txtUrlEvento").val();
                        if(txtUrlEvento.trim().length <= 100){
                            var txtNombreEspacio = $("#txtNombreEspacio").val();
                            if(txtNombreEspacio.trim().length > 0 && txtNombreEspacio.trim().length <= 100){
                                status = true;
                            }else{
                                if(txtNombreEspacio.trim().length === 0){
                                    var txtNombreLugar = $("#txtNombreLugar").val();
                                    if(txtNombreLugar.trim().length > 0 && txtNombreLugar.trim().length <= 100){
                                        var imgDescriptiva = $("#imagenDescriptiva");
                                        if( (imgDescriptiva.val().length > 0) && (imgDescriptiva.val().indexOf(".jpg") < 0 && imgDescriptiva.val().indexOf(".jpeg") < 0 && imgDescriptiva.val().indexOf(".png") < 0)  ){
                                            $("#errorImagenDescriptiva").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );                                            
                                        }else{
                                            var imagenMarcador = $("#imagenMarcador");
                                            if( (imagenMarcador.val().length > 0) &&  (imagenMarcador.val().indexOf(".jpg") < 0 && imagenMarcador.val().indexOf(".jpeg") < 0 && imagenMarcador.val().indexOf(".png") < 0) ){
                                                $("#errorImagenMarcador").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );  
                                            }else{
                                                status = true;                 
                                            }                                            
                                        }                                           
                                    }else{
                                        if(txtNombreLugar.trim().length === 0){
                                           $("#errorNombreLugar").append( "Este campo es requerido" );             
                                        }else{
                                           $("#errorNombreLugar").append( "El dato introducido en este campo excede el límite permitido" );             
                                        }
                                    }                                               
                                }else{
                                   $("#errorNombreEspacio").append( "El dato introducido en este campo excede el límite permitido" );             
                                }                                   
                            }
                        }else{
                               $("#errorUrl").append( "El dato introducido en este campo excede el límite permitido" );                                        
                        }
                    }else{
                        if(txtFechaFin.trim().length === 0){
                           $("#errorFechaFin").append( "Este campo es requerido" );             
                        }else{
                           $("#errorFechaFin").append( "El dato introducido es no es válido" );             
                        }                        
                    }
                }else{
                    if(txtFechaInicio.trim().length === 0){
                       $("#errorFechaInicio").append( "Este campo es requerido" );             
                    }else{
                       $("#errorFechaInicio").append( "El dato introducido es no es válido" );             
                    }                    
                }
                
            }else{
                if(txtDescripcionEvento.trim().length === 0){
                   $("#errorDescripcionEvento").append( "Este campo es requerido" );             
                }else{
                   $("#errorDescripcionEvento").append( "El dato introducido en este campo excede el límite permitido" );             
                }                
            }
        }else{
            if(txtNombreEvento.trim().length === 0){
               $("#errorNombreEvento").append( "Este campo es requerido" );             
            }else{
               $("#errorNombreEvento").append( "El dato introducido en este campo excede el límite permitido" );             
            }    
        }
        
        //Se detine la acción submit del formulario si se encuentran errores
        if(!status){
            event.preventDefault();
        }        
        
    });

});
$(document).ready(function(){
    
    //Mediante AJAX se obtienen los datos del espacio de interés a modificar
    $.ajax({
            url: baseurl+"ModificarEspacioDeInteres/obtenerDatosEspacio",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $("#txtNombre").val(array.nombre);
            $("#txtDescripcion").val(array.descripcion);
            $("#txtUrl").val(array.urlSitioWeb);
            $("#imgactual1").prop("src", 'data:image/png;base64,'+array.imagenDescriptiva);
            $("#imgactual1").addClass("vistaPrevia");
            $("#imgactual1").addClass("imgActual");
            
            $("#imgactual2").prop("src", 'data:image/png;base64,'+array.imagenMarcador);
            $("#imgactual2").addClass("vistaPrevia");
            $("#imgactual2").addClass("imgActual");
    });    
    
    //Se valida el archivo de imagen que seleccione el usuario
    $("#imagenDescriptiva").change(function(e){
        var archivos = e.target.files;
        
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Si no hay errores de validación se muestra una vista previa
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp1").prop("src", e.target.result);
                           $("#vp1").removeClass("imgEscondida");
                         };
                    })(value);
                     reader.readAsDataURL(value);   
               }else{
                   $("#errorImagenDescriptiva").empty();
                   $("#errorImagenDescriptiva").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });
    
    //Se valida el archivo de imagen que seleccione el usuario
    $("#imagenMarcador").change(function(e){
        var archivos = e.target.files;
        
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Si no hay errores de validación se muestra una vista previa
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp2").prop("src", e.target.result);
                           $("#vp2").removeClass("imgEscondida");
                         };
                    })(value);
                     reader.readAsDataURL(value);                   
               }else{
                   $("#errorImagenMarcador").empty();
                   $("#errorImagenMarcador").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });   
    
    //Evento para la acción submit del formulario
    $( "form" ).submit(function( event ) {    
        $("#errorImagenMarcador").empty();  
        $("#errorImagenDescriptiva").empty();
        $("#errorDescripcion").empty();
        $("#errorNombre").empty(); 

        var status = false;
        
        //Se validan los campos
        if($("#txtNombre").val().trim().length > 0 && $("#txtNombre").val().trim().length <= 100){
            if($("#txtDescripcion").val().trim().length > 0 && $("#txtDescripcion").val().trim().length <= 300){
                status = true;
                if($("#imagenDescriptiva").val().length > 0){
                    if($("#imagenDescriptiva").val().indexOf(".jpg") > 0 || $("#imagenDescriptiva").val().indexOf(".jpeg") > 0 || $("#imagenDescriptiva").val().indexOf(".png") > 0){
                        status = true;
                    }else{
                        status = false;
                        $("#errorImagenDescriptiva").empty();
                        $("#errorImagenDescriptiva").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );             
                    }
                }               
                if($("#imagenMarcador").val().length > 0){
                    if($("#imagenMarcador").val().indexOf(".jpg") > 0 || $("#imagenMarcador").val().indexOf(".jpeg") > 0 || $("#imagenMarcador").val().indexOf(".png") > 0){
                        if(status){
                            status = true;                            
                        }
                    }else{
                        status = false;
                         $("#errorImagenMarcador").empty();
                         $("#errorImagenMarcador").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );                   
                    }             
                }                                  
            }else{
               if($("#txtDescripcion").val().trim().length == 0){
                  $("#errorDescripcion").empty();
                  $("#errorDescripcion").append( "Este campo es requerido" );             
               }else{
                  $("#errorDescripcion").empty();
                  $("#errorDescripcion").append( "El dato introducido en este campo excede el límite permitido" );             
               }                
            }
        }else{
            if($("#txtNombre").val().trim().length == 0){
               $("#errorNombre").empty();
               $("#errorNombre").append( "Este campo es requerido" );             
            }else{
               $("#errorNombre").empty();
               $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" );             
            }            
        }

        //Si hay errores en la validación se detiene el evento submit
        if(!status){
            event.preventDefault();
        }

    });         
    
    
});
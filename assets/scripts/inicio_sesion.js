$(document).ready(function(){
    
    //Evento para la acción submit del formulario
    $( "form" ).submit(function( event ) {    
      $("#errorContrasenia").empty();
      $("#errorCorreo").empty();

      var status = false;

      //Se validan los campos
      if($("#txtContrasenia").val().trim().length > 0 && $("#txtContrasenia").val().trim().length < 64){
          if($("#txtCorreo").val().trim().length > 0 && $("#txtCorreo").val().trim().length <= 50){
              if($("#txtCorreo").val().indexOf("@uv.mx") > 1){
                 status = true;
              }else{
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "Introduce un correo con formato correo@uv.mx" ); 
              }               
          }else{
              if($("#txtCorreo").val().trim().length == 0){
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "Este campo es requerido" );
              }else{
                  $("#errorCorreo").empty();
                  $("#errorCorreo").append( "El dato introducido en este campo excede el límite permitido" );   
              }
          }
      }else{
          if($("#txtContrasenia").val().trim().length == 0){
             $("#errorContrasenia").empty();
             $("#errorContrasenia").append("Este campo es requerido" );
          }else{
             $("#errorContrasenia").empty();
             $("#errorContrasenia").append( "El dato introducido en este campo excede el límite permitido" ); 
          }

      }
      //Si ocurrió un error en la validación se detiene el evento submit
      if(!status){
          event.preventDefault();
      }

    });      
    
});
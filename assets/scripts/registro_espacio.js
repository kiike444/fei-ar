$(document).ready(function(){

     $("#txtNombre").attr('maxlength','100');
     $("#txtDescripcion").attr('maxlength','300');
     $("#txtUrl").attr('maxlength','100');

     //Se valida la imagen que seleccione el usuario
    $("#imagenDescriptiva").change(function(e){
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp1").prop("src", e.target.result);
                           $("#vp1").addClass("vistaPrevia");
                         };
                    })(value);
                     reader.readAsDataURL(value);   
               }else{
                   $("#errorImagenDescriptiva").empty();
                   $("#errorImagenDescriptiva").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });
    
    //Se valida la imagen que seleccione el usuario
    $("#imagenMarcador").change(function(e){
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp2").prop("src", e.target.result);
                           $("#vp2").addClass("vistaPrevia");
                         };
                    })(value);
                     reader.readAsDataURL(value);                   
               }else{
                   $("#errorImagenMarcador").empty();
                   $("#errorImagenMarcador").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });
    
    //Evento de la acción submit del formulario
    $( "form" ).submit(function( event ) {    
        $("#errorImagenMarcador").empty();  
        $("#errorImagenDescriptiva").empty();
        $("#errorDescripcion").empty();
        $("#errorNombre").empty(); 

        var status = false;
        var txtNombre = $("#txtNombre").val();
        //Se validan los campos
        if(txtNombre.trim().length > 0 && txtNombre.trim().length <= 100){
          var txtDescripcion = $("#txtDescripcion").val();
          if(txtDescripcion.trim().length > 0 && txtDescripcion.trim().length <= 300){
              var imgDescriptiva = $("#imagenDescriptiva");
              if( (imgDescriptiva.val().length > 0) && (imgDescriptiva.val().indexOf(".jpg") < 0 && imgDescriptiva.val().indexOf(".jpeg") < 0 && imgDescriptiva.val().indexOf(".png") < 0) ){
                $("#errorImagenDescriptiva").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );  
              }else{
                  var imagenMarcador = $("#imagenMarcador");
                  if( ($("#imagenMarcador").val().length > 0) && ( imagenMarcador.val().indexOf(".jpg") < 0 && imagenMarcador.val().indexOf(".jpeg") < 0 && imagenMarcador.val().indexOf(".png") < 0 ) ){
                      $("#errorImagenMarcador").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );  
                  }else{
                      status = true;                 
                  }           
              }
          }else{
               if(txtDescripcion.trim().length === 0){
                  $("#errorDescripcion").append( "Este campo es requerido" );             
               }else{
                  $("#errorDescripcion").append( "El dato introducido en este campo excede el límite permitido" );             
               }
          }
        }else{
            if(txtNombre.trim().length === 0){
               $("#errorNombre").append( "Este campo es requerido" );             
            }else{
               $("#errorNombre").append( "El dato introducido en este campo excede el límite permitido" );             
            }

        }

        //Se detine la acción submit del formulario si se encuentran errores
        if(!status){
            event.preventDefault();
        }

    });      
    
    
});
$(document).ready(function(){
  
    
    var dateToday = new Date();
    
    //Se crean los calendarios para elegir las fechas de inicio y fin del evento
    $('#txtFechaInicio').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });
    $('#txtFechaFin').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });    
    
    $("#txtFechaInicio").on("dp.change", function (e) {
        $('#txtFechaFin').data("DateTimePicker").minDate(e.date);
    }); 
    
    //Mediante AJAX se obtienen los datos del evento a modificar
    $.ajax({
            url: baseurl+"ModificarEventoExterno/obtenerDatosEvento",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $("#txtNombreEvento").val(array.nombre);
            $("#txtDescripcionEvento").val(array.descripcion);
            $("#txtFechaInicio").val(array.fechaInicio);
            $("#txtFechaFin").val(array.fechaFin);
            $("#txtUrlEvento").val(array.urlSitioWeb);
            $("#txtNombreLugar").val(array.lugar);
            $("#imgactual1").prop("src", 'data:image/png;base64,'+array.imagenDescriptiva);
            $("#imgactual2").prop("src", 'data:image/png;base64,'+array.imagenMarcador);
            $("#imgactual1").addClass("vistaPrevia");
            $("#imgactual1").addClass("imgActual");   
            $("#imgactual2").addClass("vistaPrevia");
            $("#imgactual2").addClass("imgActual");            
    });     
    
     //Se valida la imagen que seleccione el usuario
    $("#imagenDescriptiva").change(function(e){
        $("#errorImagenDescriptiva").empty();
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp1").prop("src", e.target.result);
                           $("#vp1").addClass("vistaPrevia");
                           $("#vp1").removeClass("imgEscondida");
                         };
                    })(value);
                     reader.readAsDataURL(value);   
               }else{
                   $("#errorImagenDescriptiva").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });
    
    //Se valida la imagen que seleccione el usuario
    $("#imagenMarcador").change(function(e){
        $("#errorImagenMarcador").empty(); 
        var archivos = e.target.files;
        $.each( archivos, function( index, value ) {
           if (value.type.match('image.jpeg') || value.type.match('image.jpg') || value.type.match('image.png')) {
               if(value.size < 2097152){
                   //Se crea una vista previa de la imagen
                    var reader = new FileReader();
                    reader.onload = (function() {
                         return function(e) {
                           $("#vp2").prop("src", e.target.result);
                           $("#vp2").addClass("vistaPrevia");
                           $("#vp2").removeClass("imgEscondida");
                         };
                    })(value);
                     reader.readAsDataURL(value);                   
               }else{
                   $("#errorImagenMarcador").append( "El peso de la imagen debe ser inferior a 2MB" );  
               }
           }
        });
    });    
    
    
    //Evento de la acción submit del formulario
    $("form").submit(function(event){
        $("#errorNombreEvento").empty();  
        $("#errorDescripcionEvento").empty();
        $("#errorFechaInicio").empty();
        $("#errorFechaFin").empty(); 
        $("#errorUrl").empty();  
        $("#errorNombreLugar").empty();
        $("#errorImagenDescriptiva").empty();
        $("#errorImagenMarcador").empty();        
        
        var status = false;
        var txtNombreEvento = $("#txtNombreEvento").val();        
        if(txtNombreEvento.trim().length > 0 && txtNombreEvento.trim().length <= 100){
            var txtDescripcionEvento = $("#txtDescripcionEvento").val();
            if(txtDescripcionEvento.trim().length > 0 && txtDescripcionEvento.trim().length <= 300){
                var txtFechaInicio = $("#txtFechaInicio").val();
                if(txtFechaInicio.trim().length === 16 || txtFechaInicio.trim().length === 19){
                    var txtFechaFin = $("#txtFechaFin").val();
                    if(txtFechaFin.trim().length === 16 || txtFechaFin.trim().length === 19){
                        var txtUrlEvento = $("#txtUrlEvento").val();
                        if(txtUrlEvento.trim().length <= 100){
                                    var txtNombreLugar = $("#txtNombreLugar").val();
                                    if(txtNombreLugar.trim().length > 0 && txtNombreLugar.trim().length <= 100){
                                        var imgDescriptiva = $("#imagenDescriptiva");
                                        if(imgDescriptiva.val().indexOf(".jpg") > 0 || imgDescriptiva.val().indexOf(".jpeg") > 0 || imgDescriptiva.val().indexOf(".png") > 0 || imgDescriptiva.val().length === 0){
                                            var imagenMarcador = $("#imagenMarcador");
                                            if(imagenMarcador.val().indexOf(".jpg") > 0 || imagenMarcador.val().indexOf(".jpeg") > 0 || imagenMarcador.val().indexOf(".png") > 0 || imagenMarcador.val().length === 0){
                                                status = true;
                                            }else{
                                                 $("#errorImagenMarcador").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );                   
                                            }                                            
                                        }else{
                                            $("#errorImagenDescriptiva").append( "Seleccione un archivo con extensión .jpg/.jpeg/.png" );
                                        }                                           
                                    }else{
                                        if(txtNombreLugar.trim().length === 0){
                                           $("#errorNombreLugar").append( "Este campo es requerido" );             
                                        }else{
                                           $("#errorNombreLugar").append( "El dato introducido en este campo excede el límite permitido" );             
                                        }
                                    }                                               
                        }else{
                               $("#errorUrl").append( "El dato introducido en este campo excede el límite permitido" );                                        
                        }
                    }else{
                        if(txtFechaFin.trim().length === 0){
                           $("#errorFechaFin").append( "Este campo es requerido" );             
                        }else{
                           $("#errorFechaFin").append( "El dato introducido es no es válido" );             
                        }                        
                    }
                }else{
                    if(txtFechaInicio.trim().length === 0){
                       $("#errorFechaInicio").append( "Este campo es requerido" );             
                    }else{
                       $("#errorFechaInicio").append( "El dato introducido es no es válido" );             
                    }                    
                }
                
            }else{
                if(txtDescripcionEvento.trim().length === 0){
                   $("#errorDescripcionEvento").append( "Este campo es requerido" );             
                }else{
                   $("#errorDescripcionEvento").append( "El dato introducido en este campo excede el límite permitido" );             
                }                
            }
        }else{
            if(txtNombreEvento.trim().length === 0){
               $("#errorNombreEvento").append( "Este campo es requerido" );             
            }else{
               $("#errorNombreEvento").append( "El dato introducido en este campo excede el límite permitido" );             
            }    
        }
        
        //Se detine la acción submit del formulario si se encuentran errores
        if(!status){
            event.preventDefault();
        }        
        
    });    
    
    
});



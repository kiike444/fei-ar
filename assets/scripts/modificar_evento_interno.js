$(document).ready(function (){
  
    var nombresEspacios = [];
    
    var dateToday = new Date();
    
    //Se crean los calendarios para elegir las fechas de inicio y fin del evento
    $('#txtFechaInicio').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });
    $('#txtFechaFin').datetimepicker({
        format : 'YYYY/MM/DD HH:mm',
        minDate: dateToday
    });    
    
    $("#txtFechaInicio").on("dp.change", function (e) {
        $('#txtFechaFin').data("DateTimePicker").minDate(e.date);
    });   
    
    
    //Se obtienen los nombres de los espacios de interés registrados de la dependencia
    $.ajax({
            url: baseurl+"AJAX/getEspaciosDeInteresRegistrados",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $.each(array, function(index, val) {
                   nombresEspacios[index] = val.nombre;
            });
    });    
    
    //Se cargan los nombres de los espacios de interés en el campo con propiedad autocompletar
    $("#txtNombreEspacio").autocomplete({
        source: nombresEspacios
    });
    
  
    //Mediante AJAX se obtienen los datos del evento a modificar
    $.ajax({
            url: baseurl+"ModificarEventoInterno/obtenerDatosEvento",
            type: 'GET'
        })
        .done(function(datos) {
            var array = $.parseJSON(datos);
            $("#txtNombreEvento").val(array.nombre);
            $("#txtDescripcionEvento").val(array.descripcion);
            $("#txtFechaInicio").val(array.fechaInicio);
            $("#txtFechaFin").val(array.fechaFin);
            $("#txtUrlEvento").val(array.urlSitioWeb);
            $("#txtNombreEspacio").val(array.nombreEspacio);
    });     
    
    
    //Evento de la acción submit del formulario
    $("form").submit(function(event){
        $("#errorNombreEvento").empty();  
        $("#errorDescripcionEvento").empty();
        $("#errorFechaInicio").empty();
        $("#errorFechaFin").empty(); 
        $("#errorUrl").empty(); 
        $("#errorNombreEspacio").empty();        
        
        var status = false;
        var txtNombreEvento = $("#txtNombreEvento").val();        
        if(txtNombreEvento.trim().length > 0 && txtNombreEvento.trim().length <= 100){
            var txtDescripcionEvento = $("#txtDescripcionEvento").val();
            if(txtDescripcionEvento.trim().length > 0 && txtDescripcionEvento.trim().length <= 300){
                var txtFechaInicio = $("#txtFechaInicio").val();
                if(txtFechaInicio.trim().length === 16 || txtFechaInicio.trim().length === 19){
                    var txtFechaFin = $("#txtFechaFin").val();
                    if(txtFechaFin.trim().length === 16 || txtFechaFin.trim().length === 19){
                        var txtUrlEvento = $("#txtUrlEvento").val();
                        if(txtUrlEvento.trim().length <= 100){
                            var txtNombreEspacio = $("#txtNombreEspacio").val();
                            if(txtNombreEspacio.trim().length > 0 && txtNombreEspacio.trim().length <= 100){
                                status = true;
                            }else{
                                if(txtNombreEspacio.trim().length === 0){
                                   $("#errorNombreEspacio").append( "Este campo es requerido" );             
                                }else{
                                   $("#errorNombreEspacio").append( "El dato introducido en este campo excede el límite permitido" );             
                                }                                   
                            }
                        }else{
                               $("#errorUrl").append( "El dato introducido en este campo excede el límite permitido" );                                        
                        }
                    }else{
                        if(txtFechaFin.trim().length === 0){
                           $("#errorFechaFin").append( "Este campo es requerido" );             
                        }else{
                           $("#errorFechaFin").append( "El dato introducido es no es válido" );             
                        }                        
                    }
                }else{
                    if(txtFechaInicio.trim().length === 0){
                       $("#errorFechaInicio").append( "Este campo es requerido" );             
                    }else{
                       $("#errorFechaInicio").append( "El dato introducido es no es válido" );             
                    }                    
                }
                
            }else{
                if(txtDescripcionEvento.trim().length === 0){
                   $("#errorDescripcionEvento").append( "Este campo es requerido" );             
                }else{
                   $("#errorDescripcionEvento").append( "El dato introducido en este campo excede el límite permitido" );             
                }                
            }
        }else{
            if(txtNombreEvento.trim().length === 0){
               $("#errorNombreEvento").append( "Este campo es requerido" );             
            }else{
               $("#errorNombreEvento").append( "El dato introducido en este campo excede el límite permitido" );             
            }    
        }
        
        //Se detine la acción submit del formulario si se encuentran errores
        if(!status){
            event.preventDefault();
        }        
        
    });        

});
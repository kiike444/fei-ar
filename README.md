# FEI AR: Sistema de Realidad Aumentada de la Facultad de Estadística e Informática #

Aplicación web que administra la información de las dependencias que será consultada por los estudiantes en sus dispositivos móviles mediante realidad aumentada.

* Versión 1.0

### Requerimientos ###
* PHP 5.6
* MySQL
* Codeigniter 3.0
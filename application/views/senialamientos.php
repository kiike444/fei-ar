<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width">         
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/principal.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/senialamientos.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/senialamientos.js"></script>           
        <title>Señalamientos</title>
    </head>
    <body>
        <header class="container">
            <span id="spanDependencia" class="col-md-6 col-sm-6 col-xs-6">
                <img src="<?php echo base_url(); ?>assets/images/dependencia.png" alt="usuario">
                <label><?php echo $nombreDependencia; ?></label>
            </span>
            <span id="spanAdmin" class="col-md-6 col-sm-6 col-xs-6">
                <img src="<?php echo base_url(); ?>assets/images/usuario.png" alt="usuario">
                <label><?php echo $nombreAdmin; ?></label>
            </span>
        </header>        
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
              <div class="navbar-header">
                  <span class="navbar-brand" id="navbar-brand">FEI AR</span>
<!--                  Menú responsivo-->
                  <span id="menuResponsivo">
                      <div id="btn-editar">
                          <a id="ar-Modificar" href="<?php echo base_url(); ?>index.php/Senialamientos/mandarAModificar/">
                            <img  src="<?php echo base_url(); ?>assets/images/modificar.png" alt="modificar">
                          </a>
                      </div>
                      <div id="div-eliminar">
                          <img  src="<?php echo base_url(); ?>assets/images/eliminar.png" alt="eliminar">                          
                      </div>
                      <div id="btnAgregar">
                          <a href="<?php echo base_url(); ?>index.php/RegistroSenialamiento">
                            <img src="<?php echo base_url(); ?>assets/images/agregar.png" alt="agregar">
                          </a>
                      </div>
                      <div id="btnMenu" >
                            <img src="<?php echo base_url(); ?>assets/images/menu.png" alt="menú">                         
                      </div>
                  </span>
<!--            termina responsivo-->                  
              </div>              
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">Espacios de interés </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/Servicios">Servicios</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>index.php/Eventos">Eventos</a>
                </li>             
                <li class="active">
                    <a href="">Señalamientos</a>
                </li>    
                <li id="cerrarSesionResponsivo">
                    <a href="<?php echo base_url(); ?>index.php/EspaciosDeInteres/cerrarSesion">Cerrar sesión</a>
                </li>                 
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <a id="aModificar" href="<?php echo base_url(); ?>index.php/Senialamientos/mandarAModificar/"> 
                    <button id="btn-editar" type="button" class="btn btn-default navbar-btn">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </button>
                </a>  
                <button id="btn-eliminar" type="button" class="btn btn-default navbar-btn">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
                <a href="<?php echo base_url(); ?>index.php/RegistroSenialamiento">
                    <button id="btnAgregar" type="button" class="btn btn-default navbar-btn">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button> 
                </a>
                <li id="dropToRight" class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menú <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo base_url(); ?>index.php/RegistroPersonal">Personal</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/ServidorDeAvisos">Servidor de avisos</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Administradores">Administradores</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>index.php/EspaciosDeInteres/cerrarSesion">Cerrar sesión</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div id="atrasDeNav"></div>
        <div id="mensaje"></div>
        <div id="dialog" title="Confirmación"></div>        
        <section id="listaSenialamientos" class="container">            
            <?php 
                if(sizeof($senialamientos) == 0){
                    echo '<div class="alert alert-info" role="alert"><strong>¡No hay nada que mostrar!  </strong>  Aún no has registrado señalamientos</div>';
                }
            ?>
            <ul class="list-group">
                <?php 
                            
                    if(sizeof($senialamientos) > 0){
                        $index = 0;
                        foreach($senialamientos as $senialamiento){
                            echo '<li id="li-senialamiento-'.$index.'" class="list-group-item">';
                            
                            echo '<span class="input-group-addon col-md-1 col-sm-1 col-xs-1">';
                            echo '<input type="checkbox" id="check-'.$index.'" data-senialamiento="'.$senialamiento['idEspacioDeInteres'].'">';
                            echo '</span>';                                                                                  
                            
                            echo '<a href="'.base_url().'index.php/Senialamientos/mandarADesripcion/'.$senialamiento['idEspacioDeInteres'].'">';
                            
                            echo '<span class="col-md-5 col-sm-4 col-xs-5"><h2>';
                            echo $senialamiento['nombreEspacio'];
                            echo '</h2></span>';
                            
                            echo '<span class="col-md-2 col-sm-2 col-xs-4"><label id="noIndicaciones">';
                            echo $senialamiento['numIndicaciones'].' indicaciones';
                            echo '</label></span>';
                                                      
                            echo '<span class="col-md-4 col-sm-5 col-xs-3"><label id="fechaRegistro">';
                            $fecha = explode(" ", $senialamiento['fechaRegistro']);
                            echo 'Agregado el '. $fecha[0] . ' a las '. $fecha[1]; //fecha
                            echo '</label></span>';                            
                            
                            echo '<div id="break"></div>'; 
                            
                            echo '</a>';
                            
                            echo '</li>';
                        }
                    }
                ?>
            </ul>
        </section>           
        
    </body>
</html>

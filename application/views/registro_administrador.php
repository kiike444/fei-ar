<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro-administrador.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/registro_administrador.js"></script>
        <title>Registro de administrador</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Login">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Registro de administrador</h1>
        </header>
        <div id="contenedor">
            <section>
                 <?php echo $status;?>
                <form method="POST" id="formRegistroAdministrador" autocomplete="off">
                    <label for="txtNombre">* Nombre completo</label>
                    <input type="text" name="txtNombre" id="txtNombre" value="<?php echo set_value('txtNombre'); ?>" class="form-control" autocomplete="off" maxlength="99">
                    <?php echo form_error('txtNombre', '<span class="error">', '</span>');?>

                    <span id="errorNombre" class="error"></span>
                    
                    <label>* Correo institucional</label>                  
                    <input type="email" name="txtCorreo" id="txtCorreo" value="<?php echo set_value('txtCorreo'); ?>" class="form-control" maxlength="49">
                    <?php echo form_error('txtCorreo', '<span class="error">', '</span>');?>
                    
                    <span id="errorCorreo" class="error"></span>
                    
                    <label>* Contraseña</label>
                    <input type="password" name="txtContrasenia" id="txtContrasenia" value="<?php echo set_value('txtContrasenia'); ?>" class="form-control" maxlength="30">
                    <?php echo form_error('txtContrasenia', '<span class="error">', '</span>');?>     
                    
                    <span id="errorContrasenia" class="error"></span>
                    
                    <label>* Vuelve a escribir tu contraseña</label>
                    <input type="password" name="txtContraseniaVerif" id="txtContraseniaVerif" value="<?php echo set_value('txtContraseniaVerif'); ?>" class="form-control" maxlength="30">
                    <?php echo form_error('txtContraseniaVerif', '<span class="error">', '</span>');?>     
                    
                    <span id="errorVerifContrasenia" class="error"></span>                    

                    <label>* Elige tu dependencia</label>     
                    <select id="selectDependencia" class="form-control">
                    </select>
                    <?php echo form_error('txtNombreDependencia', '<span class="error">', '</span>');?>
                    <input type="hidden" name="txtNombreDependencia" id="txtNombreDependencia" class="form-control" maxlength="99">
                    
                    <span id="errorNombreDependencia" class="error"></span>
                    
                    <label>¿No se encuentra tu dependencia?, Registrala aquí</label>
                    <a href="<?php echo base_url(); ?>index.php/RegistroDependencia">Registrar dependencia</a>
                    
                    <input class="boton btn btn-primary" value="Registrarse" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>
    </body>
</html>

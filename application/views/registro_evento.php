<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.min.css" />        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">          
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro_evento.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/moment.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/bootstrap-timepicker.min.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/registro_evento.js"></script>        
        <title>Agregar evento</title>
    </head>
    <body>
        <header>
            <a href="<?php echo base_url(); ?>index.php/Eventos">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Agregar evento</h1>
        </header>
        <div id="contenedor">
            <section>
                <?php echo $mensaje;?>
                <h3>Información del evento</h3>
                <hr>
                <?php echo form_open_multipart('RegistroEvento');?>
                    <label for="txtNombreEvento">* Nombre del evento</label>
                    <input type="text" name="txtNombreEvento" id="txtNombreEvento" value="<?php if(!$exito){ echo set_value('txtNombreEvento'); } ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombreEvento', '<span class="error">', '</span>');?>

                    <span id="errorNombreEvento" class="error"></span>
                    
                    <label>* Descripción del evento</label>            
                    <textarea name="txtDescripcionEvento" id="txtDescripcionEvento" value="<?php if(!$exito){  echo set_value('txtDescripcionEvento'); } ?>" class="form-control" maxlength="299"></textarea>
                    <?php echo form_error('txtDescripcionEvento', '<span class="error">', '</span>');?>
                    
                    <span id="errorDescripcionEvento" class="error"></span>

                    <label>* Fecha y hora de inicio</label>
                    <div class="container">
                        <div class="row">
                            <div class='col-sm-6'>                        
                                <input type="text" name="txtFechaInicio" id="txtFechaInicio" value="<?php if(!$exito){ echo set_value('txtFechaInicio'); } ?>" class="form-control" placeholder="AAAA/MM/DD HH/MM" maxlength="16">
                            </div>
                         </div>
                    </div>    
                    <?php echo form_error('txtFechaInicio', '<span class="error">', '</span>');?>
                    
                    <span id="errorFechaInicio" class="error"></span>                    

                    <label>* Fecha y hora de conclusión</label>                  
                    <div class="container">
                        <div class="row">
                            <div class='col-sm-6'>                        
                                <input type="text" name="txtFechaFin" id="txtFechaFin" class="form-control" value="<?php if(!$exito){ echo set_value('txtFechaFin'); } ?>" placeholder="AAAA/MM/DD HH/MM" maxlength="16">
                            </div>
                         </div>
                    </div>                        
                    
                    <?php echo form_error('txtFechaFin', '<span class="error">', '</span>');?>
                    
                    <span id="errorFechaFin" class="error"></span>                      
                    
                    <label>Dirección de sitio web relacionado</label>
                    <input type="url" name="txtUrlEvento" id="txtUrlEvento" value="<?php if(!$exito){ echo set_value('txtUrlEvento'); } ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtUrlEvento', '<span class="error">', '</span>');?>     
                    
                    <span id="errorUrl" class="error"></span>
                    
                    <label>* ¿El evento será dentro de la dependencia?</label>
                    <div id="radioGrupo">
                        <input type="radio" name="radio" id="radioSi" value="si"> Si
                        <input type="radio" name="radio" id="radioNo" value="no"> No
                    </div>                    
                    
                    <label id="lbEvtExterno">* Nombre del espacio de interés donde será el evento (debe estar registrado)</label>                  
                    <input type="text" name="txtNombreEspacio" id="txtNombreEspacio" value="<?php if(!$exito){ echo set_value('txtNombreEspacio'); } ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombreEspacio', '<span class="error">', '</span>');?>
                    
                    <span id="errorNombreEspacio" class="error"></span>
                    
                    <div id="evtExterno">
                        <label>* Nombre del lugar donde será el evento</label>                  
                        <input type="text" name="txtNombreLugar" id="txtNombreLugar" value="<?php if(!$exito){ echo set_value('txtNombreLugar'); } ?>" class="form-control" maxlength="99">

                        <span id="errorNombreLugar" class="error"></span>


                        <span class="spanVP">
                            <label class="labelVP">Imagen descriptiva del evento</label>
                            <label>Vista previa</label>
                        </span>                        

                        <input type="file" name="imagenDescriptiva" id="imagenDescriptiva" >
                        <img id="vp1" class="vistaPrevia" src="<?php echo base_url(); ?>assets/images/default.png">     

                        <span id="errorImagenDescriptiva" class="error"></span>

                        <span class="spanVP">
                            <label class="labelVP">Imagen de fondo para el marcador</label>
                            <label>Vista previa</label>
                        </span>

                        <input type="file" name="imagenMarcador" id="imagenMarcador" >
                        <img id="vp2" class="vistaPrevia" src="<?php echo base_url(); ?>assets/images/default.png">
                            

                        <span id="errorImagenMarcador" class="error"></span>                           
                    </div>
                   <?php echo form_error('txtNombreLugar', '<span class="error">', '</span>');?>
                   <?php echo form_error('imagenDescriptiva', '<span class="error">', '</span>');?>
                   <?php echo form_error('imagenMarcador', '<span class="error">', '</span>');?> 
                    
                    <input class="boton btn btn-primary" value="Agregar evento" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>                
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_descripcion.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/descripcion_evento_externo.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>         
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/download.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/descripcion_con_marcador.js"></script>
        <title>Descripción de evento externo</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Eventos">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Descripción de evento externo</h1>
        </header>
        <section>
                <h2><?php echo $evento->nombre ?></h2>
                <hr>
                <div id="divImgDescriptiva">
                    <?php echo "<img id='imgDescriptivaPrincipal' src=\"data:image/jpeg;base64,".base64_encode($evento->imagenDescriptiva).'"/>';?> 
                 </div>
                <div id="contenedorDatos">
                    <h3>Descripción</h3>
                    <p> <?php echo $evento->descripcion ?> </p>
                    <h3>Fecha de inicio</h3>
                    <p> <?php 
                        $fecha = explode(" ", $evento->fechaInicio); 
                        echo 'Inicia el '. $fecha[0] . ' a las '. $fecha[1];
                        ?> 
                    </p>                     
                    <h3>Fecha de conclusión</h3> 
                    <p> <?php 
                        $fecha = explode(" ", $evento->fechaFin); 
                        echo 'Finaliza el '. $fecha[0] . ' a las '. $fecha[1];
                        ?> 
                    </p>                    
                    <h3>Se celebra en</h3>
                    <p> 
                        <?php echo $evento->lugar; ?> 
                    </p>                
                </div>
               <div id="break"></div>
                    <h3>Dirección de sitio web relacionado</h3>
                    <p><?php 
                        if(strlen($evento->urlSitioWeb ) > 0){
                            echo $evento->urlSitioWeb ;
                        }else{
                            echo "No hay un sitio web relacionado";
                        }
                        ?> 
                    </p>
                    <h3>Fecha de registro</h3>
                    <p> <?php 
                        $fecha = explode(" ", $evento->fechaRegistro); 
                        echo 'Agregado el '. $fecha[0] . ' a las '. $fecha[1]; //fecha
                        ?> 
                    </p>                   
                <div id="contentVP">
                    <div id="divImgDescriptiva">
                        <h3>Imagen descriptiva</h3>
                        <?php
                             echo "<img src=\"data:image/jpeg;base64,".base64_encode($evento->imagenDescriptiva).'">';
                        ?> 
                    </div>
                    <div id="divImgMarcador">
                        <h3>Imagen de marcador</h3>
                        <?php
                             echo "<img src=\"data:image/jpeg;base64,".base64_encode($evento->imagenMarcador).'">';
                        ?> 
                    </div>                    
                </div>               
                <div id="break"></div>
                <button id="btnDescarga" type="button" class="btn btn-primary">Descargar marcador</button>
        </section>   
    </body>
</html>

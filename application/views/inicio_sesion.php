<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inicio_sesion.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/inicio_sesion.js"></script>        
        <title>FEI AR - Login</title>
    </head>
    <body>
	<header>
		<h1>FEI AR - Sistema de realidad aumentada</h1>
		<h2>Inicia sesión para acceder</h2>
	</header>
	<section>
		<div id="contenedor">
                    <form method="POST">
                                <?php echo $status;?>
				<input type="email" id="txtCorreo" name="txtCorreo" value="<?php echo set_value('txtCorreo'); ?>"  placeholder="Correo institucional" class="form-control" maxlength="49">
                                <span id="errorCorreo" class="error"></span>
                                <?php echo form_error('txtCorreo', '<span class="error">', '</span>');?>   
                                <input type="password" id="txtContrasenia" name="txtContrasenia" value="<?php echo set_value('txtContrasenia'); ?>" placeholder="Contraseña" class="form-control" maxlength="30">
                                <span id="errorContrasenia" class="error"></span>
                                <?php echo form_error('txtContrasenia', '<span class="error">', '</span>');?>   
				<input type="submit" value="Entrar" class="btn btn-primary">
			</form>
		</div>
		<a href="<?php echo base_url(); ?>index.php/RegistroAdministrador">Registrarse</a> 
	</section>
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_descripcion.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/descripcion_senialamiento.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>         
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>         
        <title>Descripción de Señalamiento</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Senialamientos">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Descripción de señalamiento</h1>
        </header>          
        <div id="contenedor" >
            <section>
                <h2><?php echo 'Señalamiento en '. $senialamiento->nombre ?></h2>
                <hr>
                <div class="col-md-6 col-sm-6 col-xs-6" id="divImgDescriptiva">
                    <?php
                        echo "<img src=\"data:image/jpeg;base64,".base64_encode($senialamiento->imagenDescriptiva).'"/>';
                    ?> 
                 </div>
                <div class="col-md-6 col-sm-6 col-xs-6" id="contenedorDatos">
                    <?php 
                    if(sizeof($indicaciones) > 0){
                        foreach($indicaciones as $indicacion){
                            echo '<div class="row">';
                            
                                if(strcmp($indicacion->orientacion, 'arriba') == 0){
                                    echo '<img src="'.base_url().'assets/images/arriba.png" alt="arriba">';
                                }

                                if(strcmp($indicacion->orientacion, 'arriba-derecha') == 0){
                                    echo '<img src="'.base_url().'assets/images/arriba-derecha.png" alt="arriba-derecha">';
                                }

                                if(strcmp($indicacion->orientacion, 'derecha') == 0){
                                    echo '<img src="'.base_url().'assets/images/derecha.png" alt="derecha">';
                                }

                                if(strcmp($indicacion->orientacion, 'abajo-derecha') == 0){
                                    echo '<img src="'.base_url().'assets/images/abajo-derecha.png" alt="abajo-derecha">';
                                }

                                if(strcmp($indicacion->orientacion, 'abajo') == 0){
                                    echo '<img src="'.base_url().'assets/images/abajo.png" alt="abajo">';
                                }

                                if(strcmp($indicacion->orientacion, 'abajo-izquierda') == 0){
                                    echo '<img src="'.base_url().'assets/images/abajo-izquierda.png" alt="abajo-izquierda">';
                                }

                                if(strcmp($indicacion->orientacion, 'izquierda') == 0){
                                    echo '<img src="'.base_url().'assets/images/izquierda.png" alt="izquierda">';
                                }

                                if(strcmp($indicacion->orientacion, 'arriba-izquierda') == 0){
                                    echo '<img src="'.base_url().'assets/images/arriba-izquierda.png" alt="arriba-izquierda">';
                                }
                                                     
                                echo '<label>'.$indicacion->descripcion.'</label>';  
                            echo "</div>";
                        }
                    }                    
                    ?>
               </div>     
               <div id="break"></div>
            </section>
        </div>    
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/servidor_de_avisos.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>   
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>          
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>     
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/servidor_de_avisos.js"></script>
        <title>Serviror de avisos</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Servidor de avisos vinculado a la dependencia</h1>
        </header>
        <?php echo $mensaje;?>
        <div id="contenedor">
            <section>
                <h2>Servidor de avisos</h2>
                <hr>
                <form method="POST" id="formRegistroServicio">
                    <label>Url del servidor de avisos</label>                  
                    <input type="url" name="txtUrl" id="txtUrl" value="<?php if(!$exito){ echo set_value('txtUrl');} ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtUrl', '<span class="error">', '</span>');?>
                    
                    <span id="errorUrl" class="error"></span>
                    
                    <input class="boton btn btn-primary" value="Guardar cambios" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>         
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_descripcion.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/descripcion_servicio.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>         
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>        
        <title>Descripción de Servicio</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Servicios">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Descripción de servicio</h1>
        </header>    
        <div id="contenedor">
            <section>
                <h2><?php echo $servicio->nombre ?></h2>
                <hr>
                <div id="divImgDescriptiva">
                    <?php
                        if(isset($servicio->nombreEspacio)){
                           echo "<img src=\"data:image/jpeg;base64,".base64_encode($servicio->imagenDescriptiva).'"/>';
                        }else{
                           echo '<img src="'.base_url().'assets/images/noasignado.png" alt="No asignado">';
                        }
                    ?> 
                 </div>
                <div id="contenedorDatos">
                    <h3>Descripción</h3>
                    <p> <?php echo $servicio->descripcion ?> </p>
                    <h3>Responsable</h3>
                    <p> <?php 
                        if(isset($servicio->nombreResponsable)){
                            echo $servicio->nombreResponsable;
                        }else{
                            echo 'No asignado aún' ;
                        }                        
                        ?> 
                    </p>        
                    <h3>Espacio de interés</h3>
                    <p> 
                        <?php 
                        if(isset($servicio->nombreEspacio)){
                            echo $servicio->nombreEspacio;
                        }else{
                            echo 'No asignado aún' ;
                        }
                        ?> 
                    </p>                      
                </div>
               <div id="break"></div>
            </section>
        </div>               
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro-servicio.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>         
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/modificar_servicio.js"></script>           
        <title>Modificar servicio</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Servicios">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Modificar un servicio</h1>
        </header>
        <div id="contenedor" class="container">
            <section>
                <form method="POST" id="formModificarServicio">
                    <label for="txtNombreEspacio">* Espacio de interés (debe estar registrado)</label>
                    <input type="text" name="txtNombreEspacio" id="txtNombreEspacio" value="<?php echo set_value('txtNombreEspacio'); ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombreEspacio', '<span class="error">', '</span>');?>

                    <span id="errorNombreEspacio" class="error"></span>
                    
                    <label>* Nombre del servicio</label>                  
                    <input type="text" name="txtNombre" id="txtNombre" value="<?php echo set_value('txtNombre'); ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombre', '<span class="error">', '</span>');?>
                    
                    <span id="errorNombre" class="error"></span>
                    
                    <label>* Descripción del servicio</label>
                    <textarea name="txtDescripcion" id="txtDescripcion" value="<?php echo set_value('txtDescripcion'); ?>" class="form-control" maxlength="299"></textarea>
                    <?php echo form_error('txtDescripcion', '<span class="error">', '</span>');?>     
                    
                    <span id="errorDescripcion" class="error"></span>
                    
                    <label>* Nombre del responsable (debe estar registrado)</label>
                    <input type="text" name="txtNombreResponsable" id="txtNombreResponsable" value="<?php echo set_value('txtNombreResponsable'); ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombreResponsable', '<span class="error">', '</span>');?>     
                    
                    <span id="errorNombreResponsable" class="error"></span>                    

                    <input class="boton btn btn-primary" value="Guardar cambios" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>        

    </body>
</html>

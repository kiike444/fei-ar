<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro-dependencia.css">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/mapa.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/registro_dependencia.js"></script>
        <title>Registro de dependencia</title>
    </head>
    <body>
        <header>
            <a href="<?php echo base_url(); ?>index.php/RegistroAdministrador">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Registro de dependencia</h1>
        </header>
        <div id="contenedor">
            <section>
                <?php echo $status;?>
                <form method="POST" id="formRegistroDependencia">
                    <label for="txtNombre">* Nombre de la dependencia</label>
                    <input type="text" name="txtNombre" id="txtNombre" value="<?php if(!$exito){ echo set_value('txtNombre');} ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombre', '<span class="error">', '</span>');?>
                    <span id="errorNombre" class="error"></span>
                    <label>Cubra la zona geográfica de su dependencia con el poligono verde, 
                        las zonas azules son zonas de dependencias ya registradas</label>
                    <div id="mapa"></div>
                    <input type="text" name="txtCoordenadas" id="txtCoordenadas" maxlength="49">
                    <?php echo form_error('txtCoordenadas', '<span class="error">', '</span>');?>
                    <label>* ¿Su institución cuenta con el sistema de avisos?</label>
                    <div id="radioGrupo">
                        <input type="radio" name="radio" id="radioSi" value="si"> Si
                        <input type="radio" name="radio" id="radioNo" value="no"> No
                    </div>
                    <label>URL del servidor de avisos</label>
                    <input type="url" name="txtUrl" id="txtUrl" value="<?php if(!$exito){ echo set_value('txtUrl'); } ?>" class="form-control" maxlength="99">
                    <span id="errorUrl" class="error"></span>
                    <?php echo form_error('txtUrl', '<span class="error">', '</span>');?>
                    <input class="boton btn btn-primary" value="Registrar dependencia" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>
    </body>
</html>

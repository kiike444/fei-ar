<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <meta content="True" name="HandheldFriendly">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="viewport" content="width=device-width"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro_espacio.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/modificar_espacio.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/modificar_espacio.js"></script>        
        <title>Modificar espacio de interés</title>
    </head>
    <body>
        <header>
            <a href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Modificar espacio de interés</h1>
        </header>
        <div id="contenedor">
            <section>
                <?php echo $status;?>
                <?php echo form_open_multipart('ModificarEspacioDeInteres');?>
                    <label for="txtNombre">* Nuevo nombre del espacio de interés</label>
                    <input type="text" name="txtNombre" id="txtNombre" value="<?php if(!$exito){ echo set_value('txtNombre'); } ?>" class="form-control" maxlength="99">
                    <?php echo form_error('txtNombre', '<span class="error">', '</span>');?>

                    <span id="errorNombre" class="error"></span>
                    
                    <label>* Nueva descripción del espacio de interés</label>
                    <textarea name="txtDescripcion" id="txtDescripcion" value="<?php if(!$exito){  echo set_value('txtDescripcion'); } ?>" class="form-control" maxlength="299"></textarea>
                    <?php echo form_error('txtDescripcion', '<span class="error">', '</span>');?>
                    
                    <span id="errorDescripcion" class="error"></span>
                    
                    <label>Nueva dirección de sitio web relacionado</label>
                    <input type="url" name="txtUrl" id="txtUrl" value="<?php if(!$exito){ echo set_value('txtUrl'); } ?>" class="form-control" maxlength="99">
                    <span id="errorUrl" class="error"></span>
                    <?php echo form_error('txtUrl', '<span class="error">', '</span>');?>
      
                    <span class="spanVP">
                        <label class="labelVP">Nueva imagen descriptiva del lugar de interés</label>
                        <label class="labelImgNueva">Vista previa</label>
                        <label>Imagen actual</label>
                    </span>                        
                        
                    <input type="file" name="imagenDescriptiva" id="imagenDescriptiva"  >
                    <img id="vp1" class="vistaPrevia imgEscondida">
                    <img id="imgactual1">
                    <?php echo form_error('imagenDescriptiva', '<span class="error">', '</span>');?>     
                    
                    <span id="errorImagenDescriptiva" class="error"></span>
                   
                    <span class="spanVP">
                        <label class="labelVP">Nueva imagen de fondo para el marcador</label>
                        <label class="labelImgNueva">Vista previa</label>
                        <label>Imagen actual</label>
                    </span>
                    
                    <input type="file" name="imagenMarcador" id="imagenMarcador" >
                    <img id="vp2" class="vistaPrevia imgEscondida">
                    <img id="imgactual2">
                    <?php echo form_error('imagenMarcador', '<span class="error">', '</span>');?>     
                    
                    <span id="errorImagenMarcador" class="error"></span>                    
                    
                    
                    <input class="boton btn btn-primary" value="Guardar cambios" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div> 
    </body>
</html>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_descripcion.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/descripcion_espacio.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/download.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/descripcion_con_marcador.js"></script>                      
        <title>Descripción Espacio de Interés</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Descripción de espacio de interés</h1>
        </header>
        <div id="contenedor" >
            <section>
                <h2><?php echo $espacio->nombre ?></h2>
                <hr>
                <h3>Descripción</h3>
                <p> <?php echo $espacio->descripcion ?> </p>
                <h3>Dirección de sitio web</h3>
                <p> 
                    <?php 
                        if(strlen($espacio->urlSitioWeb ) > 0){
                            echo $espacio->urlSitioWeb ;
                        }else{
                            echo "No hay un sitio web relacionado";
                        }
                    ?> 
                </p>
                <div id="contentVP">
                    <div id="divImgDescriptiva">
                        <h3>Imagen descriptiva</h3>
                        <?php
                             echo "<img src=\"data:image/jpeg;base64,".base64_encode($espacio->imagenDescriptiva).'">';
                        ?> 
                    </div>
                    <div id="divImgMarcador">
                        <h3>Imagen de marcador</h3>
                        <?php
                             echo "<img src=\"data:image/jpeg;base64,".base64_encode($espacio->imagenMarcador).'">';
                        ?> 
                    </div>                    
                </div>
                <div id="break"></div>
                <hr>
                <h3>Servicios</h3>
                <?php 
                    if($servicios != NULL){
                        echo '<div class="panel panel-default">';
                        echo '<table  class="table">';
                        echo '<thead>';
                        echo '<th>#</th>';
                        echo '<th>Nombre</th>';
                        echo '<th>Responsable</th>';
                        echo '</thead>';
                            $cont = 1;
                            foreach ($servicios as $servicio){
                                echo '<tr>';
                                echo '<td>'.$cont.'</td>';
                                echo '<td>'.$servicio->nombre.'</td>';
                                echo '<td>'.$servicio->nombreResponsable.'</td>';
                                echo '</tr>';
                                $cont++;
                             }
                        echo '</table>';
                        echo '</div>';
                    }else{
                        echo "<p>No hay servicios relacionados</p>";
                    }
                ?>
                <hr>
                <h3>Eventos</h3>  
                <?php 
                    if($eventos != NULL){
                        echo '<div class="panel panel-default">';
                        echo '<table  class="table">';
                        echo '<thead>';
                        echo '<th>#</th>';
                        echo '<th>Nombre</th>';
                        echo '<th>Fecha de inicio</th>';
                        echo '<th>Fecha de conclusión</th>';
                        echo '</thead>';
                            $cont = 1;
                            foreach ($eventos as $evento){
                                echo '<tr>';
                                echo '<td>'.$cont.'</td>';
                                echo '<td>'.$evento->nombre.'</td>';
                                echo '<td>'.$evento->fechaInicio.'</td>';
                                echo '<td>'.$evento->fechaFin.'</td>';
                                echo '</tr>';
                                $cont++;
                             }
                        echo '</table>';
                        echo '</div>';
                    }else{
                        echo "<p>No hay eventos relacionados</p>";
                    }
                ?>                
                <hr>
                <button id="btnDescarga" type="button" class="btn btn-primary">Descargar marcador</button>
            </section>
        </div>        
    </body>
</html>

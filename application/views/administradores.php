<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/administradores.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>   
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>          
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>        
        <title>Administradores</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Administradores de la dependencia</h1>
        </header>
        <div id="contenedor">
            <section>
                <h2>Administradores</h2>
                <hr>
                <?php 
                    if($administradores != NULL){
                        echo '<div class="panel panel-default">';
                        echo '<table  class="table">';
                        echo '<thead>';
                        echo '<th>#</th>';
                        echo '<th>Nombre</th>';
                        echo '<th>Correo</th>';
                        echo '</thead>';
                            $cont = 1;
                            foreach ($administradores as $administrador){
                                echo '<tr>';
                                echo '<td>'.$cont.'</td>';
                                echo '<td>'.$administrador->nombre.'</td>';
                                echo '<td>'.$administrador->correoInstitucional.'</td>';
                                echo '</tr>';
                                $cont++;
                             }
                        echo '</table>';
                        echo '</div>';
                    }else{
                        echo "<p>No hay administradores</p>";
                    }
                ?>
            </section>
        </div>         
    </body>
</html>

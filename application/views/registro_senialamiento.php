<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro_senialamiento.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>   
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>          
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/registro_senialamiento.js"></script>         
        <title>Agregar señalamiento</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/Senialamientos">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Agregar señalamiento</h1>
        </header>
        <div id="contenedor" class="container">
            <section>
                <div id="mensaje"></div>
                <?php echo $mensaje;?>                
                <h3>Información del señalamiento</h3>
                <hr>
                <form method="POST" id="formRegistroSenialamiento">
                    <div id="contenedorNombreEspacio" class="container">
                        <label>Espacio de interés donde estará el señalamiento (debe estar registrado)</label>
                        <input type="text" name="txtNombreEspacio" id="txtNombreEspacio" value="<?php if(!$exito){ echo set_value('txtNombreEspacio'); } ?>" class="form-control" maxlength="99">  
                        <div id="break"></div>
                        <?php echo form_error('txtNombreEspacio', '<span class="error">', '</span>');?>
                        <span id="errorNombreEspacio" class="error"></span>        
                    </div>                 

                    <div class="container orientacion">
                        <label id="lbOrientacion">Orientación</label>
                        <label id="lbDescripcion">Descripción</label>
                        <div id="break"></div>
                        <img src="<?php echo base_url(); ?>assets/images/arriba.png" alt="arriba">                    
                        <input type="text" name="txtArriba" id="txtArriba" value="<?php if(!$exito){ echo set_value('txtArriba'); } ?>" class="form-control" placeholder="lugar 1, lugar 2, lugar 3" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtArriba', '<span class="error">', '</span>');?>                
                    </div>
        
                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/arriba-derecha.png" alt="arriba-derecha">                     
                        <input type="text" name="txtArribaDerecha" id="txtArribaDerecha" value="<?php if(!$exito){ echo set_value('txtArribaDerecha'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtArribaDerecha', '<span class="error">', '</span>');?>                    
                    </div>

                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/derecha.png" alt="derecha">                    
                        <input type="text" name="txtDerecha" id="txtDerecha" value="<?php if(!$exito){ echo set_value('txtDerecha'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtDerecha', '<span class="error">', '</span>');?>                 
                    </div>
                    
                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/abajo-derecha.png" alt="abajo-derecha">
                        <input type="text" name="txtAbajoDerecha" id="txtAbajoDerecha" value="<?php if(!$exito){ echo set_value('txtAbajoDerecha'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtAbajoDerecha', '<span class="error">', '</span>');?>                      
                    </div>
                    
                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/abajo.png" alt="abajo">
                        <input type="text" name="txtAbajo" id="txtAbajo" value="<?php if(!$exito){ echo set_value('txtAbajo'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtAbajo', '<span class="error">', '</span>');?>                    
                    </div>
                    
                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/abajo-izquierda.png" alt="abajo-izquierda">
                        <input type="text" name="txtAbajoIzquierda" id="txtAbajoIzquierda" value="<?php if(!$exito){ echo set_value('txtAbajoIzquierda'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtAbajoIzquierda', '<span class="error">', '</span>');?>
                    </div>                        
                    
                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/izquierda.png" alt="izquierda">
                        <input type="text" name="txtIzquierda" id="txtIzquierda" value="<?php if(!$exito){ echo set_value('txtIzquierda'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtIzquierda', '<span class="error">', '</span>');?>                       
                    </div>                     

                    <div class="container orientacion">
                        <img src="<?php echo base_url(); ?>assets/images/arriba-izquierda.png" alt="arriba-izquierda">                    
                        <input type="text" name="txtIzquierdaArriba" id="txtIzquierdaArriba" value="<?php if(!$exito){ echo set_value('txtIzquierdaArriba'); } ?>" class="form-control" maxlength="299">
                        <div id="break"></div>
                        <?php echo form_error('txtIzquierdaArriba', '<span class="error">', '</span>');?>
                    </div>                       
                    
                    <span id="errorOrientaciones" class="error"></span>  

                    <input class="boton btn btn-primary" value="Agregar señalamiento" type="submit" id="btnRegistrar" >
                </form>
            </section>
        </div>          
    </body>
</html>

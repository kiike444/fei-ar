<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <meta name="viewport" content="maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general_registro.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/registro_personal.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/js/bootstrap.min.js"></script>   
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-ui-1.11.4/jquery-ui.js"></script>          
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/ruta.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/registro_responsable.js"></script>        
        <title>Personal</title>
    </head>
    <body>
        <header>
            <a class="fAtras" href="<?php echo base_url(); ?>index.php/EspaciosDeInteres">
                <img src="<?php echo base_url(); ?>assets/images/atras.png" alt="Atrás">
            </a>
            <h1>Personal de la dependencia</h1>
        </header>
        <div id="mensaje"></div>
        <div id="dialog" title="Confirmación"></div>
        <div id="contenedor">
            <section>
                <?php echo $mensaje;?>
                <h2>Agregar personal</h2>
                <hr>
                <h3>Modo básico</h3>
                <form method="POST" id="formRegistroResponsableBasico">
                    <label for="txtNombre">* Nombre completo</label>
                    <input type="text" name="txtNombre" id="txtNombre" value="<?php if(!$exito){ echo set_value('txtNombre'); } ?>" class="form-control" autocomplete="off" maxlength="99">
                    <?php echo form_error('txtNombre', '<span class="error">', '</span>');?>

                    <span id="errorNombre" class="error"></span>
                    
                    <label>* Correo eléctronico</label>                  
                    <input type="email" name="txtCorreo" id="txtCorreo" value="<?php if(!$exito){ echo set_value('txtCorreo'); } ?>" class="form-control" maxlength="49">
                    <?php echo form_error('txtCorreo', '<span class="error">', '</span>');?>
                    
                    <span id="errorCorreo" class="error"></span>
                    
                    <input class="boton btn btn-primary" value="Agregar" type="submit" id="btnRegistrarBasico" >
                </form>
                <h3>Modo avanzado</h3>
                <form method="POST" id="formRegistroResponsableAvanzado">
                    <label for="txtUrl">* Url de sitio de personal académico</label>
                    <input type="text" name="txtUrl" id="txtUrl" value="<?php if(!$exito){ echo set_value('txtUrl'); } ?>" class="form-control" autocomplete="off">
                    <?php echo form_error('txtUrl', '<span class="error">', '</span>');?>

                    <span id="errorUrl" class="error"></span>
                    
                    <input class="boton btn btn-primary" value="Analizar" type="submit" id="btnRegistrarAvanzado" >
                </form>     
                <hr>
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Personal registrado
                    
                        <span id="menuTabla">
                            <button id="btn-eliminar" type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>
                            <button type="button" id="btn-mostrar" class="btn btn-default" data-toggle="collapse" data-target="#tablaPersonal">Ocultar</button>
                            <button type="button" id="btnBuscar" class="btn btn-default">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </button>
                            <input type="text" id="txtBuscar" class="form-control">
                            
                        </span>
                        <div id="break"></div>
                    </div>

                    <!-- Table -->
                    <table id="tablaPersonal" class="table collapse in">
                        <thead>
                        <th>Selección</th>
                        <th>Número</th>
                        <th>Nombre</th>
                        <th>Correo electrónico</th>
                        </thead>
                        <?php 
                            if($responsables != NULL){
                                $cont = 1;
                                foreach ($responsables as $responsable){
                                    echo '<tr>';
                                    echo '<td><input type="checkbox" value="'.$responsable->correoInstitucional.'"></td>';
                                    echo '<td>'.$cont.'</td>';
                                    echo '<td>'.$responsable->nombre.'</td>';
                                    echo '<td>'.$responsable->correoInstitucional.'</td>';
                                    echo '</tr>';
                                    $cont++;
                                }
                            }
                        ?>
                        
                    </table>
               </div>
            </section>
        </div>        
    </body>
</html>

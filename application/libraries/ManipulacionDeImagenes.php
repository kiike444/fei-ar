<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManipulacionDeImagenes
 *
 * @author Enrique
 */
class ManipulacionDeImagenes {
    
    public function unirImagenes($rutaImgDestino, $rutaImgMarcador){
        $infoImagen = getimagesize($rutaImgDestino);
        $imagenTipo = $infoImagen['mime'];         
        $imgDestino=NULL;
        
        switch ( $imagenTipo ){
                case "image/jpg":
                case "image/jpeg":
                        $imgDestino = imagecreatefromjpeg( $rutaImgDestino );
                        break;
                case "image/png":
                        $imgDestino = imagecreatefrompng( $rutaImgDestino );
                        break;
                default:
                        return false;
        }         
        
        $imgMarcador = imagecreatefrompng($rutaImgMarcador);
        $result = imagecopymerge($imgDestino, $imgMarcador, 0, 0, 0, 0, 276, 276, 100); 
        imagepng($imgDestino,$rutaImgDestino);
        imagedestroy($imgDestino);
        imagedestroy($imgMarcador);
        
        return $result;
    }
    
    public function escalar($rutaImagen){
        $anchoDestino = 276;
        $altoDestino = 276;
        $infoImagen = getimagesize($rutaImagen);
        $imagenAncho = $infoImagen[0];
        $imagenAlto = $infoImagen[1];
        $imagenTipo = $infoImagen['mime'];     
        $imgActual=NULL;
        
        switch ( $imagenTipo ){
                case "image/jpg":
                case "image/jpeg":
                        $imgActual = imagecreatefromjpeg( $rutaImagen );
                        break;
                case "image/png":
                        $imgActual = imagecreatefrompng( $rutaImagen );
                        break;
                default:
                        return false;                    
        }        
        
        $imgDestino = imagecreatetruecolor( $anchoDestino, $altoDestino);
        $result = imagecopyresampled($imgDestino, $imgActual, 0, 0, 0, 0, $anchoDestino, $altoDestino, $imagenAncho, $imagenAlto);     
        imagepng($imgDestino,$rutaImagen);
        imagedestroy($imgDestino);
        imagedestroy($imgActual);
        return $result;
    }
    
}

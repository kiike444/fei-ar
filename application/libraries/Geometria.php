<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Geometria
 *
 * @author Enrique
 */
class Geometria {
    
    public function puntoEnRectanculo($lat_lo, $lng_lo, $lat_hi, $lng_hi, $pointX, $pointY){
        if($pointX >= $lng_lo && $pointX <= $lng_hi && $pointY >= $lat_lo && $pointY <= $lat_hi){
            return true;
        }else{
            return false;
        }
    }
    
    public function rectangulosSolapados($rectanculo1, $rectangulo2){        
        list($r1a,$r1b,$r1c,$r1d) = explode(",", $rectanculo1);
        list($r2a,$r2b,$r2c,$r2d) = explode(",", $rectangulo2);
        
        $r1_lat_lo = floatval ($r1a);
        $r1_lng_lo = floatval ($r1b);
        $r1_lat_hi = floatval ($r1c);
        $r1_lng_hi = floatval ($r1d);
        
        $r2_lat_lo = floatval ($r2a);
        $r2_lng_lo = floatval ($r2b);
        $r2_lat_hi = floatval ($r2c);
        $r2_lng_hi = floatval ($r2d);

    
        if($this->puntoEnRectanculo($r1_lat_lo, $r1_lng_lo, $r1_lat_hi, $r1_lng_hi, $r2_lng_hi, $r2_lat_lo )){
            return true;
        }
        
        if($this->puntoEnRectanculo($r1_lat_lo, $r1_lng_lo, $r1_lat_hi, $r1_lng_hi, $r2_lng_hi, $r2_lat_hi )){
            return true;
        }        
        
        if($this->puntoEnRectanculo($r1_lat_lo, $r1_lng_lo, $r1_lat_hi, $r1_lng_hi,$r2_lng_lo, $r2_lat_lo )){
            return true;
        }        
        
        if($this->puntoEnRectanculo($r1_lat_lo, $r1_lng_lo, $r1_lat_hi, $r1_lng_hi, $r2_lng_lo, $r2_lat_hi)){
            return true;
        }       

        
       if($this->puntoEnRectanculo($r2_lat_lo, $r2_lng_lo, $r2_lat_hi, $r2_lng_hi, $r1_lng_hi, $r1_lat_lo )){
            return true;
        }
        
        if($this->puntoEnRectanculo($r2_lat_lo, $r2_lng_lo, $r2_lat_hi, $r2_lng_hi, $r1_lng_hi, $r1_lat_hi )){
            return true;
        }        
        
        if($this->puntoEnRectanculo($r2_lat_lo, $r2_lng_lo, $r2_lat_hi, $r2_lng_hi,$r1_lng_lo, $r1_lat_lo )){
            return true;
        }        
        
        if($this->puntoEnRectanculo($r2_lat_lo, $r2_lng_lo, $r2_lat_hi, $r2_lng_hi, $r1_lng_lo, $r1_lat_hi)){
            return true;
        }               
     
        return false;
    }
}

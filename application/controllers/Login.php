<?php

class Login extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');
        $this->load->model('administrador_model');
    }
    
    public function index(){
        if ($this->session->userdata('correo')){
          redirect('EspaciosDeInteres');
        }        
        
        $this->form_validation->set_rules('txtCorreo', 'Correo', 'required|min_length[9]|max_length[50]|callback_correo_check');
        $this->form_validation->set_rules('txtContrasenia', 'Contraseña', 'required|min_length[1]|max_length[64]');

        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Escriba un correo con formato correo@uv.mx');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
     
        if($this->form_validation->run() == FALSE){
            $data['status'] = "";
            $this->load->view("inicio_sesion",$data);
        }else{
            $correo = $this->input->post("txtCorreo");
            $contrasenia = $this->input->post("txtContrasenia");
            //Se encripta la contraseña
            $contrasenia=hash('sha256', $contrasenia);
            //Se verifica que la cuenta sea válida
            if(!$this->validarCuenta($correo,$contrasenia)){
                $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!    </strong>El usuario o contraseña no son correctos</div>";
                $this->load->view("inicio_sesion",$data); 
            }else{
                //Se crean los datos para la sesión
                $idDependencia = $this->administrador_model->getIdDependenciaPorCorreo($correo);
                if($idDependencia!=NULL){
                    $nombreDependencia = $this->dependencia_model->getNombrePorId($idDependencia);
                    if($nombreDependencia!=NULL){
                        $datosSesion = array(
                                   'nombreDependencia'  => $nombreDependencia,
                                   'correo'     => $correo
                                );
                        $this->session->set_userdata($datosSesion);
                        redirect('EspaciosDeInteres'); 
                    }
                }                         
            }

        }        
    }

    /*
     * Función que valida el campo correo mediante FORM VALIDATION
     */
    function correo_check($correo){
        $result = filter_var($correo, FILTER_VALIDATE_EMAIL);
        
        if (!$result){
            $this->form_validation->set_message('correo_check', 'Ingrese un correo con formato correo@uv.mx');
            return FALSE;
        }
        else{
            $pos = strrpos($correo, "@uv.mx");
            if ($pos === false) {
                $this->form_validation->set_message('correo_check', 'Ingrese un correo con formato correo@uv.mx');
                return FALSE;
            }
            return TRUE;
        }
    }        
 
    /*
     * Función que recibe un correo y una contraseña y verifica que correspondan
     * a una cuenta registrada.
     */    
    function validarCuenta($correo,$contrasenia){
        $result = $this->administrador_model->verificarCuenta($correo,$contrasenia);
        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
}

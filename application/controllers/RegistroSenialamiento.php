<?php


class RegistroSenialamiento extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');        
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');
        $this->load->model('senialamiento_model');    
        $this->load->model('indicacion_model'); 
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }        
        
        $data['exito'] = false;
        
        $this->form_validation->set_rules('txtNombreEspacio', 'Nombre del espacio de interés',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check');
        $this->form_validation->set_rules('txtArriba', 'Orientación arriba',  'max_length[300]');       
        $this->form_validation->set_rules('txtArribaDerecha', 'Orientación arriba derecha',  'max_length[300]'); 
        $this->form_validation->set_rules('txtDerecha', 'Orientación derecha',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajoDerecha', 'Orientación derecha abajo',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajo', 'Orientación abajo',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajoIzquierda', 'Orientación abajo iaquierda',  'max_length[300]'); 
        $this->form_validation->set_rules('txtIzquierda', 'Orientación izquierda',  'max_length[300]'); 
        $this->form_validation->set_rules('txtIzquierdaArriba', 'Orientación izquierda arriba',  'max_length[300]'); 

        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        
        if($this->form_validation->run() == FALSE){
            $data['mensaje'] = "";
            $this->load->view("registro_senialamiento",$data);
        }else{
            $nombreEspacio =  $this->input->post("txtNombreEspacio");
            $arriba = $this->input->post("txtArriba");
            $arribaDerecha = $this->input->post("txtArribaDerecha");
            $derecha = $this->input->post("txtDerecha");
            $derechaAbajo = $this->input->post("txtAbajoDerecha");
            $abajo = $this->input->post("txtAbajo");
            $abajoIzquerda = $this->input->post("txtAbajoIzquierda");
            $izquierda = $this->input->post("txtIzquierda");
            $izquierdaArriba = $this->input->post("txtIzquierdaArriba");
            
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);          
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];
            $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
            
            if(strlen($arriba) > 0 || strlen($arribaDerecha) > 0 || strlen($derecha) > 0 || strlen($derechaAbajo) > 0 || 
               strlen($abajo) > 0 || strlen($abajoIzquerda) > 0 || strlen($izquierda) > 0 || strlen($izquierdaArriba) > 0){
                
               $this->senialamiento_model->guardar($fechaRegistro, $idEspacioDeInteres ,$idDependencia);
               
                //Se registra la actualización
                $this->actualizacion_model->senialamientosActualizados($idDependencia, $fechaRegistro);                
               
               $idSenialamiento=$this->senialamiento_model->getIdSenialamiento($idEspacioDeInteres, $idDependencia);
               
               if(strlen($arriba) > 0){
                   $this->indicacion_model->guardar($arriba, "arriba", $idSenialamiento ,$idDependencia); 
               }

               if(strlen($arribaDerecha) > 0){
                   $this->indicacion_model->guardar($arribaDerecha, "arriba-derecha", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($derecha) > 0){
                   $this->indicacion_model->guardar($derecha, "derecha", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($derechaAbajo) > 0){
                   $this->indicacion_model->guardar($arriba, "abajo-derecha", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($abajo) > 0){
                   $this->indicacion_model->guardar($abajo, "abajo", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($abajoIzquerda) > 0){
                   $this->indicacion_model->guardar($abajoIzquerda, "abajo-izquierda", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($izquierda) > 0){
                   $this->indicacion_model->guardar($izquierda, "izquierda", $idSenialamiento ,$idDependencia); 
               }
               
               if(strlen($izquierdaArriba) > 0){
                   $this->indicacion_model->guardar($izquierdaArriba, "arriba-izquierda", $idSenialamiento ,$idDependencia); 
               } 
               
               $data['exito'] = true;
               
                $data['mensaje'] = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" "
                             . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                             . "</button><strong>¡Éxito!  </strong>El señalamiento se agregó correctamente</div>";              
               
            }else{
                $data['mensaje'] = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" "
                             . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                             . "</button><strong>¡Algo salió mal!  </strong>Debes ingresar almenos una orientación</div>"; 
            }
            
            $this->load->view("registro_senialamiento",$data);   
        }          
    }
    

    /*
     * Función que valida el campo nombre de espacio de interés mediante FORM VALIDATION
     */    
    public function nombreEspacio_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);
        
        if(!$result){
            $this->form_validation->set_message('nombreEspacio_check', 'El nombre del espacio de interés no está registrado');
            return FALSE;     
        }else{
            $result = $this->espacio_de_interes_model->tieneSenialamiento($nombre, $idDependencia);
            if($result){
                $this->form_validation->set_message('nombreEspacio_check', 'El espacio de interés ya cuenta con un señalamiento');
                return FALSE;                 
            }
            return TRUE;
        }
    }     
    
    
}

<?php

class Senialamientos extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');  
        $this->load->model('espacio_de_interes_model');
        $this->load->model('senialamiento_model');  
        $this->load->model('indicacion_model'); 
        $this->load->model('administrador_model'); 
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            
            $correo = $this->session->userdata('correo');
            $nombreAdmin = $this->administrador_model->getNombreAdministradorPorCorreo($correo);
            $data["nombreAdmin"] = $nombreAdmin;
            $data["nombreDependencia"] = $nombreDependencia;            
            
            $aux = new stdClass();
            $auxarray = array();
            
            $senialamientos = $this->senialamiento_model->getSenialamientosDeDependencia($idDependencia);
            
            
            if(count($senialamientos) > 0){
                $count = 0;
                foreach ($senialamientos as $senialamiento)
                {
                   $idEspacioDeInteres = $senialamiento->idEspacioDeInteres;
                   $nombreEspacio = $senialamiento->nombre;
                   $idSenialamiento = $senialamiento->idSenialamiento;
                   $fechaRegistro = $senialamiento->fechaRegistro;
                   $indicaciones = $this->indicacion_model->getIndicacionesDeSenialamiento($idSenialamiento ,$idDependencia);
                   $numIndicaciones = count($indicaciones);
                   $auxarray[$count] = array('idEspacioDeInteres'=>$idEspacioDeInteres ,'nombreEspacio'=>$nombreEspacio, 'numIndicaciones'=>$numIndicaciones, 'fechaRegistro'=>$fechaRegistro);
                   $count++;
                }                    
            }
            
            $data["senialamientos"] = $auxarray;
            
            $this->load->view('senialamientos',$data);            
        }         
    }
    
    /*
     * Función accedida por AJAX que recibe los nombres de los espacios que cuentan con los señalamientos
     * que van a ser eliminados.
     */    
    public function eliminarSenialamiento(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {   
            if(!$this->input->post('espacios')){
                echo "fallo";
            }
            else{
                $idEspacios = $this->input->post('espacios');
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
                
                foreach ($idEspacios as &$idEspacio) {
                    $this->senialamiento_model->eliminarSenialamientoPorIdEspacio($idEspacio, $idDependencia);
                }
                
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);                
                
                echo "exito";
            }    
        }
    }   
    
    /*
     * Función que redirige a la descripción del señalamiento
     */    
    public function mandarADesripcion($idEspacio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-espacio-cs', urldecode($idEspacio));
            redirect('DescripcionSenialamiento');
        }         
    }    
    
    /*
     * Función que redirige a la página para modificar la información del servicio
     */    
    public function mandarAModificar($idEspacio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-espacio-cs', urldecode($idEspacio));
            redirect('ModificarSenialamiento');
        }               
    }        
    
    
    private function quitarDatosSinUsarDeSesion(){
            if($this->session->userdata('id-espacio')){
                $this->session->unset_userdata('id-espacio');
            }
            if($this->session->userdata('id-servicio')){
                $this->session->unset_userdata('id-servicio');
            }                           
            if($this->session->userdata('id-evento-interno')){
                $this->session->unset_userdata('nombre-evento-interno');
            }            
            if($this->session->userdata('id-evento-externo')){
                $this->session->unset_userdata('id-evento-externo');
            }                         
    }        
}

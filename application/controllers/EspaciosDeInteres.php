<?php

class EspaciosDeInteres extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');
        $this->load->model('administrador_model');     
        $this->load->model('espacio_de_interes_model');
        $this->load->model('marcador_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $correo = $this->session->userdata('correo');
            $nombreAdmin = $this->administrador_model->getNombreAdministradorPorCorreo($correo);
            $data["nombreAdmin"] = $nombreAdmin;
            $data["nombreDependencia"] = $nombreDependencia;
            $espacios = $this->espacio_de_interes_model->getEspaciosDeDependencia($idDependencia);
            $data["espacios"] = $espacios;
            $hayMarcadores = $this->marcador_model->hayMarcadoresLibres($idDependencia);
            $data["hayMarcadores"] = $hayMarcadores;
            $this->load->view('espacios_de_interes',$data);
      }
    }
    
    
    /*
     * Función accedida por AJAX que recibe los nombres de los espacios de interés
     * que van a ser eliminados.
     */
    public function eliminarEspacioDeInteres(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {   
            if(!$this->input->post('espacios')){
                echo "fallo";
            }
            else{
                $idEspacios = $this->input->post('espacios');
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
                foreach ($idEspacios as &$idEspacio) {
                    $this->espacio_de_interes_model->eliminarEspacioPorId($idEspacio, $idDependencia);
                }
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->espaciosDeInteresActualizados($idDependencia, $fechaRegistro);                   
                echo "exito";
            }    
        }
    }      
    
    /*
     * Función que redirige a la descripción del espacio de interés
     */
    public function mandarADesripcion($idEspacio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            //Si se tiene el nombre de un servicio en la sesión se elimina para liberar espacio.
            $this->quitarDatosSinUsarDeSesion();
            //Se añade a la sesión el nombre del espacio del cual va a mostrarse la información 
            $this->session->set_userdata('id-espacio', urldecode($idEspacio));
            redirect('DescripcionEspacioDeInteres');
        }        
    }
    
    /*
     * Función que redirige a la página para modificar la información del lugar de interés
     */
    public function mandarAModificar($idEspacio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            //Si se tiene el nombre de un servicio en la sesión se elimina para liberar espacio.
            $this->quitarDatosSinUsarDeSesion();   
            //Se añade a la sesión el nombre del espacio del cual va a modificarse            
            $this->session->set_userdata('id-espacio', urldecode($idEspacio));
            redirect('ModificarEspacioDeInteres');
        }               
    }
    
    /*
     * Función que cierra sesión y redirige a la pantalla de inicio de sesión
     */    
    public function cerrarSesion(){
        $this->session->sess_destroy();
        redirect('Login');
    }    
    
    private function quitarDatosSinUsarDeSesion(){
            if($this->session->userdata('id-servicio')){
                $this->session->unset_userdata('id-servicio');
            }                           
            if($this->session->userdata('id-evento-interno')){
                $this->session->unset_userdata('nombre-evento-interno');
            }            
            if($this->session->userdata('id-evento-externo')){
                $this->session->unset_userdata('id-evento-externo');
            }              
            if($this->session->userdata('id-espacio-cs')){
                $this->session->unset_userdata('id-espacio-cs');
            }                          
    }
    
    
}

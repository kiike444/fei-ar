<?php

class Administradores extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('dependencia_model'); 
        $this->load->model('administrador_model');        
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);   
            $admins = $this->administrador_model->getAdministradores($idDependencia);
            $data['administradores'] = $admins;
            $this->load->view("administradores",$data); 
        }        
    }
    
}

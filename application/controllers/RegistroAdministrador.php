<?php

class RegistroAdministrador extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('dependencia_model');
        $this->load->model('administrador_model');
        $this->load->model('responsable_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        
        $this->form_validation->set_rules('txtNombre', 'Nombre del administrador',  'required|min_length[2]|max_length[100]');
        $this->form_validation->set_rules('txtCorreo', 'Correo', 'required|min_length[9]|max_length[50]|callback_correo_check');
        $this->form_validation->set_rules('txtContrasenia', 'Contraseña', 'required|min_length[1]|max_length[64]');
        $this->form_validation->set_rules('txtContraseniaVerif', 'Verificación de contraseña', 'required|min_length[1]|max_length[64]|matches[txtContrasenia]');
        $this->form_validation->set_rules('txtNombreDependencia', 'Nombre de la dependencia',  'required|min_length[2]|max_length[100]|callback_nombreDependencia_check');
        
        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        $this->form_validation->set_message('matches', 'La contraseña y su verificación no coinciden');        
        
        if($this->form_validation->run() == FALSE){
            $data['status'] = "";
            $this->load->view("registro_administrador",$data);
        }else{
            $nombre = $this->input->post("txtNombre");
            $correo = $this->input->post("txtCorreo");
            $contrasenia = $this->input->post("txtContrasenia");
            $nombreDependencia = $this->input->post("txtNombreDependencia");
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            
            //Se verifica que no esté registrada la cuenta
            if($this->buscarCuenta($correo, $idDependencia)){
                $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!    </strong>La cuenta ".$correo." ya está registrada como administrador en esta o en otra dependencia</div>";
                $this->load->view("registro_administrador",$data); 
            }else{
                $idResponsable = $this->responsable_model->getIdResponsablePorCorreo($correo, $idDependencia);
                //Ciframos la contraseña recibida antes de mandar a guardar los datos
                $contrasenia=hash('sha256', $contrasenia);
                if($this->buscarCuentaResponsable($correo, $idDependencia)){
                    $this->administrador_model->convertirAdministrador($idResponsable);
                    $this->administrador_model->guardarCuenta($idResponsable, $contrasenia, $idDependencia);
                }else{
                    $fecha = getdate();
                    $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];  
                    $this->responsable_model->guardar($nombre, $correo, 'administrador',$fechaRegistro, $idDependencia);
                    $idResponsable = $this->responsable_model->getIdResponsablePorCorreo($correo, $idDependencia);
                    $this->administrador_model->guardarCuenta($idResponsable, $contrasenia, $idDependencia);
                    //Se registra la actualización
                    $this->actualizacion_model->responsablesActualizados($idDependencia, $fechaRegistro);  
                }

                //Se redirecciona a la página de inicio de sesión
                redirect('Login');  
            }            
        }          
    }
    
    /*
     * Función que recibe un correo y verifica si existe un adminnistrador registrado
     * con ese correo.
     */
    function buscarCuenta($correo, $idDependencia){
        $result = $this->administrador_model->estaRegistrado($correo, $idDependencia);
        return $result;
        
    }
    
    /*
     * Función que recibe un correo y verifica si existe un adminnistrador registrado
     * con ese correo.
     */
    function buscarCuentaResponsable($correo, $idDependencia){
        $result = $this->administrador_model->esResponsable($correo, $idDependencia);
        return $result;
        
    }    
 
    
     /*
     * Función que valida el campo correo mediante FORM VALIDATION
     */
    function correo_check($correo){
        
        $result = filter_var($correo, FILTER_VALIDATE_EMAIL);
        
        if (!$result){
            $this->form_validation->set_message('correo_check', 'Ingrese un correo con formato correo@uv.mx');
            return FALSE;
        }
        else{
            $pos = strrpos($correo, "@uv.mx");
            if ($pos === false) {
                $this->form_validation->set_message('correo_check', 'Ingrese un correo con formato correo@uv.mx');
                return FALSE;
            }
            return TRUE;
        }
    }    

     /*
     * Función que valida el campo nombre de la dependencia mediante FORM VALIDATION
      * Verifica que la dependencia esté registrada
     */    
    function nombreDependencia_check($nombre){
        $result = $this->dependencia_model->estaRegistrada($nombre);
        
        if ($result){
            return TRUE;
        }
        else{
            $this->form_validation->set_message('nombreDependencia_check', 'No existe una dependencia con el nombre "'.$nombre.'"');
            return FALSE;
        }
    }             
    
    /*
     * Función accedida por AJAX que envía los nombres de las dependencias
     * que ya están registradas.
     */
    public function enviarNombresDeDependencias(){
       $result = $this->dependencia_model->getNombresDependenciasRegistradas();
        echo json_encode($result);
    }
    
    
    
}

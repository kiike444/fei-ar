<?php

class RegistroDependencia extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('dependencia_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        $this->form_validation->set_rules('txtNombre', 'Nombre de la dependencia',  'required|min_length[2]|max_length[100]|callback_nombreDependencia_check');
        $this->form_validation->set_rules('txtCoordenadas', 'Coordenadas', 'required|min_length[15]|max_length[50]|callback_coordenadas_check');
        $this->form_validation->set_rules('txtUrl', 'URL servidor de avisos', 'prep_url|max_length[100]|callback_urlServidorAvisos_check');
        
        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        
        $data['status'] = "";   
        $data['exito'] = false;
        
        if($this->form_validation->run() == FALSE){
            $this->load->view("registro_dependencia", $data);
        }else{
            $nombre = $this->input->post("txtNombre");
            $coordenadas = $this->input->post("txtCoordenadas");
            $urlServidorAvisos = $this->input->post("txtUrl");
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];  
            $this->dependencia_model->guardar($nombre, $coordenadas, $urlServidorAvisos, $fechaRegistro);
            
            //Crear registro de actualizaciones
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombre);
            $this->actualizacion_model->crearRegistroDeActualizaciones($idDependencia, $fechaRegistro);
            
            $data['exito'] = true;
            $data['status'] = "<div id='alert' class=\"alert alert-success\" role=\"alert\"><strong>¡Exito! </strong>La dependencia se registró correctamente</div>";
            $this->load->view("registro_dependencia", $data);
        }  
    }
    
    /*
     * Función que valida el campo nombre de la dependencia mediante FORM VALIDATION
     */
    function nombreDependencia_check($nombre){
        $result = $this->dependencia_model->estaRegistrada($nombre);
        
        if ($result){
            $this->form_validation->set_message('nombreDependencia_check', 'Ya existe una dependencia con el nombre "'.$nombre.'"');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }        

    /*
     * Función que valida el campo URL mediante FORM VALIDATION
     */    
    function urlServidorAvisos_check($url){
        
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlServidorAvisos_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }     
    
    /*
     * Función que valida las coordenadas recibidas marcadas en el mapa FORM VALIDATION
     */    
    function coordenadas_check($coordenadas){
        //Verifica que las coordenadas no se traslapen con las de otras dependencias ya registradas
        $result = $this->dependencia_model->coordenadasRegistradas($coordenadas);
        
        if ($result){
            $this->form_validation->set_message('coordenadas_check', 'Ya existe una dependencia registrada con esas coordenadas');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    /*
     * Función accedida por AJAX que envia las coordenadas de las dependencias registradas
     * las coordenadas envíadas son cargadas en el mapa.
     */
    public function enviarCoordenadasRegistradas(){
        $result = $this->dependencia_model->getCoordenadasRegistradas();
        echo json_encode($result);
    }
    
}

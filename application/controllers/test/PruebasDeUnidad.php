<?php

class PruebasDeUnidad extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('dependencia_model');
        $this->load->model('administrador_model');
        $this->load->model('marcador_model');
        $this->load->model('espacio_de_interes_model');
        $this->load->model('responsable_model');
        $this->load->model('servicio_model');
        $this->load->model('evento_model');
        $this->load->library('unit_test');
        $this->load->library('geometria');
        $this->load->library('manipulaciondeimagenes');
        $this->load->library('simple_html_dom');
        $this->load->helper('email');
    }
    
    public function index(){
        $this->guardarDependencia();
        $this->guardarAdministrador();
        $this->iniciarSesion();
        $this->guardarEspacioDeInteres();
        $this->consultarEspacioDeInteres();
        $this->eliminarEspacioDeInteres(); 
        $this->modificarEspacioDeInteres();
        $this->guardarResponsableBasico();
        $this->guardarResponsableAvanzado(); 
        $this->eliminarResponsable();
        $this->agregarServicio();
        $this->modificarServicio();
        $this->eliminarServicio();
        $this->agregarEvento();
        $this->modificarEventoInterno();
        $this->modificarEventoExterno();
        
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/guardarDependencia
    public function guardarDependencia(){
        $test = 'Registro de dependencia fallido (Nombre de la dependencia vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "";
        if(strlen($nombre) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Registro de dependencia fallido (Nombre de la dependencia muy largo mayor 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombre) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);    
        
        $test = 'Registro de dependencia fallido (Longitud de URL del servidor de avisos inválida 0 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlServidorAvisos = "";
        if(strlen($urlServidorAvisos) > 0){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Registro de dependencia fallido (Longitud de URL del servidor de avisos inválida mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlServidorAvisos = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($urlServidorAvisos) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    
        $test = 'Registro de dependencia fallido (Formato de URL del servidor de avisos inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlServidorAvisos = "wasdww.uv.mx/fei/";
        if(filter_var($urlServidorAvisos, FILTER_VALIDATE_URL)){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Registro de dependencia fallido (Longitud de coordenadas inválida 0 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $coordenadas = "";
        if(strlen($coordenadas) > 0){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
                
        $test = 'Registro de dependencia fallido (Longitud de coordenadas inválida mayor 50 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $coordenadas =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($coordenadas) <= 50){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
                
        $test = 'Registro de dependencia exitoso (Todos los valores válidos)';
        $resultadoEsperado = true;
        $resultado=false;
        $nombre = "Facultad de Estadística e Informática";
        $coordenadas = "19.540494,-96.928352,19.542494,-96.926352";
        $urlServidorAvisos = "https://www.uv.mx/fei/";
        $fechaRegistro = "2015-04-06 02:39:40";        
        $resultado = $this->dependencia_model->guardar($nombre, $coordenadas, $urlServidorAvisos, $fechaRegistro);
        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);     
        
        $test = 'Registro de dependencia fallido (coordenas ya registradas)';
        $resultadoEsperado = true;
        $resultado=false;
        $coordenadas = "19.540252,-96.928481,19.542454,-96.925987";
        $coordenadas2 = "19.538917,-96.929747,19.543222,-96.924785";    
        $resultado = $this->geometria->rectangulosSolapados($coordenadas,$coordenadas2);
        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);     
        
        $test = 'Registro de dependencia (coordenas no registradas)';
        $resultadoEsperado = false;
        $resultado=false;
        $coordenadas = "19.540252,-96.928481,19.542454,-96.925987";
        $coordenadas2 = "19.54102,-96.938201,19.545325,-96.93324";    
        $resultado = $this->geometria->rectangulosSolapados($coordenadas,$coordenadas2);
        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);      
        
        $test = 'Registro de dependencia fallido (Nombre ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;
        $nombre = "Facultad de Estadística e Informática";       
        $resultado = $this->dependencia_model->estaRegistrada($nombre); 
        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/guardarAdministrador
    public function guardarAdministrador(){
        $test = 'Registro de administrador exitoso (Todos los valores válidos)';
        $resultadoEsperado = true;
        $resultado=false;
        $nombre="Pedro Enrique Virgen Dorantes";
        $correo = "pvirgen@uv.mx";
        $contrasenia = "123456";
        $fechaRegistro = "2015-04-06 02:39:40";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        if(!$this->administrador_model->estaRegistrado($correo, $idDependencia)){
            $this->responsable_model->guardar($nombre, $correo, 'administrador',$fechaRegistro, $idDependencia);
            $idResponsable = $this->responsable_model->getIdResponsablePorCorreo($correo, $idDependencia);
            $contrasenia=hash('sha256', $contrasenia);
            $this->administrador_model->guardarCuenta($idResponsable, $contrasenia, $idDependencia);
            $resultado = true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);       
        
        
        $test = 'Registro de administrador fallido (El administrador ya está registrado)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre="Pedro Enrique Virgen Dorantes";
        $correo = "pvirgen@uv.mx";
        $contrasenia = "123456";
        $fechaRegistro = "2015-04-06 02:39:40";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        if(!$this->administrador_model->estaRegistrado($correo, $idDependencia)){
            $this->responsable_model->guardar($nombre, $correo, 'administrador',$fechaRegistro, $idDependencia);
            $idResponsable = $this->responsable_model->getIdResponsablePorCorreo($correo, $idDependencia);
            $contrasenia=hash('sha256', $contrasenia);
            $this->administrador_model->guardarCuenta($idResponsable, $contrasenia, $idDependencia);
            $resultado = true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/iniciarSesion
    public function iniciarSesion(){
        $test = 'Login fallido (Formato correo inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "correonovalido.com";
        $resultado = filter_var($correo, FILTER_VALIDATE_EMAIL);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Login fallido (Correo mayor a 50 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
        if(strlen($correo) > 50){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Login fallido (Correo vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "";
        if(strlen($correo) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);          
        
        $test = 'Login fallido (Contraseña vacía)';
        $resultadoEsperado = false;
        $resultado=false;
        $contrasenia = "";
        if(strlen($contrasenia) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Login fallido (Contraseña mayor a 64 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $contrasenia = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendis";
        if(strlen($contrasenia) > 64 ){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
    
        $test = 'Login fallido (Contraseña incorrecta)';
        $resultadoEsperado = false;
        $resultado=false;
        $contrasenia = "1234567";
        $correo = "pvirgen@uv.mx";
        $contrasenia=hash('sha256', $contrasenia);
        $resultado = $this->administrador_model->verificarCuenta($correo,$contrasenia);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Obteniendo el id de la dependencia asociada al administrador mediante su correo';
        $resultadoEsperado = true;
        $resultado=false;
        $correo = "pvirgen@uv.mx";
        $idDependencia = $this->administrador_model->getIdDependenciaPorCorreo($correo);
        if($idDependencia != NULL){
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);   
        
        $test = 'Obteniendo el nombre de la dependencia asociada a partir de su id';
        $resultadoEsperado = true;
        $resultado=false;
        $correo = "pvirgen@uv.mx";
        $idDependencia = $this->administrador_model->getIdDependenciaPorCorreo($correo);
        if($idDependencia != NULL){
            $nombreDependencia = $this->dependencia_model->getNombrePorId($idDependencia);
            if($nombreDependencia != NULL){
                $resultado=true;
            }
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);           
        
        $test = 'Login exitoso (Todos los campos validos)';
        $resultadoEsperado = true;
        $resultado=false;
        $contrasenia = "123456";
        $correo = "pvirgen@uv.mx";
        $contrasenia=hash('sha256', $contrasenia);
        $resultado = $this->administrador_model->verificarCuenta($correo,$contrasenia);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);           
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/guardarEspacioDeInteres    
    public function guardarEspacioDeInteres(){
        $test = 'Registro de espacio de interés fallido (Nombre mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombre) > 100){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Registro de espacio de interés fallido (Nombre vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "";
        if(strlen($nombre) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);  
        
        $test = 'Registro de espacio de interés fallido (Descripción mayor a 300 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "1234567890";
        if(strlen($descripcion) > 300){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Registro de espacio de interés fallido (Descripción vacía)';
        $resultadoEsperado = false;
        $resultado=false;
        $descripcion = "";
        if(strlen($descripcion) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);  

        $test = 'Registro de espacio de interés fallido (Longitud de URL de sitio web inválida, 0 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "";
        if(strlen($urlSitioWeb) > 0){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Registro de espacio de interés fallido (Longitud de URL de sitio web inválida, mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($urlSitioWeb) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    
        $test = 'Registro de espacio de interés fallido (Formato de URL de sitio web inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "wasdww.uv.mx/fei/";
        if(filter_var($urlSitioWeb, FILTER_VALIDATE_URL)){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 

        $test = 'Proceso de escalar imagen fallido (Formato de imagen descriptiva o de marcador inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $rutaImagen = "tmp_uploads/test/imgtest.gif";
        $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Proceso de escalar exitoso (Formato de imagen descriptiva o de marcador válido)';
        $resultadoEsperado = true;
        $resultado=false;
        $rutaImagen = "tmp_uploads/test/imgtest.jpg";
        $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        if($resultado){
            $rutaImagen = "tmp_uploads/test/imgtest.png";
            $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Proceso de unir imagen fallido (Formato de imagen descriptiva o de marcador inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombreDependencia = "Facultad de Estadística e Informática";
        $rutaImagen = "tmp_uploads/test/imgtest.gif";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);
        $resultado = $this->manipulaciondeimagenes->unirImagenes($rutaImagen,'assets/images/framemarkers/'.$idMarcador.'.png');
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Proceso de unir exitoso (Formato de imagen descriptiva o de marcador válido)';
        $resultadoEsperado = true;
        $resultado=false;
        $nombreDependencia = "Facultad de Estadística e Informática";
        $rutaImagen = "tmp_uploads/test/imgtest.jpg";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);
        $resultado = $this->manipulaciondeimagenes->unirImagenes($rutaImagen,'assets/images/framemarkers/'.$idMarcador.'.png');
        echo $this->unit->run($resultado, $resultadoEsperado, $test);      

        $test = 'Registro de espacio de interés exitoso';
        $resultadoEsperado = true;
        $resultado=false;
        $nombre = "Espacio test";
        $descripcion = "Este es un test";
        $urlSitioWeb = "www.uv.mx";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);  
        $rutaImagenDesc = "tmp_uploads/test/imgtest.png";
        $rutaImagenMarc = "tmp_uploads/test/imgtest.jpg";
        $imgDescriptiva = file_get_contents($rutaImagenDesc);
        $imgMarcador = file_get_contents($rutaImagenMarc);        
        $fechaRegistro = "2015-04-06 02:39:40";
        $resultado = $this->espacio_de_interes_model->guardar($nombre, $descripcion, $urlSitioWeb, $imgDescriptiva, $imgMarcador,$fechaRegistro, $idDependencia, $idMarcador);
        $nombre = "Espacio test 2";
        $this->espacio_de_interes_model->guardar($nombre, $descripcion, $urlSitioWeb, $imgDescriptiva, $imgMarcador,$fechaRegistro, $idDependencia, $idMarcador);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);          
        
        }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/consultarEspacioDeInteres
    public function consultarEspacioDeInteres(){
         $test = 'Consultar espacio de interés fallido (Nombre de espacio mayor a 100 caracteres)';
         $resultadoEsperado = false;
         $resultado=false;        
         $nombreEspacio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($nombreEspacio) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);

         $test = 'Consultar espacio de interés fallido (Nombre de espacio vacío)';
         $resultadoEsperado = false;
         $resultado=false;        
         $nombreEspacio = "";
        if(strlen($nombreEspacio) > 0){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);           

        $test = 'Consultar espacio de interés fallido (No hay espacios de interés registrados)';
         $resultadoEsperado = true;
         $resultado=false;        
         $nombreDependencia = "Facultad de Estadística e Informática";
         $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);         
         $espacios = $this->espacio_de_interes_model->getEspaciosDeDependencia($idDependencia);
         if($espacios == NULL){
             $resultado=true;  
         }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Consultar espacio de interés(Hay espacios de interés registrados)';
         $resultadoEsperado = true;
         $resultado=false;        
         $nombreDependencia = "Facultad de Estadística e Informática";
         $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);         
         $espacios = $this->espacio_de_interes_model->getEspaciosDeDependencia($idDependencia);
         if($espacios != NULL){
             $resultado=true;  
         }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);                     
        
        $test = 'Consultar espacio de interés exitoso';
         $resultadoEsperado = true;
         $resultado=false;        
         $nombreDependencia = "Facultad de Estadística e Informática";
         $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
         $idEspacioDeInteres = 1;
         $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
         if($espacio != NULL){
             $resultado=true;  
         }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
        
        //TODO CONSULTA DE SERVICIOS
        //TODO CONSULTA DE EVENTOS
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/eliminarEspacioDeInteres    
    public function eliminarEspacioDeInteres(){
        $test = 'Eliminar espacio de interés';
        $resultadoEsperado = true;
        $resultado=false;
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
        $idEspacio = 2;
        $resultado = $this->espacio_de_interes_model->eliminarEspacioPorId($idEspacio, $idDependencia);
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
    }        
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/modificarEspacioDeInteres
    public function modificarEspacioDeInteres(){
        $test = 'Modificar espacio de interés fallido (Nombre mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombre) > 100){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Modificar espacio de interés fallido (Nombre vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "";
        if(strlen($nombre) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);  
        
        $test = 'Modificar espacio de interés fallido (Descripción mayor a 300 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "1234567890";
        if(strlen($descripcion) > 300){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Modificar espacio de interés fallido (Descripción vacía)';
        $resultadoEsperado = false;
        $resultado=false;
        $descripcion = "";
        if(strlen($descripcion) == 0){
            $resultado=false;
        }else{
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);  
     
        $test = 'Modificar espacio de interés fallido (Longitud de URL de sitio web inválida, mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($urlSitioWeb) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    
        $test = 'Modificar espacio de interés fallido (Formato de URL de sitio web inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "wasdww.uv.mx/fei/";
        if(filter_var($urlSitioWeb, FILTER_VALIDATE_URL)){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 

        $test = 'Proceso de escalar imagen fallido (Formato de imagen descriptiva o de marcador inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $rutaImagen = "tmp_uploads/test/imgtest.gif";
        $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Proceso de escalar exitoso (Formato de imagen descriptiva o de marcador válido)';
        $resultadoEsperado = true;
        $resultado=false;
        $rutaImagen = "tmp_uploads/test/imgtest.jpg";
        $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        if($resultado){
            $rutaImagen = "tmp_uploads/test/imgtest.png";
            $resultado = $this->manipulaciondeimagenes->escalar($rutaImagen);
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Proceso de unir imagen fallido (Formato de imagen descriptiva o de marcador inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombreDependencia = "Facultad de Estadística e Informática";
        $rutaImagen = "tmp_uploads/test/imgtest.gif";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $idEspacioDeInteres = 1;
        $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
        $idMarcador = $espacio->idMarcador;        
        $resultado = $this->manipulaciondeimagenes->unirImagenes($rutaImagen,'assets/images/framemarkers/'.$idMarcador.'.png');
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Proceso de unir exitoso (Formato de imagen descriptiva o de marcador válido)';
        $resultadoEsperado = true;
        $resultado=false;
        $nombreDependencia = "Facultad de Estadística e Informática";
        $rutaImagen = "tmp_uploads/test/imgtest.jpg";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $idEspacioDeInteres = 1;
        $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
        $idMarcador = $espacio->idMarcador;   
        $resultado = $this->manipulaciondeimagenes->unirImagenes($rutaImagen,'assets/images/framemarkers/'.$idMarcador.'.png');
        echo $this->unit->run($resultado, $resultadoEsperado, $test);      

        $test = 'Modificar espacio de interés exitoso';
        $resultadoEsperado = true;
        $resultado=false;
        $nombre = "Espacio test modificado";
        $descripcion = "Este es un test";
        $urlSitioWeb = "www.uv.mx";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);  
        $rutaImagenDesc = "tmp_uploads/test/imgtest.png";
        $rutaImagenMarc = "tmp_uploads/test/imgtest.jpg";
        $imgDescriptiva = file_get_contents($rutaImagenDesc);
        $imgMarcador = file_get_contents($rutaImagenMarc);        
        $nombreEspacioActual = "Espacio test";
        $resultado = $this->espacio_de_interes_model->actualizar($nombre,$descripcion,$urlSitioWeb,$imgDescriptiva,$imgMarcador,$nombreEspacioActual,$idDependencia);        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);               
    }    
   
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/guardarResponsableBasico    
    public function guardarResponsableBasico(){
        $test = 'Agregar responsable modo básico fallido (Nombre vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "";
        if(strlen($nombre) > 0){
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
        
        $test = 'Agregar responsable modo básico fallido (Nombre mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $nombre = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($nombre) <= 100){
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
        
        $test = 'Agregar responsable modo básico fallido (Correo vacío)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "";
        if(strlen($correo) > 0){
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
        
        $test = 'Agregar responsable modo básico fallido (Correo mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($correo) <= 100){
            $resultado=true;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
                
        $test = 'Agregar responsable modo básico fallido (Formato correo inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $correo = "correonovalido.com";
        $resultado = filter_var($correo, FILTER_VALIDATE_EMAIL);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);      

        $test = 'Agregar responsable exitoso';
        $resultadoEsperado = true;
        $resultado=false;        
        $nombre = "Responsable test";
        $correoInstitucional = "soyuntest@uv.mx";
        $fechaRegistro = "2015-04-06 02:39:40";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        if(!$this->responsable_model->estaRegistrado($correoInstitucional, $idDependencia)){
            $resultado = $this->responsable_model->guardar($nombre, $correoInstitucional, 'responsable', $fechaRegistro, $idDependencia);
           
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Agregar responsable fallido (Ya está registrado)';
        $resultadoEsperado = false;
        $resultado=false;        
        $nombre = "Responsable test";
        $correoInstitucional = "soyuntest@uv.mx";
        $fechaRegistro = "2015-04-06 02:39:40";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        if(!$this->responsable_model->estaRegistrado($correoInstitucional, $idDependencia)){
            $resultado = $this->responsable_model->guardar($nombre, $correoInstitucional, 'responsable', $fechaRegistro, $idDependencia);
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/guardarResponsableAvanzado
    public function guardarResponsableAvanzado(){
        $test = 'Agregar responsables modo avanzado';
        $resultadoEsperado = true;
        $resultado=true;        
        
        $dom = file_get_html('https://www.uv.mx/fei/personal-academico/informatica-3/');
        $responsables = array();

        try{
            if(count($dom->find('#GridView1')) > 0){
                foreach($dom->find('#GridView1 tr') as $fila) {
                    if(!$fila->find('th')){
                        $responsable = array(
                            "nombre" => $fila->find('td', 0)->plaintext,
                            "correoInstitucional" => $fila->find('td', 1)->plaintext
                        );                
                        $responsables[] = $responsable;
                    }
                }              
            }else{
                if(count($dom->find('table.contenedor')) > 0){
                    foreach($dom->find('table.contenedor tr td li') as $li) {
                            $responsable = array(
                                "nombre" => $li->find('span.nombre',0)->plaintext,
                                "correoInstitucional" => $li->find('a.correo',0)->plaintext
                            );                
                            $responsables[] = $responsable;
                    }                
                }else{
                    if(count($dom->find('tr strong')) > 0 && count($dom->find('tr td h3')) < 1){
                        foreach($dom->find('tr') as $fila) {
                            if(!$fila->find('strong')){
                                $responsable = array(
                                    "nombre" => $fila->find('td', 1)->plaintext,
                                    "correoInstitucional" => $fila->find('td', 2)->plaintext
                                );
                                $responsables[] = $responsable;                
                            }
                        }                    
                    }else{ 
                        if(count($dom->find('tr td h3')) > 0){
                            foreach($dom->find('tr') as $fila) {
                                if(!$fila->find('strong')){
                                    $responsable = array(
                                        "nombre" => $fila->find('td', 0)->plaintext,
                                        "correoInstitucional" => $fila->find('td', 1)->plaintext
                                    );
                                    $responsables[] = $responsable;                
                                }
                            }                        
                        }else{
                            $resultado=false;
                        }
                    }
                }
            }            
        }  catch (Exception $e) {
            $resultado=false;   
        }
        
        if($resultado && count($responsables)>0){
            $fechaRegistro = "2015-04-06 02:39:40";
            $nombreDependencia = "Facultad de Estadística e Informática";
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  

            for($i = 0; $i<count($responsables); $i++){
                $nombre = $responsables[$i]['nombre'];
                $correoInstitucional = $responsables[$i]['correoInstitucional'];
                if(!$this->responsable_model->estaRegistrado($correoInstitucional, $idDependencia)){
                    $this->responsable_model->guardar($nombre, $correoInstitucional, 'responsable', $fechaRegistro, $idDependencia);
                }
            }               
        }

       echo $this->unit->run($resultado, $resultadoEsperado, $test); 
    }    
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/eliminarResponsable
    public function eliminarResponsable(){
        $test = 'Eliminar responsable exitoso';
        $resultadoEsperado = true;
        $resultado=false;        
        $correoInstitucional = "soyuntest@uv.mx";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
        $resultado = $this->responsable_model->eliminar($correoInstitucional, $idDependencia); 
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Eliminar responsable fallido (No está registrado)';
        $resultadoEsperado = false;
        $resultado=false;        
        $correoInstitucional = "soyuntest@uv.mx";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
        $resultado = $this->responsable_model->eliminar($correoInstitucional, $idDependencia); 
        echo $this->unit->run($resultado, $resultadoEsperado, $test);    
    }    
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/agregarServicio
    public function agregarServicio(){
        $test = 'Registro de servicio fallido (Nombre de servicio vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombre = "";
        if(strlen($nombre) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Registro de servicio fallido (Nombre de servicio mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombre = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombre) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Registro de servicio fallido (Nombre de espacio de interés vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "";
        if(strlen($nombreEspacio) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Registro de servicio fallido (Nombre de espacio de interés mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombreEspacio) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Registro de servicio fallido (Descripción de servicio vacía)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "";
        if(strlen($descripcion) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Registro de servicio fallido (Descripción de servicio mayor a 300 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "1234567890";
        if(strlen($descripcion) <= 300){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
       
        $test = 'Registro de servicio fallido (Correo de responsable vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $correoResponsable = "";
        if(strlen($correoResponsable) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Registro de servicio fallido (Correo responsable mayor a 50 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $correoResponsable = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
        if(strlen($descripcion) <= 50){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);   
        
        $test = 'Registro de servicio fallido (Formato correo inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $correoResponsable = "correonovalido.com";
        $resultado = filter_var($correoResponsable, FILTER_VALIDATE_EMAIL);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
        
        $test = 'Registro de servicio exitoso (Todos los datos válidos)';
        $resultadoEsperado = true;
        $resultado=false;   
        $nombre = "Servicio de test";
        $descripcion = "Solo soy un test haciendo de servicio";
        $fechaRegistro = "2015-04-06 02:39:40";        
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);          
        $nombreEspacio = "Espacio test modificado";
        $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
        $correoResponsable = "soyuntest@uv.mx"; 
        $idResponsable = $this->responsable_model->getIdResponsablePorCorreo($correoResponsable, $idDependencia);
        $resultado = $this->servicio_model->guardar($nombre, $descripcion, $fechaRegistro, $idEspacioDeInteres ,$idDependencia, $idResponsable);
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
        
        $test = 'Registro de servicio fallido (Servicio ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;           
        $nombre = "Servicio de test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $resultado = $this->servicio_model->estaRegistrado($nombre, $idDependencia);        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
    }
    

    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/modificarServicio
    public function modificarServicio(){
        $test = 'Modificar servicio fallido (Nombre de servicio vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreServicio = "";
        if(strlen($nombreServicio) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Modificar servicio fallido (Nombre de servicio mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreServicio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombreServicio) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Modificar servicio fallido (Nombre de espacio de interés vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "";
        if(strlen($nombreEspacio) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Modificar servicio fallido (Nombre de espacio de interés mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombreEspacio) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Modificar servicio fallido (Descripción de servicio vacía)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "";
        if(strlen($descripcion) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Modificar servicio fallido (Descripción de servicio mayor a 300 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "1234567890";
        if(strlen($descripcion) <= 300){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
       
        $test = 'Modificar servicio fallido (Correo de responsable vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $correoResponsable = "";
        if(strlen($correoResponsable) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Modificar servicio fallido (Correo responsable mayor a 50 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $correoResponsable = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
        if(strlen($descripcion) <= 50){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);   
        
        $test = 'Modificar servicio fallido (Formato correo inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $correoResponsable = "correonovalido.com";
        $resultado = filter_var($correoResponsable, FILTER_VALIDATE_EMAIL);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
       
        $test = 'Modificar servicio fallido (Nombre del servicio ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;                 
        $nombreServicio = "Servicio de test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);         
        $resultado = $this->servicio_model->estaRegistrado($nombreServicio, $idDependencia);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Modificar servicio fallido (Nombre del espacio de interes no registrado)';
        $resultadoEsperado = false;
        $resultado=false;                 
        $nombreEspacio = "Espacio test que no está"; 
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);         
        $resultado = $this->espacio_de_interes_model->estaRegistrado($nombreEspacio, $idDependencia);  
        echo $this->unit->run($resultado, $resultadoEsperado, $test);

        $test = 'Modificar servicio fallido (Nombre del responsable no registrado)';
        $resultadoEsperado = false;
        $resultado=false;                 
        $nombreResponsable = "Responsable test que no esta";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);           
        $resultado = $this->responsable_model->estaRegistradoPorNombre($nombreResponsable, $idDependencia);  
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
        
        $test = 'Modificar servicio exitoso';
        $resultadoEsperado = true;
        $resultado=false;           
        $nombreServicio = "Servicio de test modificado";
        $descripcion =  "Solo soy un test haciendo de servicio modificado";
        $nombreEspacio = "Espacio test";  
        $nombreResponsable = "Responsable test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  
        $idServicio = 1;
        $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
        $idResponsable = $this->responsable_model->getIdResponsablePorNombre($nombreResponsable, $idDependencia);
        $resultado = $this->servicio_model->actualizar($idServicio,  $nombreServicio, $descripcion, $idEspacioDeInteres ,$idDependencia, $idResponsable);                               
        echo $this->unit->run($resultado, $resultadoEsperado, $test);            
    }    
    
    
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/eliminarServicio
    public function eliminarServicio(){
        $test = 'eliminar servicio fallido (No se encuentra el servicio)';
        $resultadoEsperado = false;
        $resultado=false;          
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  
        $idServicio = 2;
        $resultado = $this->servicio_model->eliminarServicioPorId($idServicio, $idDependencia);   
        echo $this->unit->run($resultado, $resultadoEsperado, $test);          
        
        $test = 'eliminar servicio exitoso';
        $resultadoEsperado = true;
        $resultado=false;          
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  
        $idServicio = 1;
        $resultado = $this->servicio_model->eliminarServicioPorId($idServicio, $idDependencia);     
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
    }    
    
    

    //http://localhost/feiar/index.php/test/PruebasDeUnidad/agregarEvento
    public function agregarEvento(){
        $test = 'Agregar evento fallido (Nombre de evento vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEvento = "";
        if(strlen($nombreEvento) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Agregar evento (Nombre de evento mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEvento = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombreEvento) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Agregar evento interno (Nombre de espacio de interés vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "";
        if(strlen($nombreEspacio) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Agregar evento interno fallido (Nombre de espacio de interés mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $nombreEspacio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($nombreEspacio) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);     
        
        $test = 'Agregar evento externo (Nombre del lugar del evento vacío)';
        $resultadoEsperado = false;
        $resultado=false;           
        $lugar = "";
        if(strlen($lugar) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        

        $test = 'Agregar evento externo fallido (Nombre del lugar del evento, mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $lugar = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapo";
        if(strlen($lugar) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
        
        
        $test = 'Agregar evento fallido (Descripción de evento vacía)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "";
        if(strlen($descripcion) >0){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         

        $test = 'Agregar evento fallido (Descripción de evento mayor a 300 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;           
        $descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque vitae ligula convallis, pretium sapo"
                . "1234567890";
        if(strlen($descripcion) <= 300){
            $resultado=true;
        }else{
            $resultado=false;
        }        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);   
        
        $test = 'Agregar evento fallido (Longitud de URL de sitio web inválida, 0 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "";
        if(strlen($urlSitioWeb) > 0){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
                
        $test = 'Agregar evento fallido (Longitud de fecha inicio, 19 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $fechaInicio = "2015-06 02:39:40";
        if(strlen($fechaInicio) == 19){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Agregar evento fallido (Longitud de fecha final, no igual a 19 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $fechaFin = "2015-06-06 02:39";
        if(strlen($fechaFin) == 19){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Agregar evento fallido (Longitud de URL de sitio web inválida, mayor a 100 caracteres)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                . "Quisque vitae ligula convallis, pretium sapoo";
        if(strlen($urlSitioWeb) <= 100){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    
        $test = 'Agregar evento fallido (Formato de URL de sitio web inválido)';
        $resultadoEsperado = false;
        $resultado=false;
        $urlSitioWeb = "wasdww.uv.mx/fei/";
        if(filter_var($urlSitioWeb, FILTER_VALIDATE_URL)){
            $resultado=true;
        }else{
            $resultado=false;
        }
        echo $this->unit->run($resultado, $resultadoEsperado, $test);         
        
        $test = 'Registro de evento interno exitoso (Todos los datos válidos)';
        $resultadoEsperado = true;
        $resultado=false;   
        $nombre = 'Evento interno de test';
        $descripcion = 'Este es un evento interno de test';
        $fechaInicio = "2015-05-06 02:39:40";
        $fechaFin = "2015-06-06 02:39:40";
        $nombreEspacio = "Espacio test";
        $urlSitioWeb = "http://www.eventotest.com"; 
        $nombreDependencia = "Facultad de Estadística e Informática"; 
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $fechaRegistro = "2015-04-06 02:39:40"; 
        $tipo= "interno";        
        $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
        $resultado = $this->evento_model->guardarInterno($nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $tipo, $fechaRegistro, $idDependencia, $idEspacioDeInteres);
        echo $this->unit->run($resultado, $resultadoEsperado, $test); 
        
        $test = 'Registro de evento fallido (Evento ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;           
        $nombre = "Evento interno de test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $resultado = $this->evento_model->estaRegistrado($nombre, $idDependencia);        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);     
        
        $test = 'Registro de evento externo exitoso (Todos los datos válidos)';
        $resultadoEsperado = true;
        $resultado=false;   
        $nombre = 'Evento externo de test';
        $descripcion = 'Este es un externo interno de test';
        $fechaInicio = "2015-05-06 02:39:40";
        $fechaFin = "2015-06-06 02:39:40";
        $lugar = "Casa del lago";
        $urlSitioWeb = "http://www.eventotest.com"; 
        $nombreDependencia = "Facultad de Estadística e Informática"; 
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $fechaRegistro = "2015-04-06 02:39:40"; 
        $tipo= "externo";            
        $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);  
        $rutaImagenDesc = "tmp_uploads/test/imgtest2.png";
        $rutaImagenMarc = "tmp_uploads/test/imgtest2.jpg";
        $this->manipulaciondeimagenes->escalar($rutaImagenDesc);
        $this->manipulaciondeimagenes->escalar( $rutaImagenMarc);
        $this->manipulaciondeimagenes->unirImagenes($rutaImagenDesc,'assets/images/framemarkers/'.$idMarcador.'.png');  
        $this->manipulaciondeimagenes->unirImagenes($rutaImagenMarc,'assets/images/framemarkers/'.$idMarcador.'.png');
        $imgDescriptiva = file_get_contents($rutaImagenDesc);
        $imgMarcador = file_get_contents($rutaImagenMarc);
        $this->evento_model->guardarInterno($nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $tipo, $fechaRegistro, $idDependencia, $idEspacioDeInteres);
        $idEvento = $this->evento_model->getIdEventoPorNombre($nombre, $idDependencia);
        $resultado = $this->evento_model->guardarExterno($lugar, $imgDescriptiva, $imgMarcador, $idEvento, $idMarcador);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);          
    }
    
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/modificarEventoInterno
    public function modificarEventoInterno(){
        $test = 'Modificar evento interno fallido (Evento ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;           
        $nombre = "Evento interno de test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $resultado = $this->evento_model->estaRegistrado($nombre, $idDependencia);        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);        
        
        $test = 'Modificar evento interno exitoso (Todos los datos válidos)';
        $resultadoEsperado = true;
        $resultado=false;   
        $nombre = 'Evento interno de test modificado';
        $descripcion = 'Este es un evento interno de test';
        $fechaInicio = "2015-05-06 02:39:40";
        $fechaFin = "2015-06-06 02:39:40";
        $nombreEspacio = "Espacio test";
        $urlSitioWeb = "http://www.eventotest.com"; 
        $nombreDependencia = "Facultad de Estadística e Informática"; 
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia); 
        $idEvento = 1;
        $resultado = $this->evento_model->actualizarEventoInterno($idEvento, $nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $idEspacioDeInteres, $idDependencia);      
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    }
    
    //http://localhost/feiar/index.php/test/PruebasDeUnidad/modificarEventoExterno
    public function modificarEventoExterno(){
        $test = 'Modificar evento externo fallido (Evento ya registrado)';
        $resultadoEsperado = true;
        $resultado=false;           
        $nombre = "Evento externo de test";
        $nombreDependencia = "Facultad de Estadística e Informática";
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $resultado = $this->evento_model->estaRegistrado($nombre, $idDependencia);        
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
        
        $test = 'Modificar evento externo exitoso (Todos los datos válidos)';
        $resultadoEsperado = true;
        $resultado=false;   
        $nombre = 'Evento externo de test modificado';
        $nombreActual = 'Evento externo de test';
        $descripcion = 'Este es un evento interno de test';
        $fechaInicio = "2015-05-06 02:35:40";
        $fechaFin = "2015-06-06 02:47:40";
        $lugar = "El agora";
        $urlSitioWeb = "http://www.eventotest.com"; 
        $nombreDependencia = "Facultad de Estadística e Informática"; 
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);            
        $evento = $this->evento_model->getEventoExterno($nombreActual, $idDependencia);
        $idMarcador = $evento->idMarcador;          
        $rutaImagenDesc = "tmp_uploads/test/imgtest2.png";
        $rutaImagenMarc = "tmp_uploads/test/imgtest2.jpg";
        $this->manipulaciondeimagenes->escalar($rutaImagenDesc);
        $this->manipulaciondeimagenes->escalar( $rutaImagenMarc);
        $this->manipulaciondeimagenes->unirImagenes($rutaImagenDesc,'assets/images/framemarkers/'.$idMarcador.'.png');  
        $this->manipulaciondeimagenes->unirImagenes($rutaImagenMarc,'assets/images/framemarkers/'.$idMarcador.'.png');  
        $imgDescriptiva = file_get_contents($rutaImagenDesc);
        $imgMarcador = file_get_contents($rutaImagenMarc); 
        $idEvento = 2;
        $this->evento_model->actualizarEventoInterno($idEvento, $nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, NULL, $idDependencia);        
        $idEvento = $this->evento_model->getIdEventoPorNombre($nombre, $idDependencia);
        $resultado = $this->evento_model->actualizarEventoExterno($idEvento, $lugar,  $imgDescriptiva, $imgMarcador);
        echo $this->unit->run($resultado, $resultadoEsperado, $test);
    }    
    
}
 
<?php

class RegistroEspacioDeInteres extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('ManipulacionDeImagenes');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');  
        $this->load->model('marcador_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }        
        
        $this->form_validation->set_rules('txtNombre', 'Nombre del espacio',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check');
        $this->form_validation->set_rules('txtDescripcion', 'Descripción del espacio', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtUrl', 'Url relacionada', 'prep_url|max_length[100]|callback_urlRelacionada_check');

        $imagenDescriptivaPredefinida = false;
        $marcadorPredefinido = false;
        $data['exito'] = false;
        
        if (empty($_FILES['imagenDescriptiva']['name']))
        {
            $imagenDescriptivaPredefinida = true;
        }
            if(empty($_FILES['imagenMarcador']['name'])){
                $marcadorPredefinido = true;
            }        
        
        //comprobamos si los datos son correctos, el comodín %s nos mostrará el nombre del campo que falló
        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        
        if($this->form_validation->run() == FALSE){
            $data['status'] = "";
            $this->load->view("registro_espacio",$data);
        }else{
            $nombre = $this->input->post("txtNombre");
            $descripcion = $this->input->post("txtDescripcion");
            $urlSitioWeb = $this->input->post("txtUrl");
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];
            //Se obtiene el id de un marcador libre para el espacio de interés
            $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia);
            
            $config['upload_path'] = 'tmp_uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']	= '2048'; 
            $this->load->library('upload', $config);
            
            $error = false;
            
            if($this->upload->do_upload('imagenDescriptiva')){
                $imgDescData = $this->upload->data();
                $this->manipulaciondeimagenes->escalar($imgDescData['full_path']);
                $imgDescriptiva = file_get_contents($imgDescData['full_path']);
            }else{
                if($imagenDescriptivaPredefinida){
                    copy('assets/images/default.png', 'tmp_uploads/default01.png');
                    $imgDescriptiva = file_get_contents('tmp_uploads/default01.png'); 
                }else{
                    $error = true;
                    $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!  </strong> Verifique que el formato y tamaño del archivo de imagen sea correcto</div>";
                    $this->load->view("registro_espacio",$data);
                }
            }
            
            if(!$error){
                if($this->upload->do_upload('imagenMarcador')){
                    $imgMarcData = $this->upload->data();
                    $this->manipulaciondeimagenes->escalar($imgMarcData['full_path']);
                    $this->manipulaciondeimagenes->unirImagenes($imgMarcData['full_path'],'assets/images/framemarkers/'.$idMarcador.'.png');
                    $imgMarcador = file_get_contents($imgMarcData['full_path']);
                }else{
                    if($marcadorPredefinido){
                        copy('assets/images/default.png', 'tmp_uploads/default02.png');
                        $this->manipulaciondeimagenes->unirImagenes('tmp_uploads/default02.png','assets/images/framemarkers/'.$idMarcador.'.png');
                        $imgMarcador = file_get_contents('tmp_uploads/default02.png');   
                        }else{
                            //Si se recibe la imagen de marcador pero su formato es inválido se muestra un error
                            $error = true;
                            $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!  </strong>Verifique que el formato y tamaño del archivo de imagen sea correcto</div>";
                            $this->load->view("registro_espacio",$data);                        
                        }                
                }
                
                if(!$error){
                    $this->espacio_de_interes_model->guardar($nombre, $descripcion, $urlSitioWeb, $imgDescriptiva, $imgMarcador,$fechaRegistro, $idDependencia, $idMarcador);
                    $this->actualizacion_model->espaciosDeInteresActualizados($idDependencia, $fechaRegistro);  
                    
                    //Si ya no hay marcadores disponibles se redirecciona a la página de espacios de interés
                     $hayMarcadores = $this->marcador_model->hayMarcadoresLibres($idDependencia);
                     if($hayMarcadores == FALSE){
                         redirect('EspaciosDeInteres','refresh');
                     }
                     //Se muestra un mensaje de confirmación
                     $data['status'] = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" "
                             . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                             . "</button><strong>¡Exito!  </strong>El espacio de interés se agregó correctamente</div>";
                    $data['exito'] = true;
                    $this->load->view("registro_espacio",$data);                    
                }                
            }
            
            if($imagenDescriptivaPredefinida){
                unlink('tmp_uploads/default01.png');
            }else{
                unlink($imgDescData['full_path']);
            }

            if($marcadorPredefinido){
                unlink('tmp_uploads/default02.png');
            }else{
                unlink($imgMarcData['full_path']);
            }
            
        }        
    }
    
    /*
     * Función que valida el campo URL mediante FORM VALIDATION
     */
    function urlRelacionada_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlRelacionada_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
   
    /*
     * Función que valida el campo del nombre del espacio de interés mediante FORM VALIDATION
     * Verifica que el nombre aún no esté registrado
     */    
    public function nombreEspacio_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);
        
        if($result){
            $this->form_validation->set_message('nombreEspacio_check', 'El nombre del espacio de interés ya está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }
    }
    
}

<?php

class RegistroServicio extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');        
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');
        $this->load->model('responsable_model');
        $this->load->model('servicio_model');    
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }                
        
        $data['exito'] = false;
        
        $this->form_validation->set_rules('txtNombre', 'Nombre del servicio',  'required|min_length[1]|max_length[100]|callback_nombreServicio_check');
        $this->form_validation->set_rules('txtDescripcion', 'Descripcion del servicio', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtNombreEspacio', 'Nombre del espacio de interés',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check');        
        $this->form_validation->set_rules('txtNombreResponsable', 'Nombre del responsable del servicio',  'required|min_length[1]|max_length[100]|callback_nombreResponsable_check');        

        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
                
        if($this->form_validation->run() == FALSE){
            $data['mensaje'] = "";
            $this->load->view("registro_servicio",$data);
        }else{
            $nombreServicio = $this->input->post("txtNombre");
            $descripcion = $this->input->post("txtDescripcion");
            $nombreEspacio = $this->input->post("txtNombreEspacio");  
            $nombreResponsable = $this->input->post("txtNombreResponsable");
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);          
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];
            $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
            $idResponsable = $this->responsable_model->getIdResponsablePorNombre($nombreResponsable, $idDependencia);
            $this->servicio_model->guardar($nombreServicio, $descripcion, $fechaRegistro, $idEspacioDeInteres ,$idDependencia, $idResponsable);            
            
            //Se registra la actualización
            $this->actualizacion_model->serviciosActualizados($idDependencia, $fechaRegistro);
            
            $data['exito'] = true;
            
            $data['mensaje'] = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" "
                         . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                         . "</button><strong>¡Éxito!  </strong>El servicio se agregó correctamente</div>";
            $this->load->view("registro_servicio",$data);                  
        }   

    }
    
    /*
     * Función que valida el campo nombre de servicio mediante FORM VALIDATION
     */
    public function nombreServicio_check($nombreServicio){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->servicio_model->estaRegistrado($nombreServicio, $idDependencia); 

        if($result){
            $this->form_validation->set_message('nombreServicio_check', 'El servicio ya está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }        
    }

    /*
     * Función que valida el campo nombre de espacio de interés mediante FORM VALIDATION
     */    
    public function nombreEspacio_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);
        
        if(!$result){
            $this->form_validation->set_message('nombreEspacio_check', 'El nombre del espacio de interés no está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }
    }    

    /*
     * Función que valida el campo nombre de responsable mediante FORM VALIDATION
     */        
    public function nombreResponsable_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->responsable_model->estaRegistradoPorNombre($nombre, $idDependencia);
        
        if(!$result){
            $this->form_validation->set_message('nombreResponsable_check', 'El responsable no está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }
    }
        
    
}

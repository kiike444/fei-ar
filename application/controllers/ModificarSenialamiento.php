<?php

class ModificarSenialamiento extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');        
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');   
        $this->load->model('senialamiento_model');        
        $this->load->model('indicacion_model');     
        $this->load->model('actualizacion_model');        
        
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
            redirect('Login');
        }
        if(!$this->session->userdata('id-espacio-cs')){
            redirect('Senialamientos');
        }
        
        $this->form_validation->set_rules('txtNombreEspacio', 'Nombre del espacio de interés',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check');
        $this->form_validation->set_rules('txtArriba', 'Orientación arriba',  'max_length[300]');       
        $this->form_validation->set_rules('txtArribaDerecha', 'Orientación arriba derecha',  'max_length[300]'); 
        $this->form_validation->set_rules('txtDerecha', 'Orientación derecha',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajoDerecha', 'Orientación derecha abajo',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajo', 'Orientación abajo',  'max_length[300]'); 
        $this->form_validation->set_rules('txtAbajoIzquierda', 'Orientación abajo iaquierda',  'max_length[300]'); 
        $this->form_validation->set_rules('txtIzquierda', 'Orientación izquierda',  'max_length[300]'); 
        $this->form_validation->set_rules('txtIzquierdaArriba', 'Orientación izquierda arriba',  'max_length[300]'); 

        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        
        if($this->form_validation->run() == FALSE){
            $data['mensaje'] = "";
            $this->load->view("modificar_senialamiento",$data);
        }else{
            $nombreEspacio =  $this->input->post("txtNombreEspacio");
            $arriba = $this->input->post("txtArriba");
            $arribaDerecha = $this->input->post("txtArribaDerecha");
            $derecha = $this->input->post("txtDerecha");
            $derechaAbajo = $this->input->post("txtAbajoDerecha");
            $abajo = $this->input->post("txtAbajo");
            $abajoIzquerda = $this->input->post("txtAbajoIzquierda");
            $izquierda = $this->input->post("txtIzquierda");
            $izquierdaArriba = $this->input->post("txtIzquierdaArriba");
            
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);          
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];
            $idEspacioNuevo = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);
            
            if(strlen($arriba) > 0 || strlen($arribaDerecha) > 0 || strlen($derecha) > 0 || strlen($derechaAbajo) > 0 || 
               strlen($abajo) > 0 || strlen($abajoIzquerda) > 0 || strlen($izquierda) > 0 || strlen($izquierdaArriba) > 0){
                
                //Se actualiza el señalamiento
                $idEspacio = $this->session->userdata('id-espacio-cs');  
                $this->senialamiento_model->actualizar($idEspacio, $idEspacioNuevo, $idDependencia);
                
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);                
                
                $idSenialamiento = $this->senialamiento_model->getIdSenialamiento($idEspacioNuevo, $idDependencia);
                
                //Se actualizan las indicaciones
               if(strlen($arriba) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'arriba', $arriba);
                   }else{
                       $this->indicacion_model->guardar($arriba, "arriba", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'arriba');
                   }                   
               }                

               if(strlen($arribaDerecha) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba-derecha');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'arriba-derecha', $arribaDerecha);
                   }else{
                       $this->indicacion_model->guardar($arribaDerecha, "arriba-derecha", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba-derecha');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'arriba-derecha');
                   }                   
               }               
               
               if(strlen($derecha) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'derecha');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'derecha', $derecha);
                   }else{
                       $this->indicacion_model->guardar($derecha, "derecha", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'derecha');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'derecha');
                   }                   
               } 
               
               if(strlen($derechaAbajo) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo-derecha');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'abajo-derecha', $derechaAbajo);
                   }else{
                       $this->indicacion_model->guardar($derechaAbajo, "abajo-derecha", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo-derecha');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'abajo-derecha');
                   }                   
               }                

               if(strlen($abajo) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'abajo', $abajo);
                   }else{
                       $this->indicacion_model->guardar($abajo, "abajo", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'abajo');
                   }                   
               }                
               
               if(strlen($abajoIzquerda) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo-izquierda');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'abajo-izquierda', $abajoIzquerda);
                   }else{
                       $this->indicacion_model->guardar($abajoIzquerda, "abajo-izquierda", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'abajo-izquierda');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'abajo-izquierda');
                   }                   
               } 

               if(strlen($izquierda) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'izquierda');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'izquierda', $izquierda);
                   }else{
                       $this->indicacion_model->guardar($izquierda, "izquierda", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'izquierda');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'izquierda');
                   }                   
               }               

               if(strlen($izquierdaArriba) > 0){
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba-izquierda');
                   if($result){
                       $this->indicacion_model->actualizar($idSenialamiento,$idDependencia, 'arriba-izquierda', $izquierdaArriba);
                   }else{
                       $this->indicacion_model->guardar($izquierdaArriba, "arriba-izquierda", $idSenialamiento ,$idDependencia);
                   }
               }else{
                   $result = $this->indicacion_model->tieneIndicacion($idSenialamiento ,$idDependencia, 'arriba-izquierda');
                   if($result){
                       $this->indicacion_model->eliminar($idSenialamiento,$idDependencia, 'arriba-izquierda');
                   }                   
               }               
               $this->session->set_userdata('id-espacio-cs', $idEspacioNuevo);
                redirect('DescripcionSenialamiento');   
               
            }else{
                $data['mensaje'] = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><button type=\"button\" "
                             . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                             . "</button><strong>¡Algo salió mal!  </strong>Debes ingresar almenos una orientación</div>"; 
            
                $this->load->view("modificar_senialamiento",$data); 
            }             
        }        
    }
    
    /*
     * Función que valida el campo nombre de espacio de interés mediante FORM VALIDATION
     */    
    public function nombreEspacio_check($nombre){
        $idEspacioDeInteres = $this->session->userdata('id-espacio-cs');
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
        $nombreActual = $espacio->nombre;        
        if(strcmp($nombre , $nombreActual ) == 0){
            return TRUE;
        }else{
            $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);

            if(!$result){
                $this->form_validation->set_message('nombreEspacio_check', 'El nombre del espacio de interés no está registrado');
                return FALSE;     
            }else{
                $result = $this->espacio_de_interes_model->tieneSenialamiento($nombre, $idDependencia);
                if($result){
                    $this->form_validation->set_message('nombreEspacio_check', 'El espacio de interés ya cuenta con un señalamiento');
                    return FALSE;                 
                }
                return TRUE;
            }            
        }            
    }    
    
    public function obtenerDatosSenialamiento(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-espacio-cs')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEspacioDeInteres = $this->session->userdata('id-espacio-cs');
                $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
                $nombreEspacio = $espacio->nombre;                
                
                $senialamiento = $this->senialamiento_model->getSenialamientoPorIdEspacio($idEspacioDeInteres, $idDependencia);
                $idSenialamiento = $senialamiento->idSenialamiento;
                $indicaciones = $this->indicacion_model->getIndicacionesDeSenialamiento($idSenialamiento ,$idDependencia);

                $senialamientojson = array('nombreEspacio'  => $nombreEspacio);                 
                
                foreach ($indicaciones as $indicacion)
                {
                    $senialamientojson[str_replace("-", "_",$indicacion->orientacion)] = $indicacion->descripcion ;
                }                             
            } 
                echo json_encode($senialamientojson);       
        }
    }
    
}

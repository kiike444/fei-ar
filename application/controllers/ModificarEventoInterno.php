<?php

class ModificarEventoInterno extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');        
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');   
        $this->load->model('responsable_model');        
        $this->load->model('evento_model');    
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
            redirect('Login');
        }
        if(!$this->session->userdata('id-evento-interno')){
            redirect('Eventos');
        }  
        
        $this->form_validation->set_rules('txtNombreEvento', 'nombre del evento',  'required|min_length[1]|max_length[100]|callback_nombreEvento_check');
        $this->form_validation->set_rules('txtDescripcionEvento', 'descripción del evento', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtFechaInicio', 'fecha de inicio del evento', 'required|min_length[16]|max_length[19]');
        $this->form_validation->set_rules('txtFechaFin', 'fecha de conclusión del evento', 'required|min_length[16]|max_length[19]');                       
        $this->form_validation->set_rules('txtUrlEvento', 'URL relacionada', 'prep_url|max_length[100]|callback_urlRelacionada_check');
        $this->form_validation->set_rules('txtNombreEspacio', 'Nombre del espacio de interés',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check'); 
        
        $this->form_validation->set_message('required', 'El campo %s es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');   
        if($this->form_validation->run() == FALSE){
            $this->load->view("modificar_evento_interno");
        }else{
            $nombreEvento = $this->input->post("txtNombreEvento");
            $descripcion = $this->input->post("txtDescripcionEvento");
            $fechaInicio = $this->input->post("txtFechaInicio");
            if(strlen($fechaInicio) == 16){
                $fechaInicio = $fechaInicio.":00";                
            }            
            $fechaFin = $this->input->post("txtFechaFin");
            if(strlen($fechaFin) == 16){
                $fechaFin = $fechaFin.":00";                
            }
            $urlSitioWeb = $this->input->post("txtUrlEvento");
            $nombreDependencia = $this->session->userdata('nombreDependencia'); 
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $nombreEspacio = $this->input->post("txtNombreEspacio");
            $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia); 
            $idEvento = $this->session->userdata('id-evento-interno');
            $this->evento_model->actualizarEventoInterno($idEvento, $nombreEvento, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $idEspacioDeInteres, $idDependencia);            
            
            //Se registra la actualización
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
            $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);       
            
            redirect('DescripcionEventoInterno');             
        }   
    }
   
   public function nombreEvento_check($nombreEvento){
        $idEvento = $this->session->userdata('id-evento-interno');
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $nombreActual = $this->evento_model->getNombrePorId($idEvento, $idDependencia);
        if(strcmp($nombreEvento , $nombreActual ) == 0){
            return TRUE;
        }else{
            $result =$this->evento_model->estaRegistrado($nombreEvento, $idDependencia);
            if($result){
                $this->form_validation->set_message('nombreEvento_check', 'El nombre del evento ya está registrado');
                return FALSE;     
            }else{
                return TRUE;
            }             
        }             
   }
   
    public function nombreEspacio_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);
        
        if(!$result){
            $this->form_validation->set_message('nombreEspacio_check', 'El espacio de interés no está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }
    }
    
    function urlRelacionada_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlRelacionada_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    /*
     * Función accedida mediante AJAX que envía los datos del servicio a modificar
     */
    public function obtenerDatosEvento(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-evento-interno')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEvento = $this->session->userdata('id-evento-interno');
                $evento = $this->evento_model->getEventoInternoPorId($idEvento, $idDependencia);
                if(!isset($evento)){
                    $evento = $this->evento_model->getEventoInternoSinEspacioPorId($idEvento, $idDependencia);
                    $eventojson = array(
                                       'nombreEspacio'  => 'No asignado aún',
                                       'nombre'     => $evento->nombre,
                                       'descripcion'  => $evento->descripcion,
                                       'fechaInicio' => $evento->fechaInicio,
                                       'fechaFin' => $evento->fechaFin,
                                       'urlSitioWeb' => $evento->urlSitioWeb                 
                    );                     
                }else{
                    $eventojson = array(
                                       'nombreEspacio'  => $evento->nombreEspacio,
                                       'nombre'     => $evento->nombre,
                                       'descripcion'  => $evento->descripcion,
                                       'fechaInicio' => $evento->fechaInicio,
                                       'fechaFin' => $evento->fechaFin,
                                       'urlSitioWeb' => $evento->urlSitioWeb                      
                    );                      
                }                
               
                
                echo json_encode($eventojson);
            }else{
                redirect('Eventos');
            }  
            
        }         
    }       
    
}

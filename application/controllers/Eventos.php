<?php

class Eventos extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');     
        $this->load->model('evento_model');   
        $this->load->model('administrador_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $correo = $this->session->userdata('correo');
            $nombreAdmin = $this->administrador_model->getNombreAdministradorPorCorreo($correo);
            $data["nombreAdmin"] = $nombreAdmin;
            $data["nombreDependencia"] = $nombreDependencia;            
            
            
            $eventosInternos = $this->evento_model->getEventosInternosDeDependencia($idDependencia);
            $eventosInternosSinEspacio = $this->evento_model->getEventosInternosSinEspacioDeDependencia($idDependencia);
            $eventosExternos = $this->evento_model->getEventosExternosDeDependencia($idDependencia);
            $data["eventosInternos"] = $eventosInternos;
            $data["eventosInternosSinEspacio"] = $eventosInternosSinEspacio;
            $data["eventosExternos"] = $eventosExternos;
            $this->load->view('eventos',$data);            
        }        
    }
    
    /*
     * Función accedida por AJAX que recibe los nombres de los eventos
     * que van a ser eliminados.
     */    
    public function eliminarEvento(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {   
            if(!$this->input->post('eventos')){
                echo "fallo";
            }
            else{
                $eventos = $this->input->post('eventos');
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
                foreach ($eventos as &$idEvento) {
                    $this->evento_model->eliminarEventoPorId($idEvento, $idDependencia); 
                }
                
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);                 
                
                echo "exito";
            }    
        }
    }
    
    /*
     * Función que redirige a la descripción de un evento interno
     */    
    public function mandarADesripcionEventoInterno($idEvento){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();   
            $this->session->set_userdata('id-evento-interno', urldecode($idEvento));
            redirect('DescripcionEventoInterno');
        }         
    }  
    
    /*
     * Función que redirige a la descripción de un evento externo
     */    
    public function mandarADesripcionEventoExterno($idEvento){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();   
            $this->session->set_userdata('id-evento-externo', urldecode($idEvento));
            redirect('DescripcionEventoExterno');
        }         
    }    
    
    /*
     * Función que redirige a la página para modificar la información del servicio
     */    
    public function mandarAModificarEventoInterno($nombreEvento){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-evento-interno', urldecode($nombreEvento));
            redirect('ModificarEventoInterno');
        }               
    }       

    /*
     * Función que redirige a la página para modificar la información del servicio
     */    
    public function mandarAModificarEventoExterno($nombreEvento){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-evento-externo', urldecode($nombreEvento));
            redirect('ModificarEventoExterno');
        }               
    }     
    
    private function quitarDatosSinUsarDeSesion(){            
            if($this->session->userdata('id-espacio')){
                $this->session->unset_userdata('id-espacio');
            }
            if($this->session->userdata('id-servicio')){
                $this->session->unset_userdata('id-servicio');
            }                                  
            if($this->session->userdata('id-espacio-cs')){
                $this->session->unset_userdata('id-espacio-cs');
            }                          
            
    }     
    
}

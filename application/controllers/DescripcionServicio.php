<?php


class DescripcionServicio extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('dependencia_model');  
        $this->load->model('servicio_model');  
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-servicio')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idServicio = $this->session->userdata('id-servicio');
                //Se obtiene los datos del servicio que se va a describir
                $servicio = $this->servicio_model->getServicioPorId($idServicio, $idDependencia);
                if(! isset($servicio)){
                    $servicio = $this->servicio_model->getServicioSinEspacioPorId($idServicio, $idDependencia);
                }
              if(! isset($servicio)){
                    $servicio = $this->servicio_model->getServicioSinResponsablePorId($idServicio, $idDependencia);
                }                
                $data["servicio"] = $servicio;
                $this->load->view('descripcion_servicio',$data);                   
            }else{
                redirect('Servicios');
            }         
        }             
    }
}

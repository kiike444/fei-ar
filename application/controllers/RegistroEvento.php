<?php

class RegistroEvento extends CI_Controller{
   function __construct() {
       parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('ManipulacionDeImagenes');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');  
        $this->load->model('evento_model');  
        $this->load->model('marcador_model');    
        $this->load->model('actualizacion_model');
   }
   
   public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }        
        
        $imagenDescriptivaPredefinida = false;
        $marcadorPredefinido = false;
        $data['exito'] = false;
        
        $this->form_validation->set_rules('txtNombreEvento', 'nombre del evento',  'required|min_length[1]|max_length[100]|callback_nombreEvento_check');
        $this->form_validation->set_rules('txtDescripcionEvento', 'descripción del evento', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtFechaInicio', 'fecha de inicio del evento', 'required|exact_length[16]');
        $this->form_validation->set_rules('txtFechaFin', 'fecha de conclusión del evento', 'required|exact_length[16]');                       
        $this->form_validation->set_rules('txtUrlEvento', 'URL relacionada', 'prep_url|max_length[100]|callback_urlRelacionada_check');


        if(empty($_FILES['imagenDescriptiva']['name'])){
            $imagenDescriptivaPredefinida = true;
        }

        if(empty($_FILES['imagenMarcador']['name'])){
            $marcadorPredefinido = true;
        }  
            
        if(!empty($this->input->post("txtNombreLugar"))){
            $this->form_validation->set_rules('txtNombreLugar', 'nombre del lugar del evento',  'required|min_length[1]|max_length[100]');
        }else{
            $this->form_validation->set_rules('txtNombreEspacio', 'Nombre del espacio de interés',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check'); 
        }            
               
        $this->form_validation->set_message('required', 'El campo %s es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        $this->form_validation->set_message('exact_length', 'La longitud de la fecha debe de ser de 19 caracteres');                        
        
        if($this->form_validation->run() == FALSE){
            $data['mensaje'] = "";
            $this->load->view("registro_evento",$data);
        }else{
            $nombre = $this->input->post("txtNombreEvento");
            $descripcion = $this->input->post("txtDescripcionEvento");
            $fechaInicio = $this->input->post("txtFechaInicio");
            $fechaInicio = $fechaInicio.":00";
            $fechaFin = $this->input->post("txtFechaFin");
            $fechaFin = $fechaFin.":00";
            $nombreEspacio = $this->input->post("txtNombreEspacio");
            $urlSitioWeb = $this->input->post("txtUrlEvento");
            $nombreDependencia = $this->session->userdata('nombreDependencia'); 
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];
            $tipo= "";
            if(!empty($this->input->post("txtNombreLugar"))){
                $idEspacioDeInteres = NULL;
                $tipo = "externo";
            }else{
                $idEspacioDeInteres = $this->espacio_de_interes_model->getIdEspacioPorNombre($nombreEspacio, $idDependencia);             
                $tipo = "interno";
            }
            $this->evento_model->guardarInterno($nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $tipo, $fechaRegistro, $idDependencia, $idEspacioDeInteres);
            //Se registra la actualización
            $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);            
            
            $data['exito'] = true;
            
           //mensaje de confirmación
           $data['mensaje'] = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" "
                . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                . "</button><strong>¡Exito!  </strong>El evento se agregó correctamente</div>";             
            
            $config['upload_path'] = 'tmp_uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']	= '2048'; 
            $this->load->library('upload', $config);   
            
            if($this->input->post("txtNombreLugar") != FALSE){
                $idEvento = $this->evento_model->getIdEventoPorNombre($nombre, $idDependencia);
                //Se obtiene el id de un marcador libre para el espacio de interés
                $idMarcador = $this->marcador_model->getIdMarcadorLibre($idDependencia); 
                $lugar = $this->input->post("txtNombreLugar");
                //Se verifica si los archivos de imagen se subieron correctamente
                
                $error = false;
                
                if($this->upload->do_upload('imagenDescriptiva')){
                    $imgDescData = $this->upload->data();
                    $this->manipulaciondeimagenes->escalar($imgDescData['full_path']);
                    $imgDescriptiva = file_get_contents($imgDescData['full_path']);
                }else{
                    if($imagenDescriptivaPredefinida){
                        copy('assets/images/default.png', 'tmp_uploads/default01.png');
                        $imgDescriptiva = file_get_contents('tmp_uploads/default01.png'); 
                    }else{
                        $error = true;
                        $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!  </strong> Verifique que el formato y tamaño del archivo de imagen sea correcto</div>";
                    }                    
                }
                
                if(!$error){
                    if($this->upload->do_upload('imagenMarcador')){
                        $imgMarcData = $this->upload->data();
                        $this->manipulaciondeimagenes->escalar($imgMarcData['full_path']);
                        $this->manipulaciondeimagenes->unirImagenes($imgMarcData['full_path'],'assets/images/framemarkers/'.$idMarcador.'.png');
                        $imgMarcador = file_get_contents($imgMarcData['full_path']);                        
                    }else{
                        if($marcadorPredefinido){
                            copy('assets/images/default.png', 'tmp_uploads/default02.png');
                            $this->manipulaciondeimagenes->unirImagenes('tmp_uploads/default02.png','assets/images/framemarkers/'.$idMarcador.'.png');
                            $imgMarcador = file_get_contents('tmp_uploads/default02.png');   
                        }else{
                            //Si se recibe la imagen de marcador pero su formato es inválido se muestra un error
                            $error = true;
                            $data['status'] = "<div id='alert' class=\"alert alert-danger\" role=\"alert\"><strong>¡Algo salió mal!  </strong>Verifique que el formato y tamaño del archivo de imagen sea correcto</div>";                   
                        }                         
                    }
                    
                    if(!$error){
                        $this->evento_model->guardarExterno($lugar, $imgDescriptiva, $imgMarcador, $idEvento, $idMarcador);  
                        $data['exito'] = true;
                    }
                }
                
                if($imagenDescriptivaPredefinida){
                    unlink('tmp_uploads/default01.png');
                }else{
                    unlink($imgDescData['full_path']);
                }

                if($marcadorPredefinido){
                    unlink('tmp_uploads/default02.png');
                }else{
                    unlink($imgMarcData['full_path']);
                }                
                
            }
            
            $this->load->view("registro_evento",$data);            
        }    
        
   }
   
   public function nombreEvento_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result =$this->evento_model->estaRegistrado($nombre, $idDependencia);
        if($result){
            $this->form_validation->set_message('nombreEvento_check', 'El evento ya está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }       
   }
   
    public function nombreEspacio_check($nombre){
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);
        
        if(!$result){
            $this->form_validation->set_message('nombreEspacio_check', 'El espacio de interés no está registrado');
            return FALSE;     
        }else{
            return TRUE;
        }
    }
    
    function urlRelacionada_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlRelacionada_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }    
   
}

<?php

class Servicios extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');     
        $this->load->model('servicio_model');    
        $this->load->model('administrador_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            
            $correo = $this->session->userdata('correo');
            $nombreAdmin = $this->administrador_model->getNombreAdministradorPorCorreo($correo);
            $data["nombreAdmin"] = $nombreAdmin;
            $data["nombreDependencia"] = $nombreDependencia;            
            
            $servicios = $this->servicio_model->getServiciosDeDependencia($idDependencia);
            $serviciosSinEspacio = $this->servicio_model->getServiciosSinEspacioDeDependencia($idDependencia);
            $serviciosSinResponsable = $this->servicio_model->getServiciosSinResponsablesDeDependencia($idDependencia);
            $data["servicios"] = $servicios;
            $data["serviciosSinEspacio"] = $serviciosSinEspacio;
            $data["serviciosSinResponsable"] = $serviciosSinResponsable;
            $this->load->view('servicios',$data);            
        }        
    }
    
    /*
     * Función que redirige a la descripción del servicio
     */    
    public function mandarADesripcion($idServicio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-servicio', urldecode($idServicio));
            redirect('DescripcionServicio');
        }         
    }
    
    /*
     * Función que redirige a la página para modificar la información del servicio
     */    
    public function mandarAModificar($idServicio){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else { 
            $this->quitarDatosSinUsarDeSesion();             
            $this->session->set_userdata('id-servicio', urldecode($idServicio));
            redirect('ModificarServicio');
        }               
    }    
 

    /*
     * Función accedida por AJAX que recibe los nombres de los servicios
     * que van a ser eliminados.
     */    
    public function eliminarServicio(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {   
            if(!$this->input->post('servicios')){
                echo "fallo";
            }
            else{
                $servicios = $this->input->post('servicios');
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
                foreach ($servicios as &$idServicio) {
                    $this->servicio_model->eliminarServicioPorId($idServicio, $idDependencia); 
                }
                
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->serviciosActualizados($idDependencia, $fechaRegistro);                    
                
                echo "exito";
            }    
        }
    }   
    
    private function quitarDatosSinUsarDeSesion(){
            if($this->session->userdata('id-espacio')){
                $this->session->unset_userdata('id-espacio');
            }                      
            if($this->session->userdata('id-evento-interno')){
                $this->session->unset_userdata('id-evento-interno');
            }            
            if($this->session->userdata('id-evento-externo')){
                $this->session->unset_userdata('id-evento-externo');
            }               
            if($this->session->userdata('id-espacio-cs')){
                $this->session->unset_userdata('id-espacio-cs');
            }                   
    }    
    
    
}

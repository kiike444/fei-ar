<?php

class DescripcionEventoInterno extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('dependencia_model');  
        $this->load->model('evento_model');        
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-evento-interno')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEvento = $this->session->userdata('id-evento-interno');
                //Se obtiene los datos del evento interno que se va a describir
                $evento = $this->evento_model->getEventoInternoPorId($idEvento, $idDependencia);
                if(! isset($evento)){
                    $evento = $this->evento_model->getEventoInternoSinEspacioPorId($idEvento, $idDependencia);
                }
                $data["evento"] = $evento;
                $this->load->view('descripcion_evento_interno',$data);                   
            }else{
                redirect('Eventos');
            }         
        }        
    }
}

<?php

class DescripcionSenialamiento extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('dependencia_model');  
        $this->load->model('espacio_de_interes_model');
        $this->load->model('senialamiento_model');  
        $this->load->model('indicacion_model');         
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-espacio-cs')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                
                $idEspacioDeInteres = $this->session->userdata('id-espacio-cs');
                $senialamiento = $this->senialamiento_model->getSenialamientoPorIdEspacio($idEspacioDeInteres, $idDependencia);
                $idSenialamiento = $senialamiento->idSenialamiento;
                $indicaciones = $this->indicacion_model->getIndicacionesDeSenialamiento($idSenialamiento ,$idDependencia);
                
                $data["senialamiento"] = $senialamiento;
                $data["indicaciones"] = $indicaciones;
                $this->load->view('descripcion_senialamiento',$data);                   
            }else{
                redirect('Senialamientos');
            }            
        }        
    }
    
}

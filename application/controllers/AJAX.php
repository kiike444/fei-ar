<?php

class AJAX extends CI_Controller{
    
    function __construct() {
        parent::__construct(); 
        $this->load->library('session');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');   
        $this->load->model('responsable_model');               
    }
    
    /*
     * Función accedida mediante AJAX 
     *  Devuelve los nombres de los espacios de interés registrados en la dependencia
     */
    public function getEspaciosDeInteresRegistrados(){
        if (!$this->session->userdata('correo')){
          redirect('login');
        }                        
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $result = $this->espacio_de_interes_model->getNombresEspaciosDeDependencia($idDependencia);
        echo json_encode($result);        
    }

    /*
     * Función accedida mediante AJAX 
     *  Devuelve los nombres y correos de los responsables registrados en la dependencia
     */    
    public function getResponsablesRegistrados(){
        if (!$this->session->userdata('correo')){
          redirect('login');
        }                        
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $result = $this->responsable_model->getResponsablesDeDependencia($idDependencia);
        echo json_encode($result);        
    }    
    
    public function getEspaciosDeInteresRegistradosSinSenialamiento(){
        if (!$this->session->userdata('correo')){
          redirect('login');
        }                        
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $result = $this->espacio_de_interes_model->getNombresEspaciosDeDependenciaSinSenialamientos($idDependencia);
        if(!isset($result)){
            $result = false;
        }
        echo json_encode($result);     
    }        
    
}

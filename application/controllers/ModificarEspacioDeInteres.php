<?php

class ModificarEspacioDeInteres extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('ManipulacionDeImagenes');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');  
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
            redirect('Login');
        }
        if(!$this->session->userdata('id-espacio')){
            redirect('EspaciosDeInteres');
        }
        
        $data['exito'] = false;
        
        $this->form_validation->set_rules('txtNombre', 'Nombre del espacio',  'required|min_length[1]|max_length[100]|callback_nombreEspacio_check');
        $this->form_validation->set_rules('txtDescripcion', 'Descripción del espacio', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtUrl', 'Url relacionada', 'prep_url|max_length[100]|callback_urlRelacionada_check');
               
        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
                
        if($this->form_validation->run() == FALSE){
            $data['status'] = "";
            $this->load->view("modificar_espacio_de_interes",$data);
        }else{
            $nombre = $this->input->post("txtNombre");
            $descripcion = $this->input->post("txtDescripcion");
            $urlSitioWeb = $this->input->post("txtUrl");   
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            $idEspacioDeInteres = $this->session->userdata('id-espacio');
            $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
            $nombreEspacioActual = $espacio->nombre;            
            $imgDescriptiva = NULL;
            $imgMarcador  = NULL;
            
            if (!empty($_FILES['imagenDescriptiva']['name']) || !empty($_FILES['imagenMarcador']['name']))
            {
                $config['upload_path'] = 'tmp_uploads/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size']	= '2048'; 
                $this->load->library('upload', $config);
                
                if(!empty($_FILES['imagenDescriptiva']['name']) && $this->upload->do_upload('imagenDescriptiva')){
                    $imgDescData = $this->upload->data();
                    $this->manipulaciondeimagenes->escalar($imgDescData['full_path']);
                    $imgDescriptiva = file_get_contents($imgDescData['full_path']);                 
                }
                
                if(!empty($_FILES['imagenMarcador']['name']) && $this->upload->do_upload('imagenMarcador')){
                    $imgMarcData = $this->upload->data();
                     $this->manipulaciondeimagenes->escalar($imgMarcData['full_path']);
                     $idMarcador = $espacio->idMarcador;
                     $result = $this->manipulaciondeimagenes->unirImagenes($imgMarcData['full_path'],'assets/images/framemarkers/'.$idMarcador.'.png');
                     if($result){
                        $imgMarcador = file_get_contents($imgMarcData['full_path']);  
                     }  
                }
            }
            $this->espacio_de_interes_model->actualizar($nombre,$descripcion,$urlSitioWeb,$imgDescriptiva,$imgMarcador,$nombreEspacioActual,$idDependencia);
            //Se registra la actualización
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
            $this->actualizacion_model->espaciosDeInteresActualizados($idDependencia, $fechaRegistro);            
            
            if($imgDescriptiva != NULL){
                unlink($imgDescData['full_path']);
            }
            if($imgMarcador != NULL){
                unlink($imgMarcData['full_path']);
            }            
            
            $this->session->set_userdata('id-espacio', $idEspacioDeInteres);
            redirect('DescripcionEspacioDeInteres');
        }        
        
    }
    
    /*
     * Función que valida el campo URL mediante FORM VALIDATION.
     */
    function urlRelacionada_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlRelacionada_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    /*
     * Función que valida el campo nombre del espacio de interés mediante FORM VALIDATION.
     * Comprueba que el nombre no esté registrado
     */    
    public function nombreEspacio_check($nombre){
        $idEspacioDeInteres = $this->session->userdata('id-espacio');
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
        $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
        $nombreActual = $espacio->nombre;
        if(strcmp($nombre , $nombreActual ) == 0){
            return TRUE;
        }else{
            $result = $this->espacio_de_interes_model->estaRegistrado($nombre, $idDependencia);

            if($result){
                $this->form_validation->set_message('nombreEspacio_check', 'El nombre del espacio de interés ya está registrado');
                return FALSE;     
            }else{
                return TRUE;
            }               
        }
    }  
    
   /*
    * Función accedida mediante AJAX que envía los datos actuales del espacio de interés
    * para que sean cargados en el formulario.
    */
    public function obtenerDatosEspacio(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-espacio')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEspacioDeInteres = $this->session->userdata('id-espacio');
                $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
                $espaciojson = array(
                                   'nombre'  => $espacio->nombre,
                                   'descripcion'     => $espacio->descripcion,
                                   'urlSitioWeb'  => $espacio->urlSitioWeb,
                                   'imagenDescriptiva' => base64_encode($espacio->imagenDescriptiva),
                                   'imagenMarcador' => base64_encode($espacio->imagenMarcador),
                                   'fechaRegistro' => $espacio->fechaRegistro                    
                );                
                
                echo json_encode($espaciojson);
            }else{
                redirect('EspaciosDeInteres');
            }  
            
        }         
    }    
    
}

<?php

class ServidorDeAvisos extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('dependencia_model');   
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }
        
        $data['exito'] = false;
        
        $this->form_validation->set_rules('txtUrl', 'URL servidor de avisos', 'prep_url|max_length[100]|callback_urlServidorAvisos_check');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');
        
        if($this->form_validation->run() == FALSE){
            $data['mensaje'] = "";
            $this->load->view("servidor_de_avisos",$data);
        }else{
            $urlServidorAvisos = $this->input->post("txtUrl");
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  
            $this->dependencia_model->actualizarUrlServidorAvisos($idDependencia, $urlServidorAvisos); 
            
           //Se registra la actualización
           $fecha = getdate();
           $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
           $this->actualizacion_model->dependenciaActualizada($idDependencia, $fechaRegistro);            
            
           $data['exito'] = true;
           
           $data['mensaje'] = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" "
                . "class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>"
                . "</button><strong>¡Exito!  </strong>La URL del servidor de avisos se actualizó de forma correcta correctamente</div>";               
            
            $this->load->view("servidor_de_avisos", $data);
        }        
    }
    
    /*
     * Función que valida el campo URL mediante FORM VALIDATION
     */    
    function urlServidorAvisos_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlServidorAvisos_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }        
    
    public function obtenerUrlServidorAvisos(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $url = $this->dependencia_model->getUrlServidorAvisosPorNombre($nombreDependencia);
                if($url != NULL){
                    echo $url;
                }else{
                    echo 'No';
                }
        }             
    }
    
}

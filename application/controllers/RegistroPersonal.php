<?php

class RegistroPersonal extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('simple_html_dom');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');  
        $this->load->model('responsable_model');
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }
        
        $data['exito'] = false;
        
        //Se definen las reglas de validación dependiendo de los datos que sean recibidos
        //Modo básico
        if(isset($_POST['txtNombre']) || isset($_POST['txtCorreo'])){
            $this->form_validation->set_rules('txtNombre', 'Nombre del responsable',  'required|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('txtCorreo', 'Correo', 'required|min_length[9]|max_length[50]|callback_correo_check');            
        }
        //Modo avanzado
        if(isset($_POST['txtUrl'])){
            $this->form_validation->set_rules('txtUrl', 'Url sitio personal', 'max_length[100]|callback_urlPersonal_check');                
        }
        
        $this->form_validation->set_message('required', 'El campo es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');         
        
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $data['mensaje'] ='';
        
        if($this->form_validation->run() == FALSE){
            $responsables = $this->responsable_model->getResponsablesDeDependencia($idDependencia);
            $data['responsables'] = $responsables;
            $this->load->view("registro_responsable",$data);
        }

        //Si se reciben estos dos campos significa que el proceso para agregar persona será el modo básico
        if ($this->input->post("txtNombre") || $this->input->post("txtCorreo")) { 
                    $nombre = $this->input->post("txtNombre");
                    $correoInstitucional = $this->input->post("txtCorreo");
                    $fecha = getdate();
                    $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];                   
                    if(strlen($nombre)>0 && strlen($correoInstitucional)>0){
                        $this->responsable_model->guardar($nombre, $correoInstitucional, 'responsable', $fechaRegistro, $idDependencia);

                        //Se registra la actualización
                        $this->actualizacion_model->responsablesActualizados($idDependencia, $fechaRegistro);                     

                        $data['exito'] = true;
                    
                        $data['mensaje'] = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" '
                                    . 'data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                                    . '<strong>¡Exito! </strong> Responsable agregado exitosamente </div>';
                    }
                    $responsables = $this->responsable_model->getResponsablesDeDependencia($idDependencia);
                    $data['responsables'] = $responsables;
                    $this->load->view("registro_responsable",$data);
        }

        //Si es recibido el parametro de URL de página de personal académico
        //por lo dando el proceso será el modo avanzado
        if($this->input->post("txtUrl")){
                $urlSitioPersonal = $this->input->post("txtUrl"); 
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];                                         

                $contExitos = 0;
                //Se obtienen los datos de la página de personal académico
                $responsablesWeb = $this->webScrapingPersonalUV($urlSitioPersonal);
                //Si se obtuvieron datos se mandan a guardar
                if($responsablesWeb != NULL){
                    for($i = 0; $i<count($responsablesWeb); $i++){
                        $nombre = $responsablesWeb[$i]['nombre'];
                        $correoInstitucional = $responsablesWeb[$i]['correoInstitucional'];
                        if( strpos($correoInstitucional, "@") != FALSE && !$this->responsable_model->estaRegistrado($correoInstitucional, $idDependencia)){
                            $this->responsable_model->guardar($nombre, $correoInstitucional, 'responsable',$fechaRegistro, $idDependencia);
                            $contExitos++;
                        }
                    }
                    
                    //Se registra la actualización
                    $this->actualizacion_model->responsablesActualizados($idDependencia, $fechaRegistro);  
                    
                    $data['exito'] = true;
                    
                    //Mensaje de confirmación 
                    $data['mensaje'] = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" '
                            . 'data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                            . '<strong>¡Exito! </strong> '.$contExitos.' Responsables agregados</div>';               
                }else{
                    //No se pueden encontrar responsables en la pagina
                    $data['mensaje'] = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" '
                                . 'data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                                . '<strong>¡Algo salió mal! </strong> No es posible encontrar datos de responsables en la página web </div>';                                       
                }
                //Se obtienen los responsables registrados en la dependencia y se mandan a mostrar
                if(strpos( $urlSitioPersonal,"uv.mx") != FALSE){
                    $responsables = $this->responsable_model->getResponsablesDeDependencia($idDependencia);
                    $data['responsables'] = $responsables;
                    $this->load->view("registro_responsable",$data);                     
                }               
        }
    }
    
    /*
     * Función que valida el campo correo mediante FORM VALIDATION
     */
    public function correo_check($correo){
        $result = filter_var($correo, FILTER_VALIDATE_EMAIL);
        
        if (!$result){
            $this->form_validation->set_message('correo_check', 'Ingrese un correo con formato correo@uv.mx');
            return FALSE;
        }
        else{
            $nombreDependencia = $this->session->userdata('nombreDependencia');
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
            if($this->responsable_model->estaRegistrado($correo, $idDependencia)){
                $this->form_validation->set_message('correo_check', 'El correo '.$correo.' ya está registrado');
                return FALSE;                
            }
            return TRUE;
        }        
    }
  
    /*
     * Función que valida el campo url de página de personal académico mediante FORM VALIDATION
     */    
    public function urlPersonal_check($url){ 
        $result = filter_var($url, FILTER_VALIDATE_URL);
        $pos = strpos($url, "www.uv.mx");
        
        if (!$result || $pos == FALSE){
            $this->form_validation->set_message('urlPersonal_check', 'Ingrese una URL de personal académico con dominio www.uv.mx');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
  
    /*
     * Función que obtiene los datos de personal mediante una URL de una página de
     * personal académico de la UV.
     * No hay un estandar para estas páginas, asi que solo están soportadas las 4 formas más
     * comunes de estructurar estas páginas.
     * 
     * Recibe -> URL de página de personal académico de UV
     * Regresa -> Arreglo de nombres y correos del personal encontrado
     */     
    private function webScrapingPersonalUV($url){
        $dom = file_get_html($url);
        $responsables = array();
        $resultado=true;
        try{
            if(count($dom->find('#GridView1')) > 0){
                foreach($dom->find('#GridView1 tr') as $fila) {
                    if(!$fila->find('th')){
                        $responsable = array(
                            "nombre" => $fila->find('td', 0)->plaintext,
                            "correoInstitucional" => $fila->find('td', 1)->plaintext
                        );                
                        $responsables[] = $responsable;
                    }
                }              
            }else{
                if(count($dom->find('table.contenedor')) > 0){
                    foreach($dom->find('table.contenedor tr td li') as $li) {
                            $responsable = array(
                                "nombre" => $li->find('span.nombre',0)->plaintext,
                                "correoInstitucional" => $li->find('a.correo',0)->plaintext
                            );                
                            $responsables[] = $responsable;
                    }                
                }else{
                    if(count($dom->find('tr strong')) > 0 && count($dom->find('tr td h3')) < 1){
                        foreach($dom->find('tr') as $fila) {
                            if(!$fila->find('strong')){
                                $responsable = array(
                                    "nombre" => $fila->find('td', 1)->plaintext,
                                    "correoInstitucional" => $fila->find('td', 2)->plaintext
                                );
                                $responsables[] = $responsable;                
                            }
                        }                    
                    }else{ 
                        if(count($dom->find('tr td h3')) > 0){
                            foreach($dom->find('tr') as $fila) {
                                if(!$fila->find('strong')){
                                    $responsable = array(
                                        "nombre" => $fila->find('td', 0)->plaintext,
                                        "correoInstitucional" => $fila->find('td', 1)->plaintext
                                    );
                                    $responsables[] = $responsable;                
                                }
                            }                        
                        }else{
                            $resultado=false;
                        }
                    }
                }
            }            
        }  catch (Exception $e) {
            $resultado=false;   
        }
        
        if($resultado){
            return $responsables;
        }else{
            return NULL;
        }
        
    }
    
    
    /*
     * Función accedida por AJAX que recibe los correos de los responsables que
     * van a ser eliminados
     */
    public function eliminarResponsable(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        } else {   
            if(!$this->input->post('responsables')){
                echo "fallo";
            }
            else{
                $responsables = $this->input->post('responsables');
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia); 
                $count = 0;
                foreach ($responsables as &$correoResponsable) {
                    $this->responsable_model->eliminar($correoResponsable, $idDependencia); 
                }
                
                //Se registra la actualización
                $fecha = getdate();
                $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
                $this->actualizacion_model->responsablesActualizados($idDependencia, $fechaRegistro);                
                
                echo "exito";
            }    
        }
    }    
    
    
}

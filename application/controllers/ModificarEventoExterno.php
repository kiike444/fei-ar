<?php

class ModificarEventoExterno extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');        
        $this->load->library('ManipulacionDeImagenes');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model');   
        $this->load->model('responsable_model');        
        $this->load->model('evento_model');    
        $this->load->model('actualizacion_model');
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
            redirect('Login');
        }
        if(!$this->session->userdata('id-evento-externo')){
            redirect('Eventos');
        }          
        
        $this->form_validation->set_rules('txtNombreEvento', 'nombre del evento',  'required|min_length[1]|max_length[100]|callback_nombreEvento_check');
        $this->form_validation->set_rules('txtDescripcionEvento', 'descripción del evento', 'required|min_length[1]|max_length[300]');
        $this->form_validation->set_rules('txtFechaInicio', 'fecha de inicio del evento', 'required|min_length[16]|max_length[19]');
        $this->form_validation->set_rules('txtFechaFin', 'fecha de conclusión del evento', 'required|min_length[16]|max_length[19]');                       
        $this->form_validation->set_rules('txtUrlEvento', 'URL relacionada', 'prep_url|max_length[100]|callback_urlRelacionada_check');
        $this->form_validation->set_rules('txtNombreLugar', 'nombre del lugar del evento',  'required|min_length[1]|max_length[100]');
        
        $this->form_validation->set_message('required', 'El campo %s es requerido');
        $this->form_validation->set_message('min_length', 'Longitud de datos inválida');
        $this->form_validation->set_message('max_length', 'La longitud del dato introducido en este campo excede el límite permitido');   
        
        if($this->form_validation->run() == FALSE){
            $this->load->view("modificar_evento_externo");
        }else{
            $nombreEvento = $this->input->post("txtNombreEvento");
            $descripcion = $this->input->post("txtDescripcionEvento");
            $fechaInicio = $this->input->post("txtFechaInicio");
            if(strlen($fechaInicio) == 16){
                $fechaInicio = $fechaInicio.":00";                
            }            
            $fechaFin = $this->input->post("txtFechaFin");
            if(strlen($fechaFin) == 16){
                $fechaFin = $fechaFin.":00";                
            }
            $urlSitioWeb = $this->input->post("txtUrlEvento");
            $lugar = $this->input->post("txtNombreLugar");
            $nombreDependencia = $this->session->userdata('nombreDependencia'); 
            $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);  
            $idEvento = $this->session->userdata('id-evento-externo');              
       
            $imgDescriptiva = NULL;
            $imgMarcador  = NULL;             

            if (!empty($_FILES['imagenDescriptiva']['name']) || !empty($_FILES['imagenMarcador']['name'])){
                $config['upload_path'] = 'tmp_uploads/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size']	= '2048'; 
                $this->load->library('upload', $config);  
                if(!empty($_FILES['imagenDescriptiva']['name']) && $this->upload->do_upload('imagenDescriptiva')){
                    $imgDescData = $this->upload->data();
                    $this->manipulaciondeimagenes->escalar($imgDescData['full_path']);
                    $imgDescriptiva = file_get_contents($imgDescData['full_path']);                 
                }                
                
                if(!empty($_FILES['imagenMarcador']['name']) && $this->upload->do_upload('imagenMarcador')){
                     $imgMarcData = $this->upload->data();
                     $this->manipulaciondeimagenes->escalar($imgMarcData['full_path']);
                     $evento = $this->evento_model->getEventoExterno($nombreEvento, $idDependencia);
                     $idMarcador = $evento->idMarcador;
                     $this->manipulaciondeimagenes->unirImagenes($imgMarcData['full_path'],'assets/images/framemarkers/'.$idMarcador.'.png');
                     $imgMarcador = file_get_contents($imgMarcData['full_path']);  
                }                
                
            }            

            if($imgDescriptiva != NULL){
                unlink($imgDescData['full_path']);
            }
            if($imgMarcador != NULL){
                unlink($imgMarcData['full_path']);
            }             
            $this->evento_model->actualizarEventoInterno($idEvento, $nombreEvento, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, NULL, $idDependencia);
            $this->evento_model->actualizarEventoExterno($idEvento, $lugar,  $imgDescriptiva, $imgMarcador);
            
            //Se registra la actualización
            $fecha = getdate();
            $fechaRegistro = $fecha['year']."-".$fecha['mon']."-".$fecha['mday']." ".$fecha['hours'].":".$fecha['minutes'].":".$fecha['seconds'];       
            $this->actualizacion_model->eventosActualizados($idDependencia, $fechaRegistro);            
            
            $this->load->view("modificar_evento_externo");
            redirect('DescripcionEventoExterno');
        }             
    }
    
   public function nombreEvento_check($nombreEvento){
        $idEvento = $this->session->userdata('id-evento-externo');
        $nombreDependencia = $this->session->userdata('nombreDependencia');
        $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);        
        $nombreActual = $this->evento_model->getNombrePorId($idEvento, $idDependencia);
        if(strcmp($nombreEvento , $nombreActual ) == 0){
            return TRUE;
        }else{
            $result =$this->evento_model->estaRegistrado($nombreEvento, $idDependencia);
            if($result){
                $this->form_validation->set_message('nombreEvento_check', 'El nombre del evento ya está registrado');
                return FALSE;     
            }else{
                return TRUE;
            }             
        }             
   }
   
    function urlRelacionada_check($url){
        if($url == NULL){
            return TRUE;
        }
        
        $result = filter_var($url, FILTER_VALIDATE_URL);
        
        if (!$result){
            $this->form_validation->set_message('urlRelacionada_check', 'Ingrese una URL válida');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    /*
     * Función accedida mediante AJAX que envía los datos del servicio a modificar
     */
    public function obtenerDatosEvento(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-evento-externo')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEvento = $this->session->userdata('id-evento-externo');
                $evento = $this->evento_model->getEventoExternoPorId($idEvento, $idDependencia);
                $eventojson = array(
                    'lugar' => $evento->lugar,
                    'nombre'     => $evento->nombre,
                    'descripcion'  => $evento->descripcion,
                    'fechaInicio' => $evento->fechaInicio,
                    'fechaFin' => $evento->fechaFin,
                    'urlSitioWeb' => $evento->urlSitioWeb,
                    'imagenDescriptiva' => base64_encode($evento->imagenDescriptiva),
                    'imagenMarcador' => base64_encode($evento->imagenMarcador)
                );                                    
                echo json_encode($eventojson);
            }else{
                redirect('Eventos');
            }  
            
        }         
    }       
    
}

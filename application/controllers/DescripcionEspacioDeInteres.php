<?php

class DescripcionEspacioDeInteres extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model'); 
        $this->load->model('servicio_model'); 
        $this->load->model('evento_model'); 
    }
    
    public function index(){
        if (!$this->session->userdata('correo')){
          redirect('Login');
        }else{
            if($this->session->userdata('id-espacio')){
                $nombreDependencia = $this->session->userdata('nombreDependencia');
                $idDependencia = $this->dependencia_model->getIdPorNombre($nombreDependencia);
                $idEspacioDeInteres = $this->session->userdata('id-espacio');
                //Se obtienen los datos del espacio de interés que se va a describir
                $espacio = $this->espacio_de_interes_model->getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia);
                $data["espacio"] = $espacio;
                //Se obtienen los servicios asociados al espacio de interés
                $servicios = $this->servicio_model->getServiciosDeEspacioDeInteres($idEspacioDeInteres, $idDependencia);
                $data["servicios"] = $servicios;
                //Se obtienen los eventos internos asociados al espacio de interés
                $eventos = $this->evento_model->getEventosDeEspacioDeInteres($idEspacioDeInteres, $idDependencia);
                $data["eventos"] = $eventos;                
                $this->load->view('descripcion_espacio_de_interes',$data);                   
            }else{
                redirect('EspaciosDeInteres');
            }         
        }      
    }
}

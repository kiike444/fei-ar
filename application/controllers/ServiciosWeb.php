<?php
require("application/libraries/REST_Controller.php");

class ServiciosWeb extends REST_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('dependencia_model');
        $this->load->model('espacio_de_interes_model'); 
        $this->load->model('servicio_model');   
        $this->load->model('responsable_model');
        $this->load->model('evento_model');  
        $this->load->model('indicacion_model');
        $this->load->model('actualizacion_model'); 
        $this->load->library('geometria');
    }

    public function estoyEnDependencia_post(){
		$longitud = floatval($this->post("longitud"));
                $latitud = floatval($this->post("latitud"));
                $dependencias = $this->dependencia_model->getDependenciasRegistradas();
                $dep = -1;
                foreach ($dependencias as $dependencia)
                {
                   $dpCoordenadas =  $dependencia->coordenadas;
                   list($r1a,$r1b,$r1c,$r1d) = explode(",", $dpCoordenadas);
                   $lat_lo = floatval ($r1a);
                   $lng_lo = floatval ($r1b);
                   $lat_hi = floatval ($r1c);
                   $lng_hi = floatval ($r1d);                   
                   $resultado = $this->geometria->puntoEnRectanculo($lat_lo, $lng_lo, $lat_hi, $lng_hi, $longitud, $latitud);
                   if($resultado){
                        $dep = new stdClass();
                        $dep->idDependencia = $dependencia->idDependencia;
                        $dep->nombre = $dependencia->nombre;
                        break;
                   }                       
                }                
		$this->response($dep);
    }    
    
    //http://localhost/feiar/index.php/ServiciosWeb/getDependencia/format/json?idDependencia=3
    public function getDependencia_post(){
        $idDependencia = $this->post("idDependencia");
        $dependencia = $this->dependencia_model->getDependenciaParaCliente($idDependencia);
        $this->response($dependencia);        
    }
    
    public function getEspaciosDeInteres_post(){
        $idDependencia = $this->post("idDependencia");
        $espaciosDeInteres = $this->espacio_de_interes_model->getEspaciosDeDependenciaParaCliente($idDependencia);
        $this->response($espaciosDeInteres);
    }
    
    public function getServicios_post(){
        $idDependencia = $this->post("idDependencia");
        $servicios = $this->servicio_model->getServiciosDeDependenciaParaCliente($idDependencia);
        $this->response($servicios);        
    }
    
    public function getResponsables_post(){
        $idDependencia = $this->post("idDependencia");        
        $responsables =  $this->responsable_model->getResponsablesDeDependenciaParaCliente($idDependencia);  
        $this->response($responsables);
    }
    
    public function getEventosInternos_post(){
        $idDependencia = $this->post("idDependencia");
        $eventosInternos = $this->evento_model->getEventosInternosParaCliente($idDependencia);
        $this->response($eventosInternos);        
    }
    
    ////http://localhost:8080/dar/codeigniter/index.php/RestServer/usuario/format/json?nombre=Enrique
    public function getEventosExternos_post(){
        $idDependencia = $this->post("idDependencia");
        $eventosExternos = $this->evento_model->getEventosExternosParaCliente($idDependencia);
        $this->response($eventosExternos);
    }    
    
    public function getIndicaciones_post(){
        $idDependencia = $this->post("idDependencia");
        $indicaciones = $this->indicacion_model->getIndicacionesParaCliente($idDependencia);
        $this->response($indicaciones);        
    }
    
    public function getActualizaciones_post(){
        $idDependencia = $this->post("idDependencia");
        $actualizaciones = $this->actualizacion_model->getActualizaciones($idDependencia);
        $this->response($actualizaciones); 
    }
}

<?php

class senialamiento_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Función que guarda un señalamiento
     * @param string $fechaRegistro
     * @param int $idEspacioDeInteres
     * @param int $idDependencia
     * @return boolean
     */
    public function guardar($fechaRegistro, $idEspacioDeInteres ,$idDependencia){
       $data=array(
            'fechaRegistro' => $fechaRegistro,
            'idEspacioDeInteres' => $idEspacioDeInteres,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->insert('Senialamiento', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }    
    
    /**
     * Función que obtiene el ID de un señalamiento
     * @param int $idEspacioDeInteres
     * @param int $idDependencia
     * @return null
     */
    public function getIdSenialamiento($idEspacioDeInteres, $idDependencia){
        $sql = "SELECT idSenialamiento FROM Senialamiento WHERE idEspacioDeInteres = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($idEspacioDeInteres,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idSenialamiento;
        }else{
            return NULL;
        }      
    }    
    
    /**
     * Función que obtiene todos los señalamientos de una dependencia
     * @param int $idDependencia
     * @return arreglo de señalamientos
     */
    public function getSenialamientosDeDependencia($idDependencia){
        $sql="select EspacioDeInteres.idEspacioDeInteres, EspacioDeInteres.nombre, Senialamiento.idSenialamiento, Senialamiento.fechaRegistro FROM EspacioDeInteres "
                . "LEFT JOIN Senialamiento ON (EspacioDeInteres.idEspacioDeInteres = Senialamiento.idEspacioDeInteres) "
                . "WHERE Senialamiento.idDependencia = ?";
        
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }              
    }
    
    /**
     * Elimina el señalamiento de un espacio de interés
     * @param int $idEspacioDeInteres
     * @param int $idDependencia
     * @return boolean
     */
    public function eliminarSenialamientoPorIdEspacio($idEspacioDeInteres, $idDependencia){
       $data=array(
            'idEspacioDeInteres' => $idEspacioDeInteres,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Senialamiento', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }
    
    /**
     * Obtiene el señalamiento de un espacio de interés
     * @param int $idEspacioDeInteres
     * @param int $idDependencia
     * @return señalamiento
     */
    public function getSenialamientoPorIdEspacio($idEspacioDeInteres, $idDependencia){
        $sql = 'select EspacioDeInteres.nombre, EspacioDeInteres.imagenDescriptiva, '
                . 'Senialamiento.idSenialamiento, Senialamiento.fechaRegistro, Senialamiento.idSenialamiento '
                . 'FROM EspacioDeInteres LEFT JOIN Senialamiento '
                . 'ON (EspacioDeInteres.idEspacioDeInteres = Senialamiento.idEspacioDeInteres) '
                . 'WHERE EspacioDeInteres.idEspacioDeInteres = ? AND Senialamiento.idDependencia = ?';
        $query = $this->db->query($sql, array($idEspacioDeInteres, $idDependencia));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }
    
    /**
     * Función que actualiza un señalamiento
     * @param type $idEspacioActual
     * @param type $idEspacioDeInteres
     * @param type $idDependencia
     * @return boolean
     */
    public function actualizar($idEspacioActual, $idEspacioDeInteres, $idDependencia){
       $data=array(
            'idEspacioDeInteres' => $idEspacioDeInteres
	);             
        
        $where = array(
                       'idDependencia' => $idDependencia,
                       'idEspacioDeInteres' => $idEspacioActual
        );
        
        $this->db->update('Senialamiento', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }                
    }
    
    
}

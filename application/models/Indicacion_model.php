<?php

class indicacion_model extends CI_Model{
    
    /**
     * Función que guarda una indicaci{on de un señalamiento
     * @param string $descripcion
     * @param string $orientacion
     * @param int $idSenialamiento
     * @param int $idDependencia
     * @return boolean
     */
    public function guardar($descripcion, $orientacion, $idSenialamiento ,$idDependencia){
       $data=array(
            'descripcion' => $descripcion,
            'orientacion' => $orientacion,
            'idSenialamiento' => $idSenialamiento,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->insert('Indicacion', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }    
    
    /**
     * Función que obtiene las indicaciones de un señalamiento
     * @param int $idSenialamiento
     * @param int $idDependencia
     * @return arreglo de indicaciones
     */
    public function getIndicacionesDeSenialamiento($idSenialamiento ,$idDependencia){
        $sql = "select Indicacion.descripcion, Indicacion.orientacion FROM Indicacion "
                . "LEFT JOIN Senialamiento ON (Indicacion.idSenialamiento = Senialamiento.idSenialamiento) "
                . "WHERE  Indicacion.idSenialamiento =  ? AND  Indicacion.idDependencia = ?";
        $query = $this->db->query($sql, array($idSenialamiento, $idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }              
    }
    
    /**
     * Función que verifica si un señalamiento tiene una indicación en una orientación
     * @param int $idSenialamiento
     * @param int $idDependencia
     * @param string $orientacion
     * @return boolean
     */
    public function tieneIndicacion($idSenialamiento ,$idDependencia, $orientacion){
        $sql = 'SELECT * FROM Indicacion WHERE idSenialamiento = ? AND orientacion = ? '
                . 'AND idDependencia = ?';
        $query = $this->db->query($sql, array($idSenialamiento, $orientacion, $idDependencia));
        
        if($query->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }        
    }
    
    /**
     * Función que actualiza una indicación de un señalamiento
     * @param int $idSenialamiento
     * @param int $idDependencia
     * @param string $orientacion
     * @param string $descripcion
     * @return boolean
     */
    public function actualizar($idSenialamiento,$idDependencia, $orientacion, $descripcion){
       $data=array(
            'descripcion' => $descripcion
	);             
        
        $where = array(
                       'idSenialamiento' => $idSenialamiento,
                       'idDependencia' => $idDependencia,
                       'orientacion' => $orientacion
        );
        
        $this->db->update('Indicacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }            
    }
    
    /**
     * Función que elimina una indicación de un señalamiento
     * @param int $idSenialamiento
     * @param int $idDependencia
     * @param string $orientacion
     * @return boolean
     */
    public function eliminar($idSenialamiento,$idDependencia, $orientacion){
       $data=array(
            'idSenialamiento' => $idSenialamiento,
            'idDependencia' => $idDependencia,
            'orientacion' => $orientacion
	);        
        
        $this->db->delete('Indicacion', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }            
    }
    
    /**
     * Función que obtiene las indicaciones de todos los señalamientos de una dependencia
     * para la aplicación móvil.
     * @param int $idDependencia
     * @return arreglo de indicaciones
     */
    public function getIndicacionesParaCliente($idDependencia){
        $sql='SELECT Indicacion.idIndicacion, Indicacion.descripcion, Indicacion.orientacion, '
                . 'Senialamiento.idEspacioDeInteres FROM Indicacion '
                . 'INNER JOIN Senialamiento '
                . 'ON (Indicacion.idSenialamiento = Senialamiento.idSenialamiento) '
                . 'WHERE Senialamiento.idDependencia = ?';
        
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }              
    }
    
}

<?php

class dependencia_model extends CI_Model {
    public function __construct(){
        parent:: __construct();
        $this->load->library('geometria');
    }
    
    /**
     * Función que recibe los datos de una dependencia y los almacena en la base de datos
     * @param string $nombre - Nombre de la dependencia
     * @param string $coordenadas - Coordenadas de la dependencia (Obtenidas por Google Maps)
     * @param string $urlServidorAvisos - URL del servidor de avisos (Si es que se cuenta con uno)
     * @param string $fechaRegistro - Fecha de registro de la dependencia
     * @return boolean
     *      TRUE = Si los datos se guardaron correctamente
     *      FALSE = Si no se guardaron los datos
     */  
    public function guardar($nombre, $coordenadas, $urlServidorAvisos, $fechaRegistro){
       $data=array(
            'nombre' => $this->db->escape_str($nombre),
            'coordenadas' => $coordenadas,
            'urlServidorAvisos' => $urlServidorAvisos,
            'fechaRegistro' => $fechaRegistro
	);        
        
        $this->db->insert('Dependencia', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
/**
 * Función que verifica si el nombre de una dependencia está ocupado por otra ya
 * registrada
 * @param string $nombre - Nombre de la dependencia
 * @return boolean
 *      TRUE = Si ya existe otra dependencia con ese nombre
 *      FALSE = Si no hay otra dependencia con ese nombre
 */    
    public function estaRegistrada($nombre){
        $sql = "SELECT * FROM Dependencia WHERE nombre = ?";
        $query = $this->db->query($sql, array($nombre));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Función que recibe coordenadas de una dependencia y verifica que no se
     * encimen con las coordenadas de las dependencias ya registradas
     * @param string $coordenadas - Coordenadas de dependencia (Obtenidas por Google Maps)
     * @return boolean
     *      TRUE = Si ya existe otra dependencia en esas coordenadas
     *      FALSE = Si no hay otra dependencia en esas coordenadas
     */
    public function coordenadasRegistradas($coordenadas){
        $sql = "SELECT coordenadas FROM Dependencia";
        $query = $this->db->query($sql);
        
        foreach ($query->result_array() as $row)
        {
           $resultado = $this->geometria->rectangulosSolapados($coordenadas,$row['coordenadas']);
           if($resultado){
               return true;
           }
        }        
        return false;
    }
    
    
    /**
     * Función que obtiene y regresa las coordenadas de todas las dependencias
     * registradas
     * @return array - Arreglo de coordenadas de las dependencias registradas
     */
    public function getCoordenadasRegistradas(){
        $sql = "SELECT coordenadas FROM Dependencia";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
     * Función que regresa la clave de la dependencia mendiante su nombre
     * @param string $nombre - Nombre de la dependencia
     * @return int  - Clave de la dependencia
     *      Regresa NULL si no hay una dependencia con ese nombre
     */
    public function getIdPorNombre($nombre){
        $sql = "SELECT idDependencia FROM Dependencia WHERE nombre=?";
        $query = $this->db->query($sql, array($nombre));
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idDependencia;
        }else{
            return NULL;
        }
    }
    
    /**
     * Función que regresa el nombre de la dependencia mendiante su clave
     * @param int $idDependencia - Clave de la dependencia
     * @return string  - Nombre de la dependencia
     *      Regresa NULL si no hay una dependencia con esa clave
     */    
    public function getNombrePorId($idDependencia){
        $sql = "SELECT nombre FROM Dependencia WHERE idDependencia=?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->nombre;
        }else{
            return NULL;
        }
    }    
    
    /**
     * Función que regresa los nombres de las dependencias que están registradas
     * @return array - Arreglo de los nombres de las dependencias registradas
     */
    public function getNombresDependenciasRegistradas(){
        $sql = "SELECT nombre FROM Dependencia";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
     * Función que actualiza la URL del servidor de avisos de una dependencia
     * @param int $idDependencia - La clave de la dependencia
     * @param string $urlServidorAvisos - La URL del servidor
     * @return boolean
     *      TRUE = Si la actualización se llevó a cabo
     *      FALSE = Si la actualización no se llevó a cabo
     */
    public function actualizarUrlServidorAvisos($idDependencia, $urlServidorAvisos){
       $data=array(
            'urlServidorAvisos' => $urlServidorAvisos
	);             
       
        $where = array(
            'idDependencia' => $idDependencia
        );

        $this->db->update('Dependencia', $data, $where ); 
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }             
    }
    
    /**
     * Función que obtiene la URL del servidor de avisos de una dependencia
     * @param string $nombre - Nombre de la dependencia
     * @return string urlServidorAvisos
     */
    public function getUrlServidorAvisosPorNombre($nombre){
        $sql = "SELECT urlServidorAvisos FROM Dependencia WHERE nombre=?";
        $query = $this->db->query($sql, array($nombre));
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->urlServidorAvisos;
        }else{
            return NULL;
        }
    }
    
    /**
     * Función que obtiene todas las dependencia registradas
     * @return arreglo Dependencia
     */
    public function getDependenciasRegistradas(){
        $sql = "SELECT * FROM Dependencia";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    
    /**
     * Función que obtiene los datos de una dependencia para la aplicación móvil
     * @param int $idDependencia
     * @return Dependencia
     */
    public function getDependenciaParaCliente($idDependencia){
        $sql='SELECT idDependencia, nombre, urlServidorAvisos FROM Dependencia WHERE idDependencia = ?';
        
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->row(); 
        }else{
            return NULL;
        }              
    }    
    
}

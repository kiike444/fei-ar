<?php

class evento_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    
    /**
     * Función que guarda un evento interno
     * @param string $nombre
     * @param string $descripcion
     * @param string $fechaInicio
     * @param string $fechaFin
     * @param string $urlSitioWeb
     * @param string $tipo
     * @param string $fechaRegistro
     * @param int $idDependencia
     * @param int $idEspacio
     * @return boolean
     */
    public function guardarInterno($nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $tipo , $fechaRegistro, $idDependencia, $idEspacio){
       $data=array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'fechaInicio' => $fechaInicio,
            'fechaFin' => $fechaFin,
            'urlSitioWeb' => $urlSitioWeb,
            'tipo' => $tipo,
            'fechaRegistro' => $fechaRegistro,
            'idDependencia' => $idDependencia,
            'idEspacioDeInteres' => $idEspacio
	);        
        
        $this->db->insert('Evento', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }        
    }
    
    /**
     * Función que guarda un evento externo
     * @param string $lugar
     * @param blob $imagenDescriptiva
     * @param blob $imagenMarcador
     * @param int $idEvento
     * @param int $idMarcador
     * @return boolean
     */
    public function guardarExterno($lugar, $imagenDescriptiva, $imagenMarcador, $idEvento, $idMarcador){
       $data=array(
            'lugar' => $lugar,
            'imagenDescriptiva' => $imagenDescriptiva,
            'imagenMarcador' => $imagenMarcador,
            'idEvento' => $idEvento,
            'idMarcador' => $idMarcador           
	);        
        
        $this->db->insert('EventoExterno', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }            
    }
    
    /**
     * Función que determina si un evento esta registrado en una dependencia
     * @param string $nombre   -> Nombre del espacio de interés
     * @param int $idDependencia
     * @return boolean
     */
    public function estaRegistrado($nombre, $idDependencia){
        $sql = "SELECT * FROM Evento WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre, $idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }        
    }
    
    /**
     * Función que obtiene los eventos internos de una dependencia
     * @param int $idDependencia
     * @return arreglo de eventos internos
     */
    public function getEventosInternosDeDependencia($idDependencia){
        $sql = "SELECT Evento.idEvento, Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, "
                . 'Evento.urlSitioWeb, Evento.fechaRegistro, EspacioDeInteres.nombre AS "nombreEspacio" '
                . "FROM Evento INNER JOIN EspacioDeInteres ON (Evento.idDependencia = EspacioDeInteres.idDependencia "
                . "AND Evento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . 'WHERE Evento.idDependencia = ? AND Evento.tipo = "interno"';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }    

    /**
     * Función que obtiene los eventos internos de una dependencia que NO están
     * asociados con un espacio de interés porque fué eliminado
     * @param int $idDependencia
     * @return arreglo de eventos internos
     */
    public function getEventosInternosSinEspacioDeDependencia($idDependencia){
        $sql = "SELECT idEvento, nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, fechaRegistro "
                . 'FROM Evento WHERE idDependencia = ? AND tipo = "interno" AND idEspacioDeInteres IS NULL';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }    
    
    /**
     * Función que obtiene los eventos externos de una dependencia
     * @param int $idDependencia
     * @return arreglo de eventos externos
     */
    public function getEventosExternosDeDependencia($idDependencia){
        $sql = "SELECT Evento.idEvento, Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, Evento.urlSitioWeb, "
                . "Evento.fechaRegistro, EventoExterno.lugar, EventoExterno.imagenDescriptiva, "
                . "EventoExterno.imagenMarcador FROM Evento INNER JOIN EventoExterno "
                . 'ON (Evento.idEvento = EventoExterno.idEvento) WHERE Evento.idDependencia = ? AND Evento.tipo = "externo" ';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }
    
    /**
     * Fcunción que elimina un evento por su nombre
     * @param string $nombre
     * @param int $idDependencia
     * @return boolean
     */
    public function eliminarEventoPorNombre($nombre, $idDependencia){
       $data=array(
            'nombre' => $nombre,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Evento', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }   

    /**
     * Función que elimina un evento por su ID
     * @param int $idEvento
     * @param int $idDependencia
     * @return boolean
     */
    public function eliminarEventoPorId($idEvento, $idDependencia){
       $data=array(
            'idEvento' => $idEvento,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Evento', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }       
    
    /**
     * Función que obtiene un evento interno por su nombre
     * @param string $nombre
     * @param int $idDependencia
     * @return EventoInterno
     */
    public function getEventoInterno($nombre, $idDependencia){
        $sql = "SELECT Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, "
                . 'Evento.urlSitioWeb, Evento.fechaRegistro, EspacioDeInteres.imagenDescriptiva, EspacioDeInteres.nombre AS "nombreEspacio" '
                . "FROM Evento INNER JOIN EspacioDeInteres ON (Evento.idDependencia = EspacioDeInteres.idDependencia "
                . "AND Evento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . "WHERE Evento.nombre = ? AND  Evento.idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    

    /**
     * Función que obtiene un evento interno por su ID
     * @param int $idEvento
     * @param int $idDependencia
     * @return EventoInterno
     */
    public function getEventoInternoPorId($idEvento, $idDependencia){
        $sql = "SELECT Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, "
                . 'Evento.urlSitioWeb, Evento.fechaRegistro, EspacioDeInteres.imagenDescriptiva, EspacioDeInteres.nombre AS "nombreEspacio" '
                . "FROM Evento INNER JOIN EspacioDeInteres ON (Evento.idDependencia = EspacioDeInteres.idDependencia "
                . "AND Evento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . "WHERE Evento.idEvento = ? AND  Evento.idDependencia = ?";
        $query = $this->db->query($sql, array($idEvento,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }      
    
    /**
     * Función que obtiene un evento interno de una dependencia que NO
     * tiene un espacio de interés asociado porque fué eliminado.
     * @param string $nombre   -> nombre del evento interno
     * @param int $idDependencia
     * @return EventoInterno
     */
    public function getEventoInternoSinEspacio($nombre, $idDependencia){
        $sql =  "SELECT nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, fechaRegistro "
                . 'FROM Evento WHERE Evento.nombre = ? AND idDependencia = ? AND tipo = "interno" AND idEspacioDeInteres IS NULL';
        $query = $this->db->query($sql, array($nombre,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    

    /**
     * Función que obtiene un evento interno sin espacio asociado mediante su ID
     * @param int $idEvento
     * @param int $idDependencia
     * @return EventoInterno
     */
    public function getEventoInternoSinEspacioPorId($idEvento, $idDependencia){
        $sql =  "SELECT nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, fechaRegistro "
                . 'FROM Evento WHERE Evento.idEvento = ? AND idDependencia = ? AND tipo = "interno" AND idEspacioDeInteres IS NULL';
        $query = $this->db->query($sql, array($idEvento,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    
    
    /**
     * Función que obtiene un evento externo mediante su nombre
     * @param string $nombre
     * @param int $idDependencia
     * @return EventoExterno
     */
    public function getEventoExterno($nombre, $idDependencia){
        $sql = "SELECT Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, Evento.urlSitioWeb, "
                . "Evento.fechaRegistro, EventoExterno.lugar, EventoExterno.imagenDescriptiva, "
                . "EventoExterno.imagenMarcador, EventoExterno.idMarcador FROM Evento INNER JOIN EventoExterno "
                . "ON (Evento.idEvento = EventoExterno.idEvento) WHERE Evento.nombre = ? AND Evento.idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    

    /**
     * Función que obtiene un evento externo mediante su ID
     * @param int $idEspacio
     * @param int $idDependencia
     * @return EventoExterno
     */
    public function getEventoExternoPorId($idEspacio, $idDependencia){
        $sql = "SELECT Evento.nombre, Evento.descripcion, Evento.fechaInicio, Evento.fechaFin, Evento.urlSitioWeb, "
                . "Evento.fechaRegistro, EventoExterno.lugar, EventoExterno.imagenDescriptiva, "
                . "EventoExterno.imagenMarcador, EventoExterno.idMarcador FROM Evento INNER JOIN EventoExterno "
                . "ON (Evento.idEvento = EventoExterno.idEvento) WHERE Evento.idEvento = ? AND Evento.idDependencia = ?";
        $query = $this->db->query($sql, array($idEspacio,$idDependencia));
                   
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }     
    
    /**
     * Función que actualiza un evento interno
     * @param int $idEvento
     * @param string $nombre
     * @param string $descripcion
     * @param string $fechaInicio
     * @param string $fechaFin
     * @param string $urlSitioWeb
     * @param int $idEspacio
     * @param int $idDependencia
     * @return boolean
     */
    public function actualizarEventoInterno($idEvento, $nombre, $descripcion, $fechaInicio, $fechaFin, $urlSitioWeb, $idEspacio, $idDependencia){
       $data=array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'fechaInicio' => $fechaInicio,
            'fechaFin' => $fechaFin,
            'urlSitioWeb' => $urlSitioWeb,
            'idEspacioDeInteres' => $idEspacio
	);             
       
        $where = array(
            'idDependencia' => $idDependencia,
            'idEvento' => $idEvento
        );

        $this->db->update('Evento', $data, $where ); 
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }                     
    }
    
    /**
     * Función que actualiza un evento externo
     * @param int $idEvento
     * @param string $lugar
     * @param blob $imgDescriptiva
     * @param blob $imgMarcador
     * @return boolean
     */
    public function actualizarEventoExterno($idEvento, $lugar,  $imgDescriptiva, $imgMarcador){

        if($imgDescriptiva == NULL && $imgMarcador == NULL){
            $data = array(
                           'lugar' => $lugar
                        );    
        }else{
            
            if($imgDescriptiva != NULL && $imgMarcador == NULL){
                $data = array(
                            'lugar' => $lugar,
                            'imagenDescriptiva' => $imgDescriptiva
                            );                   
            }else{
                if($imgDescriptiva == NULL && $imgMarcador != NULL){
                    $data = array(
                                   'lugar' => $lugar,
                                   'imagenMarcador' => $imgMarcador
                                );                         
                }else{
                    $data = array(
                                'lugar' => $lugar,
                                'imagenDescriptiva' => $imgDescriptiva,
                                'imagenMarcador' => $imgMarcador  
                                );                        
                }
            }
        }
        
        $where = array(
            'idEvento' => $idEvento
        );
        
        $this->db->update('EventoExterno', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }                     
    }
    
    /**
     * Función que obtiene un evento interno por su nombre
     * @param string $nombre
     * @param int $idDependencia
     * @return EventoInterno
     */
    public function getIdEventoPorNombre($nombre, $idDependencia){
        $sql = "SELECT idEvento FROM Evento WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idEvento;
        }else{
            return NULL;
        }      
    }    
    
    /**
     * Función que obtiene los eventos relacioandos con un espacio de interés
     * @param int $idEspacioDeInteres
     * @param int $idDependencia
     * @return arreglo de eventos
     */
    public function getEventosDeEspacioDeInteres($idEspacioDeInteres, $idDependencia){
        $sql = "SELECT Evento.nombre, Evento.fechaInicio, Evento.fechaFin "
                . "FROM Evento INNER JOIN EspacioDeInteres ON (Evento.idDependencia = EspacioDeInteres.idDependencia "
                . "AND Evento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . 'WHERE EspacioDeInteres.idEspacioDeInteres = ? AND Evento.idDependencia = ? AND Evento.tipo = "interno"';   
        $query = $this->db->query($sql, array($idEspacioDeInteres,$idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }           
    }
    
    /**
     * Función que obtiene el nombre de un evento mediante su ID
     * @param int $idEvento
     * @param int $idDependencia
     * @return string
     */
    public function getNombrePorId($idEvento, $idDependencia){
        $sql = "SELECT nombre FROM Evento WHERE idEvento = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($idEvento,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->nombre;
        }else{
            return NULL;
        }        
    }
    
    /**
     * Función que obtiene los eventos internos de una dependencia para la 
     * aplicación web
     * @param int $idDependencia
     * @return arreglo de EventoInterno
     */
    public function getEventosInternosParaCliente($idDependencia){
        $sql='SELECT idEvento, nombre, descripcion, fechaInicio, fechaFin, '
                . 'urlSitioWeb, tipo , idEspacioDeInteres FROM Evento '
                . 'WHERE Evento.idDependencia = ? AND Evento.tipo = "interno"';
        
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }           
    }
    
    /**
     * Función que obtiene los eventos externos de una dependencia para la aplicación
     * móvil
     * @param int $idDependencia
     * @return arreglo de EventoExterno
     */
    public function getEventosExternosParaCliente($idDependencia){
        $sql='SELECT Evento.idEvento, Evento.nombre, Evento.descripcion, Evento.fechaInicio, '
                . 'Evento.fechaFin, Evento.urlSitioWeb, Evento.tipo, EventoExterno.lugar, '
                . 'EventoExterno.idMarcador FROM Evento INNER JOIN EventoExterno '
                . 'ON (Evento.idEvento = EventoExterno.idEvento) '
                . 'WHERE Evento.idDependencia = ? AND Evento.tipo = "externo"';
        
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }             
    }
    
}

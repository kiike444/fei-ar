<?php

class marcador_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

    /**
     * Función que busca y regresa la clave de un marcador libre para una
     * dependencia.
     * @param int $idDependencia - Clave de la dependencia
     * @return int - Clave del marcador libre
     */
    public function getIdMarcadorLibre($idDependencia){
        $sql = "SELECT Marcador.idMarcador from Marcador  LEFT JOIN ("
                . "((select EspacioDeInteres.idMarcador FROM EspacioDeInteres WHERE idDependencia = ?)"
                . "UNION (select EventoExterno.idMarcador FROM EventoExterno LEFT JOIN Evento ON "
                . "(Evento.idEvento = EventoExterno.idEvento) where idDependencia = ?) ) aux) ON "
                . "(aux.idMarcador = Marcador.idMarcador) WHERE aux.idMarcador IS NULL";       
        
        $query = $this->db->query($sql, array($idDependencia, $idDependencia));
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idMarcador;
        }else{
            return -1;
        }            
    }
    
    /**
     * Función que busca si existen marcadores libres para la dependencia.
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si existen marcadores libres para la dependencia.
     *      FALSE = Si no existen marcadores libres para la dependencia.
     */
    public function hayMarcadoresLibres($idDependencia){
        $sql = "SELECT EspacioDeInteres.idMarcador from EspacioDeInteres where idDependencia=? UNION "
                . "(SELECT EventoExterno.idMarcador from EventoExterno left JOIN Evento ON "
                . "(Evento.idEvento = EventoExterno.idEvento) where idDependencia=?)";     
        
        $query = $this->db->query($sql, array($idDependencia, $idDependencia));   
        
        if($query->num_rows() == 512){
            return false;
        }else{
            return true;
        }          
    }
}

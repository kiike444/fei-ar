<?php

class espacio_de_interes_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Función que recibe los datos de un espacio de interés y los almacena en 
     * en la base de datos.
     * @param string $nombre - Nombre de la dependencia
     * @param string $descripcion - Descripción de la dependencia
     * @param string $urlSitioWeb - URL de sitio web relacionado (Si es que existe)
     * @param string $imgDescriptiva - Imagen descriptiva convertida en string
     * @param string $imgMarcador - Imagen de marcador convertida en string
     * @param string $fechaRegistro - Fecha de registro del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada al espacio de interés
     * @param int $idMarcador - Clave del marcador asociado al lugar de interés
     * @return boolean
     *      TRUE = Si el espacio de guardó correctamente
     *      FALSE = Si el espacio no se pudo guardar
     */
    public function guardar($nombre, $descripcion, $urlSitioWeb, $imgDescriptiva, $imgMarcador,$fechaRegistro, $idDependencia, $idMarcador){
       $data=array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'urlSitioWeb' => $urlSitioWeb,
            'imagenDescriptiva' => $imgDescriptiva,
            'imagenMarcador' => $imgMarcador,
            'fechaRegistro' => $fechaRegistro,
            'idDependencia' => $idDependencia,
            'idMarcador' => $idMarcador
	);        
        
        $this->db->insert('EspacioDeInteres', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }        
    
    /**
     * Función que verifica si un espacio de interés ya está registrado en la 
     * dependencia medainte su nombre y la clave de su dependencia.
     * @param string $nombre - Nombre del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada al espacio
     * @return boolean
     */
    public function estaRegistrado($nombre, $idDependencia){
        $sql = "SELECT * FROM EspacioDeInteres WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre, $idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Función que obtiene los espacios de interés registrados en una dependencia.
     * @param int $idDependencia - Clave de la dependencia 
     * @return array asociativo {nombre, descripcion, imagenDescriptiva, imagenMarcador, fechaRegistro}
     *      NULL = Si no hay espacios registrados en la dependencia
     */
    public function getEspaciosDeDependencia($idDependencia){
        $sql = "SELECT idEspacioDeInteres, nombre, descripcion, imagenDescriptiva, imagenMarcador, fechaRegistro "
                . "FROM EspacioDeInteres WHERE idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }

    /**
     * Función que obtiene y regresa los nombres de los espacios de interés
     * registrados en una dependencia
     * @param int $idDependencia - Clave de la depedencia
     * @return array string - Arreglo de nombres de los espacios de interés.
     *      NULL = Si no hay espacios de interés en la dependencia
     */
    public function getNombresEspaciosDeDependencia($idDependencia){
        $sql = "SELECT nombre FROM EspacioDeInteres WHERE idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }    
    
    /**
     * Función que elimina de la base de datos un espacio de interés mediante
     * su nombre y la clave de la dependencia sociada.
     * @param string $nombre - Nombre del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return boolean
     *      TRUE = Si el espacio se eliminó correctamente
     *      FALSE = Si no se pudo eliminar el espacio
     */
    public function eliminarEspacioPorNombre($nombre, $idDependencia){
       $data=array(
            'nombre' => $nombre,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('EspacioDeInteres', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }

    /**
     * Función que elimina de la base de datos un espacio de interés mediante
     * su nombre y la clave de la dependencia sociada.
     * @param int $idEspacio - Clave del espacio de interés
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si el espacio se eliminó correctamente
     *      FALSE = Si no se pudo eliminar el espacio 
     */
    public function eliminarEspacioPorId($idEspacio, $idDependencia){
       $data=array(
            'idEspacioDeInteres' => $idEspacio,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('EspacioDeInteres', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }    
    
    
    /**
     * Función que obtiene los datos de un espacio de interés mediante su nombre
     * y la clave de su dependencia asociada
     * @param string $nombre - Nombre del lugar de interés
     * @param id $idDependencia - Clave de la dependencia asociada
     * @return array asociativo {nombre, descripcion, urlSitioWeb, 
     *          imagenDescriptiva, imagenMarcador, fechaRegistro, idMarcador}
     *      NULL = Si no hay un espacio de interés con ese nombre en la dependencia
     */
    public function getEspacioDeInteres($nombre, $idDependencia){
        $sql = "SELECT nombre, descripcion, urlSitioWeb, imagenDescriptiva, imagenMarcador, fechaRegistro, idMarcador "
                . "FROM EspacioDeInteres WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }

    /**
     * Función que obtiene los datos de un espacio de interés mediante su nombre
     * y la clave de su dependencia asociada
     * @param int $idEspacioDeInteres - Clave del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return array asociativo {nombre, descripcion, urlSitioWeb, 
     *          imagenDescriptiva, imagenMarcador, fechaRegistro, idMarcador}
     *      NULL = Si no hay un espacio de interés con ese nombre en la dependencia
     */
    public function getEspacioDeInteresPorId($idEspacioDeInteres, $idDependencia){
        $sql = "SELECT nombre, descripcion, urlSitioWeb, imagenDescriptiva, imagenMarcador, fechaRegistro, idMarcador "
                . "FROM EspacioDeInteres WHERE idEspacioDeInteres = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($idEspacioDeInteres,$idDependencia));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    
    
    
    /**
     * Función que actualiza un espacio de interés registrado en la base de datos
     * mediante los nuevos datos recibidos.
     * @param string $nombre - Nuevo nombre del espacio de interés
     * @param string $descripcion - Nueva descripción del lugar de interés
     * @param string $urlSitioWeb - Nueva URL de sitio web asociado (Si es que existe)
     * @param string $imgDescriptiva - Nueva imagen descriptiva convertida en string
     * @param string $imgMarcador - Nueva imagen de marcador convertida en string
     * @param string $nombreActual - Nueva nombre actual del nombre del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return boolean
     *      TRUE = Si la actualización fue correcta
     *      FALSE = Si la actualización falló
     */
    public function actualizar($nombre,$descripcion,$urlSitioWeb,$imgDescriptiva,$imgMarcador,$nombreActual,$idDependencia){
                        
        if($imgDescriptiva == NULL && $imgMarcador == NULL){
            $data = array(
                           'nombre' => $nombre,
                           'descripcion' => $descripcion,
                           'urlSitioWeb' => $urlSitioWeb
                        );    
        }else{
            
            if($imgDescriptiva != NULL && $imgMarcador == NULL){
                $data = array(
                               'nombre' => $nombre,
                               'descripcion' => $descripcion,
                               'urlSitioWeb' => $urlSitioWeb,
                               'imagenDescriptiva' => $imgDescriptiva
                            );                   
            }else{
                if($imgDescriptiva == NULL && $imgMarcador != NULL){
                    $data = array(
                                   'nombre' => $nombre,
                                   'descripcion' => $descripcion,
                                   'urlSitioWeb' => $urlSitioWeb,
                                   'imagenMarcador' => $imgMarcador
                                );                         
                }else{
                    $data = array(
                                   'nombre' => $nombre,
                                   'descripcion' => $descripcion,
                                   'urlSitioWeb' => $urlSitioWeb,
                                   'imagenDescriptiva' => $imgDescriptiva,
                                   'imagenMarcador' => $imgMarcador
                                );                        
                }
            }
        }
        
        $where = array(
                       'idDependencia' => $idDependencia,
                       'nombre' => $nombreActual
        );

        $this->db->update('EspacioDeInteres', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }              
    }
    
    /**
     * Función que obtiene y regresa la clave de un espacio de interés mediante
     * su nombre y la clave de su dependenca asociada.
     * @param string $nombre - Nombre del espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return int Clave del espacio de interés
     */
    public function getIdEspacioPorNombre($nombre, $idDependencia){
        $sql = "SELECT idEspacioDeInteres FROM EspacioDeInteres WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idEspacioDeInteres;
        }else{
            return NULL;
        }      
    }
    
    /**
     * Función que obtiene los nombres de los espacios de interes de una
     * dependencia que no cuentan con señalamientos
     * @param int $idDependencia
     * @return arreglo de string
     */
    public function getNombresEspaciosDeDependenciaSinSenialamientos($idDependencia){
        $sql="SELECT EspacioDeInteres.nombre FROM EspacioDeInteres LEFT JOIN Senialamiento "
                . "ON ( EspacioDeInteres.idDependencia = Senialamiento.idDependencia "
                . "AND Senialamiento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . "WHERE EspacioDeInteres.idDependencia = ? AND Senialamiento.idEspacioDeInteres IS NULL";
    
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }               
        
    }
    
    /**
     * Función que verifica si un espacio de interés cuenta con un señalamiento
     * @param string $nombre  - > Nombre del espacio de interés
     * @param int $idDependencia
     * @return boolean
     */
    public function tieneSenialamiento($nombre, $idDependencia){
        $sql = "SELECT * FROM EspacioDeInteres LEFT JOIN Senialamiento "
                . "ON ( EspacioDeInteres.idDependencia = Senialamiento.idDependencia "
                . "AND Senialamiento.idEspacioDeInteres = EspacioDeInteres.idEspacioDeInteres) "
                . "WHERE EspacioDeInteres.nombre = ? AND EspacioDeInteres.idDependencia = ? "
                . "AND Senialamiento.idEspacioDeInteres IS NOT NULL";
        $query = $this->db->query($sql, array($nombre, $idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }    
    
    /**
     * Función que obtiene todos los espacios de interés de una dependencia para
     * la aplicación móvil
     * @param type $idDependencia
     * @return null
     */
    public function getEspaciosDeDependenciaParaCliente($idDependencia){
        $sql = "SELECT idEspacioDeInteres, nombre, descripcion, urlSitioWeb, idMarcador "
                . "FROM EspacioDeInteres WHERE idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }    
   
}

<?php

class administrador_model extends CI_Model{
   
    public function __construct(){
        parent:: __construct();
    }
  
    /**
     * Función que guarda la cuenta de un administrador
     * @param int $idResponsable
     * @param string $contrasenia
     * @param int $idDependencia
     * @return boolean
     *      TRUE = Si la cuenta se guardó correctamente
     *      FALSE = Si la cuenta no se pudo guardar
     */
    public function guardarCuenta($idResponsable, $contrasenia, $idDependencia){
        $data=array(
            'contrasenia' => $contrasenia,
            'idResponsable' => $idResponsable,
            'idDependencia' => $idDependencia
	);     
        
        $this->db->insert('Cuenta', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }        
    }
    
    /**
     * Función que verifica si un administrador ya está registrado mediante su correo
     * @param string $correo - Correo del administrador
     * @return boolean
     *      TRUE = Si está registrado
     *      FALSE = Si no estpa registrado
     */
    public function estaRegistrado($correo, $idDependencia){
        $sql = "select * from Responsable WHERE tipo = 'administrador' AND correoInstitucional = ?";
        //$sql = "select * from responsable WHERE tipo = 'administrador' AND correoInstitucional = ? AND idDependencia = ?";
        //$query = $this->db->query($sql, array($correo, $idDependencia));
        $query = $this->db->query($sql, array($correo));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Función que verifica si una persona ya está registrada como responsable mediante su correo
     * @param string $correo - Correo del responsable
     * @return boolean
     *      TRUE = Si está registrado
     *      FALSE = Si no estpa registrado
     */
    public function esResponsable($correo, $idDependencia){
        $sql = "select * from Responsable WHERE tipo = 'responsable' AND correoInstitucional = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($correo, $idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * Función que convierte una cuenta de responsable a una de administrador
     * @param int $idResponsable - Id del responsable
     * @return boolean
     *      TRUE = Si todo sale bien
     *      FALSE = Si hay error
     */    
    public function convertirAdministrador($idResponsable){ 
       $data=array(
            'tipo' => 'administrador'
	);             
        $where = array(
            'idResponsable' => $idResponsable
        );
        $this->db->update('Responsable', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }           
    }    
    
    
   /**
     * Función que obtiene la clave de la dependencia a la que el administrador
     * está asociado mediante el correo del administrador
     * @param string $correo - Correo del administrador
     * @return int - La clave de la dependencia
     *      Puede regresar NULL si el correo no corresponde al de un administrador
     */
    public function getIdDependenciaPorCorreo($correo){
        $sql = "SELECT idDependencia FROM Responsable WHERE correoInstitucional =?";
        $query = $this->db->query($sql, array($correo));    
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idDependencia;
        }else{
            return NULL;
        }    
        
    } 
    
    
    /**
     * Función que verifica si una cuenta de administrador es compatible con los 
     * datos introducidos en el inicio de sesión 
     * @param string $correo
     * @param string $contrasenia
     * @return boolean
     *      TRUE = Los datos de la cuenta son válidos
     *      FALSE = Los datos no corresponden a ninguna cuenta
     */
    public function verificarCuenta($correo,$contrasenia){
        $sql = "SELECT * FROM Responsable INNER JOIN Cuenta 
            ON (Responsable.idResponsable = Cuenta.idResponsable)
            WHERE Responsable.tipo = 'administrador' AND 
            Responsable.correoInstitucional = ?  AND Cuenta.contrasenia= ?";
        $query = $this->db->query($sql, array($correo, $contrasenia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }      
    
    /**
     * Función que obtiene los administradores de una dependencia
     * @param int $idDependencia - Clave de la dependencia
     * @return arreglo asociativo {nombre, correoInstitucional}
     */
    public function getAdministradores($idDependencia){
        $sql = 'SELECT nombre, correoInstitucional FROM Responsable '
                . 'WHERE tipo = "administrador" AND idDependencia = ?';
         $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }     
        
    }
    
    /**
     * Función que obtiene el nombre de un administrador mediante su correo
     * @param string $correo - Correo del administrador
     * @return string nombre - Nombre del administrador
     */
    public function getNombreAdministradorPorCorreo($correo){
        $sql = "SELECT nombre FROM Responsable WHERE correoInstitucional =?";
        $query = $this->db->query($sql, array($correo));    
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->nombre;
        }else{
            return NULL;
        }         
    }
    
}

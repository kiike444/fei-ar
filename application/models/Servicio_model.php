<?php

class servicio_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }    
    
    /**
     * Función que recibe y guarda datos de un servicio en la base de datos
     * @param string $nombre - Nombre de servicio
     * @param string $descripcion - Descripción de servicio
     * @param string $fechaRegistro - Fecha de registro del servicio
     * @param int $idEspacioDeInteres - Clave del espacio de interés asociado
     * @param int $idDependencia - Clave de la dependencia asociada
     * @param int $idResponsable - Clave del responsable del servicio
     * @return boolean
     *      TRUE = Si se guardó el servicio
     *      FALSE = Si no se pudo guardar el servicio
     */
    public function guardar($nombre, $descripcion, $fechaRegistro, $idEspacioDeInteres ,$idDependencia, $idResponsable){
       $data=array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'fechaRegistro' => $fechaRegistro,
            'idEspacioDeInteres' => $idEspacioDeInteres,
            'idDependencia' => $idDependencia,
            'idResponsable' => $idResponsable
	);        
        
        $this->db->insert('Servicio', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Función que verifica si un servicio ya está registrado en una dependencia
     * mediante el nombre del servicio y la clave de la dependencia asociada.
     * @param string $nombre - Nombre de servicio
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si el servicio está registrado
     *      FALSE = Si el servicio no está registrado
     */
    public function estaRegistrado($nombre, $idDependencia){
        $sql = "SELECT * FROM Servicio WHERE  nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }    
    
    /**
     * Función que obtiene los servicios de una dependencia
     * @param type $idDependencia - Clave de la dependencia
     * @return array asociativo {nombre,descripcion,fechaRegistro,nombreEspacio,nombreResponsable}
     */
    public function getServiciosDeDependencia($idDependencia){
        $sql = 'SELECT Servicio.idServicio, Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, EspacioDeInteres.nombre AS "nombreEspacio" , Responsable.nombre AS "nombreResponsable"'
                . "FROM Servicio INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres)"
                . "INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) "
                . "WHERE Servicio.idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }    
    
    /**
     * Función que obtiene los servicios de una dependencia sin responsables asignados
     * @param type $idDependencia - Clave de la dependencia
     * @return array asociativo {nombre,descripcion,fechaRegistro,nombreEspacio}
     */
    public function getServiciosSinResponsablesDeDependencia($idDependencia){
        $sql = 'SELECT Servicio.idServicio, Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, '
                . 'EspacioDeInteres.nombre AS "nombreEspacio" FROM Servicio '
                . 'INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres) '
                . 'WHERE Servicio.idResponsable IS NULL AND Servicio.idDependencia = ?';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }  
    

    /**
     * Función que obtiene los servicios de una dependencia que no tienen un espacio de interés
     * asignado
     * @param type $idDependencia - Clave de la dependencia
     * @return array asociativo {nombre,descripcion,fechaRegistro,nombreResponsable}
     */
    public function getServiciosSinEspacioDeDependencia($idDependencia){
        $sql = 'SELECT Servicio.idServicio, Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, Responsable.nombre AS "nombreResponsable" '
                . 'FROM Servicio INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) '
                . 'WHERE Servicio.idDependencia = ? AND idEspacioDeInteres IS NULL';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }       
    
    
    /**
     * Función que elimina un servicio mediante su nombre y la clave de la dependencia
     * asociada.
     * @param string $nombre  - Nombre del servicio
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si se eliminó el servicio
     *      FALSE = Si no se pudo eliminar el servicio
     */
    public function eliminarServicioPorNombre($nombre, $idDependencia){
       $data=array(
            'nombre' => $nombre,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Servicio', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }    

    /**
     * Función que elimina un servicio mediante su nombre y la clave de la dependencia
     * asociada.
     * @param string $nombre  - Nombre del servicio
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si se eliminó el servicio
     *      FALSE = Si no se pudo eliminar el servicio
     */
    public function eliminarServicioPorId($idServicio, $idDependencia){
       $data=array(
            'idServicio' => $idServicio,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Servicio', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }     
    
    /**
     * Función que obtiene los datos de un servicio mediante su nombre y la clave
     * de la dependencia asociada.
     * @param string $nombre
     * @param int $idDependencia
     * @return array {nombre,descripcion,fechaRegistro,nombreEspacio,imagenDescriptiva,nombreResponsable}
     *      NULL = Si no hay un servicio con ese nombre en la dependencia
     */
    public function getServicio($idServicio, $idDependencia){
        $sql = 'SELECT Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, EspacioDeInteres.nombre AS "nombreEspacio" , '
                . 'EspacioDeInteres.imagenDescriptiva ,  Responsable.nombre AS "nombreResponsable"'
                . "FROM Servicio INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres)"
                . "INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) "
                . "WHERE Servicio.idDependencia = ? AND Servicio.idServicio = ?";
        $query = $this->db->query($sql, array($idDependencia,$idServicio));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }    

    /**
     * Función que obtiene los datos de un servicio mediante su nombre y la clave
     * de la dependencia asociada.
     * @param int $idServicio
     * @param int $idDependencia
     * @return array {nombre,descripcion,fechaRegistro,nombreEspacio,imagenDescriptiva,nombreResponsable}
     *      NULL = Si no hay un servicio con ese nombre en la dependencia
     */
    public function getServicioPorId($idServicio, $idDependencia){
        $sql = 'SELECT Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, EspacioDeInteres.nombre AS "nombreEspacio" , '
                . 'EspacioDeInteres.imagenDescriptiva ,  Responsable.nombre AS "nombreResponsable"'
                . "FROM Servicio INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres)"
                . "INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) "
                . "WHERE Servicio.idDependencia = ? AND Servicio.idServicio = ?";
        $query = $this->db->query($sql, array($idDependencia,$idServicio));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }     
    
    /**
     * Función que obtiene los datos de un servicio sin espacio asignado mediante su ID y la clave
     * de la dependencia asociada.
     * @param string $nombre
     * @param int $idDependencia
     * @return array {nombre,descripcion,fechaRegistro,imagenDescriptiva,nombreResponsable}
     *      NULL = Si no hay un servicio con ese nombre en la dependencia
     */
    public function getServicioSinEspacio($idServicio, $idDependencia){
        $sql = 'SELECT Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, Responsable.nombre AS "nombreResponsable" '
                . 'FROM Servicio INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) '
                . 'WHERE Servicio.idDependencia = ? AND Servicio.idServicio = ?';
        $query = $this->db->query($sql, array($idDependencia,$idServicio));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }   
    
    /**
     * Función que obtiene los datos de un servicio sin responsable asignado mediante su ID y la clave
     * de la dependencia asociada.
     * @param string $nombre
     * @param int $idDependencia
     * @return array {nombre,descripcion,fechaRegistro,imagenDescriptiva,nombreResponsable}
     *      NULL = Si no hay un servicio con ese nombre en la dependencia
     */
    public function getServicioSinResponsablePorId($idServicio, $idDependencia){
        $sql = 'SELECT Servicio.idServicio, Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, '
                . 'EspacioDeInteres.nombre AS "nombreEspacio", EspacioDeInteres.imagenDescriptiva FROM Servicio '
                . 'INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres) '
                . 'WHERE Servicio.idDependencia = ? AND Servicio.idServicio = ?';
        $query = $this->db->query($sql, array($idDependencia,$idServicio));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    }       
    
    /**
     * Función que obtiene los datos de un servicio sin espacio asignado mediante su nombre y la clave
     * de la dependencia asociada.
     * @param int $idServicio
     * @param int $idDependencia
     * @return array {nombre,descripcion,fechaRegistro,imagenDescriptiva,nombreResponsable}
     *      NULL = Si no hay un servicio con ese nombre en la dependencia
     */
    public function getServicioSinEspacioPorId($idServicio, $idDependencia){
        $sql = 'SELECT Servicio.nombre, Servicio.descripcion, Servicio.fechaRegistro, Responsable.nombre AS "nombreResponsable" '
                . 'FROM Servicio INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) '
                . 'WHERE Servicio.idDependencia = ? AND Servicio.idServicio = ?';
        $query = $this->db->query($sql, array($idDependencia,$idServicio));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }         
    } 
    
    
    /**
     * Función que obtiene datos de los servicios de un espacio de interés mediante
     * el nombre del espacio de interés y la dependencia asociada
     * @param string $nombreEspacio = Nombre del espacio de interés
     * @param int $idDependencia = Clave de la dependencia asociada.
     * @return array {nombre, nombreResponsable}
     *      NULL = Si no hay servicios en el espacio de interés
     */
    public function getServiciosDeEspacioDeInteres($idEspacioDeInteres, $idDependencia){
        $sql = 'SELECT Servicio.nombre, Responsable.nombre AS "nombreResponsable" FROM Servicio'
                . ' INNER JOIN EspacioDeInteres ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres) '
                . 'INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) '
                . 'WHERE EspacioDeInteres.idEspacioDeInteres  = ? AND Servicio.idDependencia = ?';
        $query = $this->db->query($sql, array($idEspacioDeInteres,$idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }           
    }
    
    /**
     * Función que actualiza los datos de un servicio
     * @param int $idServicio - Clave del servicio
     * @param string $nombre - Nuevo nombre del servicio
     * @param string $descripcion - Nueva descripción
     * @param int $idEspacioDeInteres - Clave del nuevo espacio de interés
     * @param int $idDependencia - Clave de la dependencia asociada
     * @param int $idResponsable - Clave del nuevo responsable
     * @return boolean
     *      TRUE = Si se actualizó la información del servicio
     *      FALSE = Si no se puedo actualizar el servicio
     */
    public function actualizar($idServicio, $nombre, $descripcion, $idEspacioDeInteres ,$idDependencia, $idResponsable){
        
       $data=array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'idEspacioDeInteres' => $idEspacioDeInteres,
            'idDependencia' => $idDependencia,
            'idResponsable' => $idResponsable
	);             
        
        $where = array(
                       'idDependencia' => $idDependencia,
                       'idServicio' => $idServicio
        );
        

        $this->db->update('Servicio', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }              
    }    
    
    /**
     * Función que obtiene el nombre de un servicio mediante su ID
     * @param int $idServicio
     * @return string
     */
    public function getNombrePorId($idServicio){
        $sql = 'SELECT nombre FROM Servicio WHERE idServicio  = ?';
        $query = $this->db->query($sql, array($idServicio));
        
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->nombre;
        }else{
            return NULL;
        } 
    }

    /**
     * Obtiene los servicios de una dependencia para la aplicación móvil
     * @param int $idDependencia
     * @return arreglo de servicios
     */
    public function getServiciosDeDependenciaParaCliente($idDependencia){
        $sql = 'SELECT Servicio.idServicio, Servicio.nombre, Servicio.descripcion, '
                . 'Servicio.idEspacioDeInteres , Servicio.idResponsable FROM Servicio INNER JOIN EspacioDeInteres '
                . 'ON (EspacioDeInteres.idEspacioDeInteres = Servicio.idEspacioDeInteres) '
                . 'INNER JOIN Responsable ON (Responsable.idResponsable = Servicio.idResponsable) '
                . 'WHERE Servicio.idDependencia = ?';
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }       
    }       
    
}

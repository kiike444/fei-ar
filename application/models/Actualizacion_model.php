<?php

class actualizacion_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Función que crea registro en la tabla de actualización de la base de datos
     * cuando una dependencia es registrada
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function crearRegistroDeActualizaciones($idDependencia, $fecha){
       $data=array(
            'idDependencia' => $idDependencia,
            'dependencia' => $fecha,
            'espaciosDeInteres' => $fecha,
            'servicios' => $fecha,
            'eventos' => $fecha,
            'senialamientos' =>$fecha,
            'responsables' =>$fecha
	);        
        
        $this->db->insert('Actualizacion', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }        
    }
    
    /**
     * Función que registra la fecha cuando los datos de una dependencia son
     * actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function dependenciaActualizada($idDependencia, $fecha){ 
       $data=array(
            'dependencia' => $fecha
	);             
        
        $where = array(
            'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }           
        
    }
    
    /**
     * Función que registra la fecha en el registro de la tabla de actualización
     * cuando los espacios de interés de una dependencia son actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function espaciosDeInteresActualizados($idDependencia, $fecha){
       $data=array(
            'espaciosDeInteres' => $fecha
	);             
        
        $where = array(
                       'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }
    
    /**
     * Función que registra la fecha en el registro de actualización en la tabla
     * actualización cuando los servicios de una dependencia son actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function serviciosActualizados($idDependencia, $fecha){
       $data=array(
            'servicios' => $fecha
	);             
        
        $where = array(
                       'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    } 
    
    /**
     * Función que registra la fecha en el registro de actualización en la tabla
     * actualización cuando los eventos de una dependencia son actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function eventosActualizados($idDependencia, $fecha){
       $data=array(
            'eventos' => $fecha
	);             
        
        $where = array(
                       'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }
    
    
    /**
     * Función que registra la fecha en el registro de actualización en la tabla
     * actualización cuando los señlamientos de una dependencia son actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function senialamientosActualizados($idDependencia, $fecha){
       $data=array(
            'senialamientos' => $fecha
	);             
        
        $where = array(
            'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }      
    
    /**
     * Función que registra la fecha en el registro de actualización en la tabla
     * actualización cuando los responsables de una dependencia son actualizados
     * @param int $idDependencia
     * @param string $fecha
     * @return boolean
     */
    public function responsablesActualizados($idDependencia, $fecha){
       $data=array(
            'responsables' => $fecha
	);             
        
        $where = array(
            'idDependencia' => $idDependencia
        );
        
        $this->db->update('Actualizacion', $data, $where ); 
        
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }         
    }         
    
    /**
     * Función que obtiene las fechas de actualizaciones del registro de actualizaciones
     * de una dependencia para enviarlos a la aplicación móvil
     * @param int $idDependencia
     * @return Actualizacion
     */
    public function getActualizaciones($idDependencia){
        $sql = 'SELECT dependencia, espaciosDeInteres, servicios, eventos, senialamientos, responsables '
                . 'FROM Actualizacion WHERE idDependencia = ?';
         $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->row();
        }else{
            return NULL;
        }            
    }
    
}

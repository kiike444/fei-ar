<?php

class responsable_model extends CI_Model{
   
    public function __construct(){
        parent:: __construct();
    }

    /**
     * Función que recibe y guarda los datos de un responsable en la base de datos
     * @param string $nombre - Nombre del responsable
     * @param string $correoInstitucional - Correo institucional del responsable
     * @param string $fechaRegistro - Fecha de registro del responsable
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return boolean
     *      TRUE = Si los datos del responsable se guardó correctamente
     *      FALSE = Si no se pudieron guardar los datos del responsable
     */
    public function guardar($nombre, $correoInstitucional, $tipo, $fechaRegistro, $idDependencia){
       $data=array(
            'nombre' => $nombre,
            'correoInstitucional' => $correoInstitucional,
            'tipo' => $tipo,
            'fechaRegistro' => $fechaRegistro,
            'idDependencia' => $idDependencia
	);        
        $this->db->insert('Responsable', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Función que verifica si un responsable ya está registrado en la dependencia
     * mediante su correo y la clave de la dependencia asociada.
     * @param string $correo - Correo del responsable
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return boolean
     *      TRUE = Si el responsable ya está registrado.
     *      FALSE = Si el correo no corresponde a un responsable de la dependencia.
     */
    public function estaRegistrado($correo, $idDependencia){
        $sql = "SELECT * FROM Responsable WHERE  correoInstitucional = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($correo,$idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Función que obtiene datos de los responsables registrados en una dependencia
     * @param int $idDependencia = Clave de la dependencia
     * @return array asociativo {nombre, correoInstitucional}
     */
    public function getResponsablesDeDependencia($idDependencia){
        $sql = "SELECT nombre, correoInstitucional, tipo FROM Responsable WHERE idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }               
    }
    
    /**
     * Función que elimina un responsable mediante su correo y la clave de la
     * dependencia asociada.
     * @param string $correoInstitucional - Correo del responsable
     * @param int $idDependencia - Clave de la dependencia
     * @return boolean
     *      TRUE = Si el responsable se eliminó
     *      FALSE = Si el responsable no se pudo eliminar
     */
    public function eliminar($correoInstitucional, $idDependencia){
       $data=array(
            'correoInstitucional' => $correoInstitucional,
            'idDependencia' => $idDependencia
	);        
        
        $this->db->delete('Responsable', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }          
    }
    
    /**
     * Función que obtiene la clave de un responsable mediante su correo y la
     * clave de la dependencia asociada.
     * @param string $correoInstitucional - Correo del responsable
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return int
     */
    public function getIdResponsablePorCorreo($correoInstitucional, $idDependencia){
        $sql = "SELECT idResponsable FROM Responsable WHERE correoInstitucional = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($correoInstitucional,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idResponsable;
        }else{
            return NULL;
        }      
    }
    
    /**
     * Función que obtiene la clave de un responsable mediante su nombre y la
     * clave de la dependencia asociada.
     * @param string $nombre - Nombre del responsable
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return int
     */
    public function getIdResponsablePorNombre($nombre, $idDependencia){
        $sql = "SELECT idResponsable FROM Responsable WHERE nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        if($query->num_rows()>0){
            $row = $query->row(); 
            return $row->idResponsable;
        }else{
            return NULL;
        }      
    }      
    
    /**
     * Función que verifica si un responsable está registrado en una dependencia
     * mediante su nombre y la clave de la dependencia asociada.
     * @param string $nombre - Nombre del responsable
     * @param int $idDependencia - Clave de la dependencia asociada
     * @return boolean
     *      TRUE = Si el responsable está registrado
     *      FALSE = Si no se encontró nungún responsable
     */
    public function estaRegistradoPorNombre($nombre, $idDependencia){
        $sql = "SELECT * FROM Responsable WHERE  nombre = ? AND idDependencia = ?";
        $query = $this->db->query($sql, array($nombre,$idDependencia));
        
        if($query->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }    
    
    /**
     * Obtiene los responsables de una dependencia para la aplicación móvil
     * @param int $idDependencia
     * @return arreglo de responsables
     */
    public function getResponsablesDeDependenciaParaCliente($idDependencia){
        $sql = "SELECT idResponsable, nombre, correoInstitucional FROM Responsable WHERE idDependencia = ?";
        $query = $this->db->query($sql, array($idDependencia));
        
        if($query->num_rows()>0){
            return $query->result();
        }else{
            return NULL;
        }               
    }    
    
}
